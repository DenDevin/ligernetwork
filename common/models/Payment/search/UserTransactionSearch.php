<?php

namespace common\models\Payment\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payment\UserTransaction;

/**
 * UserTransactionSearch represents the model behind the search form of `common\models\Payment\UserTransaction`.
 */
class UserTransactionSearch extends UserTransaction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'transaction_id', 'payment_summ_eur', 'payment_system', 'payment_status', 'sender_id', 'reciever_id', 'created_at', 'updated_at'], 'integer'],
            [['payment_type', 'payment_title', 'invoice_link', 'payment_bank', 'routing', 'account_number', 'account_type', 'sender_number', 'reciever_number', 'benefitiary_name'], 'safe'],
            [['payment_summ_btc'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserTransaction::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 25
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'transaction_id' => $this->transaction_id,
            'payment_summ_btc' => $this->payment_summ_btc,
            'payment_summ_eur' => $this->payment_summ_eur,
            'payment_system' => $this->payment_system,
            'payment_status' => $this->payment_status,
            'sender_id' => $this->sender_id,
            'reciever_id' => $this->reciever_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'payment_title', $this->payment_title])
            ->andFilterWhere(['like', 'invoice_link', $this->invoice_link])
            ->andFilterWhere(['like', 'payment_bank', $this->payment_bank])
            ->andFilterWhere(['like', 'routing', $this->routing])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'sender_number', $this->sender_number])
            ->andFilterWhere(['like', 'reciever_number', $this->reciever_number])
            ->andFilterWhere(['like', 'benefitiary_name', $this->benefitiary_name]);

        return $dataProvider;
    }
}
