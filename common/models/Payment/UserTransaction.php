<?php

namespace common\models\Payment;

use Yii;
use yii\behaviors\TimestampBehavior;

class UserTransaction extends \yii\db\ActiveRecord
{

    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_EXPIRED = 2;


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function tableName()
    {
        return 'user_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_type', 'transaction_id', 'payment_system', 'payment_status', 'sender_id', 'reciever_id', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['payment_title', 'invoice_link', 'payment_bank', 'payment_summ_btc', 'payment_summ_eur', 'routing', 'account_number', 'account_type', 'sender_number', 'reciever_number', 'benefitiary_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payment_type' => 'Type',
            'transaction_id' => 'Transaction ID',
            'payment_title' => 'Payment Title',
            'invoice_link' => 'Invoice Link',
            'payment_summ_btc' => 'Payment Summ',
            'payment_summ_eur' => 'Amount',
            'payment_system' => 'Payment System',
            'payment_status' => 'Status',
            'payment_bank' => 'Payment Bank',
            'routing' => 'Routing',
            'account_number' => 'Account Number',
            'account_type' => 'Account Type',
            'sender_number' => 'Sender Number',
            'reciever_number' => 'Reciever Number',
            'sender_id' => 'Sender ID',
            'reciever_id' => 'Reciever ID',
            'benefitiary_name' => 'Benefitiary Name',
            'created_at' => 'Date',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Payment\query\UserTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\Payment\query\UserTransactionQuery(get_called_class());
    }

    public static function create($order)
    {

        $transaction = new self([
            'user_id'      => $order['user_id'],
            'payment_type' => $order['order_type'],
            'transaction_id' => $order['id'],
            'payment_title' => $order['order_id'],
            'invoice_link' => $order['url'],
            'payment_summ_btc' => $order['coin_amount'],
            'payment_summ_eur' => self::convertEuroToCent($order['order_summ_eur']),
            'payment_summ_pure_eur' => self::convertEuroToCent($order['order_pure_summ_eur']),
            'payment_system' => $order['order_system'],
            'payment_status' => self::STATUS_PENDING,
            'reciever_number' => $order['address'],


        ]);
        if($transaction->save(false))
        {
            $message['title'] = Yii::t('frontend.api', 'admin_withdraw_message_title');
            $message['text'] = Yii::t('frontend.api', 'admin_withdraw_message_text');
            Yii::$app->notification->sendMessage('superadmin', $message, 'success');
            return true;
        }


    }


    public static function setExeeded($post)
    {
        $transaction = self::findOne(['transaction_id' => $post['data']['txn_id']]);
        $transaction->payment_status = self::STATUS_EXPIRED;
        if($transaction->save(false))
        {
            $data['message'] = Yii::t('frontend.api', 'transexpired');
            $data['status'] = true;
            return $data;
        }
    }


    public static function getUserTransactions()
    {
       $user = Yii::$app->getUser();
       $transactions = self::findAll(['user_id' => $user->id]);
       return $transactions;


    }


    public function convertEuroToCent($euro)
    {
        return $euro * 100;
    }

    public function convertCentToEuro($cent)
    {

        return $cent / 100;
    }

    public static function getWithdrawnFunds($user_id)
    {

        $query = (new \yii\db\Query())->
        from('user_transaction')->
        where([
            'payment_type' => 'withdraw',
            'user_id' => $user_id,
            'payment_status' => self::STATUS_CONFIRMED,
        ]);

        return $query->sum('payment_summ_eur');

    }



    public static function getReplenishFunds($user_id)
    {

        $query = (new \yii\db\Query())->
        from('user_transaction')->
        where([
            'payment_type' => 'replenishment',
            'user_id' => $user_id,
            'payment_status' => self::STATUS_CONFIRMED,
        ]);

        return $query->sum('payment_summ_pure_eur');

    }



    public static function createWithdrawTransaction($post)
    {

        $user = Yii::$app->getUser();
        $transaction = new self([
            'user_id'      => $user->id,
            'payment_type' => 'withdraw',
            'transaction_id' => null,
            'payment_summ_eur' => self::convertEuroToCent($post['data']['amount']),
            'payment_summ_pure_eur' => self::convertEuroToCent($post['data']['amount_pure']),
            'payment_system' => null,
            'payment_status' => self::STATUS_PENDING,
            'reciever_number' => $post['data']['wallet'],


        ]);

        if($transaction->save(false))
        {
            return true;
        }
    }


    public function allowWithdrawPayment($transaction)
    {
       // $order = Yii::$app->payment->alfacoins->createAdminWithdraw($transaction);

    }

    public function convertToEuro($cents)
    {
        $balance = $cents / 100;
        return $balance;

    }

    public function convertToCent($euro)
    {
        $balance = $euro * 100;

    }


}
