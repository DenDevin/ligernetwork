<?php

namespace common\models\Payment\query;

/**
 * This is the ActiveQuery class for [[\common\models\Payment\UserTransaction]].
 *
 * @see \common\models\Payment\UserTransaction
 */
class UserTransactionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Payment\UserTransaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Payment\UserTransaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
