<?php

namespace common\models\Payment;

use Yii;
use common\models\Payment\UserTransaction;
use common\models\Package\UserPackage;
use common\models\Package\Package;
use common\models\Package\UserPackageStat;



class UserBalance extends \yii\db\ActiveRecord
{

    public $user;

    public function __construct()
    {
      $this->user = Yii::$app->user->identity;
    }

    public static function tableName()
    {
        return 'user_balance';
    }


    public function rules()
    {
        return [
            [['user_id', 'dm', 'euro', 'income', 'product_return'], 'integer'],

        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'dm' => 'Dm',
            'euro' => 'Euro',
            'income' => 'Income',
        ];
    }


    public static function find()
    {
        return new \common\models\Payment\query\UserBalanceQuery(get_called_class());
    }

    public static function createBalance($token)
    {

        $balance  = new self([
            'user_token' => $token,
            'dm' => 0,
            'euro' => 0,
            'income' => 0,
            'product_return' => 0,
        ]);
        $balance->save(false);

    }

    public function convertEuroToCent($euro)
    {
        return $euro * 100;
    }

    public function convertEuroFromCent($cents)
    {

      return $cents / 100;
    }

    public function getUserBonuses()
    {

         return UserPackageStat::getUserBonuses();

    }

   public static function isWithdrawEnable($trans)
   {
       $user = Yii::$app->getUser();
       $balance = self::getUserBalanceById($user->id);

       if($balance)
       {
           if($balance->income >= $trans->payment_summ_eur)
           {
               return true;
           }
           else
           {
               return false;
           }

       }

   }


    public function getWithdrawnFunds()
    {
        $user = Yii::$app->getUser();
        return self::convertEuroFromCent(UserTransaction::getWithdrawnFunds($user->id));
    }





    public static function withdrawEnable($post)
    {
        $user = Yii::$app->getUser();
        $balance = self::getUserBalanceById($user->id);
        $amount = self::convertEuroToCent($post['data']['amount']);

        if($balance)
        {
           if($balance->income >= $amount)
           {
              return true;
           }
           else
               {
                   return false;
               }

        }

    }

    public static function payPackageFromBalance($post)
    {
        $user = Yii::$app->getUser();
        $balance = self::getUserBalanceById($user->id);
        if(Package::isActive($post['data']['package']))
        {
            $data['message'] = Yii::t('frontend.api', 'packageactive');
            $data['status'] = 'error';
            return $data;
        }

        if((int)$balance->euro >= (int)$post['data']['cost'])
        {
            $balance->euro -=  $post['data']['cost'];
            $package = UserPackage::prolongPackage($post, $user->id);
            $replenishTeamBonus = UserPackageStat::replenishTeamBonus($package, $post['data']['cost']);
            $replenishProductReturn = self::replenishProductReturn($post['data']['cost']);
            $replenishDirectBonus = self::replenishDirectBonus();


            if($balance->save(false))
            {
                $message['title'] = Yii::t('frontend.api', 'your_balance_was_payed_title');
                $message['text'] = Yii::t('frontend.api', 'your_balance_was_payed_text');
                Yii::$app->notification->sendMessage($user->id, $message, 'success');
                $data['message'] = Yii::t('frontend.api', 'paysuccess');
                $data['status'] = 'success';
                $data['avaliable_till'] = $package['avaliable_till'];
                $data['package'] = $package['package'];
                return $data;
            }

        }

        else
            {
                $data['message'] = Yii::t('frontend.api', 'nomoney');
                $data['status'] = 'error';
                return $data;
            }

    }





    public static function checkUserTransactions()
    {
        $userId = Yii::$app->user->identity->getId();
        $transactions = UserTransaction::find()->
        where([
            'user_id' => $userId,
            'payment_status' => UserTransaction::STATUS_PENDING
        ])->
        orWhere([
            'user_id' => $userId,
            'payment_status' => UserTransaction::STATUS_EXPIRED
        ])->
        all();

        $completed = self::checkPendingTransactions($transactions);
        if($completed && count($completed) > 0)
        {
           foreach($completed as $transaction)
           {

             $trans = UserTransaction::find()->where(['transaction_id' => $transaction->transaction_id])->one();
             if( $trans->payment_status === UserTransaction::STATUS_PENDING ||
                 $trans->payment_status === UserTransaction::STATUS_EXPIRED
             )
             {

                 if($trans->payment_type == 'replenishment')
                 {

                     $trans->payment_status = UserTransaction::STATUS_CONFIRMED;
                     if($trans->save(false))
                     {
                         self::replenishUserEuroBalance($trans);
                     }

                 }

                 if($trans->payment_type == 'withdraw')
                 {

                     if(self::isWithdrawEnable($trans))
                     {
                         $trans->payment_status = UserTransaction::STATUS_CONFIRMED;
                         if($trans->save(false))
                         {
                             self::withdrawUserEuroBalance($trans);
                         }
                     }
                     else
                         {
                             $message['title'] = Yii::t('frontend.api', 'withdraw_not_enable_title');
                             $message['text'] = Yii::t('frontend.api', 'withdraw_not_enable_text');
                             Yii::$app->notification->sendMessage($trans->user_id, $message, 'error');

                         }

                 }
             }

           }

        }



    }


       private static function replenishUserEuroBalance($transaction)
       {

           if($transaction->payment_status === UserTransaction::STATUS_CONFIRMED)
            {
                $balance = self::getUserBalanceById($transaction->user_id);
                $balance->euro += (int)$transaction->payment_summ_pure_eur;

                if($balance->save(false))
                {

                    $message['title'] = Yii::t('frontend.api', 'your_balance_replenished_title');
                    $message['text'] = Yii::t('frontend.api', 'your_balance_replenished_text');
                    Yii::$app->notification->sendMessage($transaction->user_id, $message, 'success');
                }

            }

       }



        public static function checkPendingTransactions($transactions)
        {
         //   print_r($transactions); die;
            $completed_trans_arr = [];

            foreach($transactions as $transaction)
            {

                if($transaction->payment_type == 'replenishment')
                {
                    $transaction['transaction_id'] = 649763;
                    $order = Yii::$app->payment->alfacoins->getOrderStatus($transaction['transaction_id']);

                    if($order['status'] == 'new')
                    {
                        // платеж только создан
                    }

                    if($order['status'] == 'partially paid')
                    {
                        // платеж оплачен частично // попытка оплаты // не хватает для оплаты платежа
                    }


                    if($order['status'] == 'completed')
                    {
                        $completed_trans_arr[] = $transaction;
                        // платеж оплачен
                    }

                    if($order['status'] == 'expired')
                    {
                        // прошло время транзакции

                    }

                    if($order['status'] == 'paid')
                    {
                        // платеж оплачен но не подтвержден блокчейном
                    }





                }

                if($transaction->payment_type == 'withdraw')
                {
                    $order = Yii::$app->payment->alfacoins->getOrderWithdrawStatus($transaction['transaction_id']);

                    if($order['status'] == 'new')
                    {
                        // платеж только создан
                    }

                    if($order['status'] == 'partially paid')
                    {
                        // платеж оплачен частично // попытка оплаты // не хватает для оплаты платежа
                    }


                    if($order['status'] == 'completed')
                    {
                        $completed_trans_arr[] = $transaction;
                        // платеж оплачен
                    }

                    if($order['status'] == 'expired')
                    {
                        // прошло время транзакции

                    }

                    if($order['status'] == 'paid')
                    {
                        // платеж оплачен но не подтвержден блокчейном
                    }



                }




            }

        return $completed_trans_arr;

        }

    public static function withdrawUserEuroBalance($transaction)
    {

            $balance = self::getUserBalanceById($transaction->user_id);

            $balance->income -= $transaction->payment_summ_eur;
            if($balance->save(false))
            {
                $message['title'] = Yii::t('frontend.api', 'your_balance_withdraw_title');
                $message['text'] = Yii::t('frontend.api', 'your_balance_withdraw_text');
                Yii::$app->notification->sendMessage($transaction->user_id, $message, 'success');
            }










    }


    public function getUserBalance($user_token)
    {

        $balance = self::findOne(['user_token' => $user_token]);
        return $balance;

    }

    public function getUserBalanceData()
    {
        $bal_obj = new self;
        $balance = [];
        $user = Yii::$app->user->identity;
        $balance_data = self::getUserBalance($user->user_token);

        $balance['income'] = $bal_obj->convertPriceToEuro($balance_data->income);
        $balance['product_return'] = $bal_obj->convertPriceToEuro($balance_data->product_return);
        $balance['euro'] = $bal_obj->convertPriceToEuro($balance_data->euro);

       return $balance;

    }

    public static function getUserBalanceByToken($user_token)
    {

        $balance = self::findOne(['user_token' => $user_token]);
        return $balance;

    }


    private static function getUserBalanceById($user_id)
    {

        $balance = self::findOne(['user_id' => $user_id]);
        return $balance;

    }


    public function convertToEuro()
    {
        $balance = $this->euro / 100;
        return $balance;

    }

    public function convertToCent()
    {
        $balance = $this->euro * 100;

    }

    public function convertPriceToCent($price)
    {
        return $price * 100;
    }

    public function convertPriceToEuro($price)
    {
       return $price / 100;
    }


    public static function replenishProductReturn($paySum)
    {


    }


    public static function replenishDirectBonus()
    {




    }








}
