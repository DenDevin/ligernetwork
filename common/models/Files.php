<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use Yii;
use yii\helpers\FileHelper;

class Files extends \yii\db\ActiveRecord
{

public $video;
public $image;
public $document;
public $audio;
public $file_name;


public function behaviors() {
return [

TimestampBehavior::className(),


];
}

public static function tableName()
{
return 'files';
}

/**
* {@inheritdoc}
*/
public function rules()
{
return [
[['description'], 'string'],
[['title', 'ext', 'unit', 'url'], 'string', 'max' => 255],
];
}

/**
* {@inheritdoc}
*/
public function attributeLabels()
{
return [
'id' => 'ID',
'title' => 'Title',
'size' => 'Size',
'ext' => 'Ext',
'unit' => 'Unit',
'url' => 'Url',
'description' => 'Description',
'parent_id' => 'Parent ID',
'created_by' => 'Created By',
'active' => 'Active',
'created_at' => 'Created At',
'updated_at' => 'Updated At',
];
}

public function preparePath($file, $ext = null)
{

$name = Yii::$app->security->generateRandomString();
$name = substr_replace($name, '/', 2, 0);
$name = substr_replace($name, '/', 5, 0);
$name = $name . '.' . $file->extension;
$this->file_name = $name;
if($ext == 'video')
{
$path = $this->getStorageVideoPath() . $name;
}
elseif($ext == 'image')
{
$path = $this->getStorageImagePath() . $name;
}
elseif($ext == 'document')
{
$path = $this->getStorageDocPath() . $name;
}


$path = FileHelper::normalizePath($path);
if(FileHelper::createDirectory(dirname($path)))
{
return $path;
}

}



protected function getStoragePath()
{
return Yii::getAlias(Yii::$app->params['storagePath']);
}


protected function getStorageVideoPath()
{
return Yii::getAlias(Yii::$app->params['uploadVideoPath']);
}


protected function getStorageImagePath()
{
return Yii::getAlias(Yii::$app->params['uploadImagePath']);
}

protected function getStorageDocPath()
{
return Yii::getAlias(Yii::$app->params['uploadDocPath']);
}


public function saveFile($file, $id = null, $unit = null)
{
        if(!is_null($id))
        {
        $this->title = $file->name;
        $this->size = $file->size;
        $this->ext = $file->extension;
        $this->parent_id = $id;
        $this->unit = $unit;
        $this->url = $this->file_name;
        if($this->save())
        {
        return $this->file_name;
        }
        else
        {
        return false;
        }
        }

        else
        {

        $this->title = $file->name;
        $this->size = $file->size;
        $this->ext = $file->extension;
        $this->url = $this->file_name;
        if($this->save())
        {
        return $this->id;
        }
        else
        {
        return false;
        }

        }


        }








}