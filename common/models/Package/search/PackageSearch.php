<?php

namespace common\models\Package\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Package\Package;

/**
 * PackageSearch represents the model behind the search form of `common\models\Package`.
 */
class PackageSearch extends Package
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'turnover', 'bonus', 'cost', 'term'], 'integer'],
            [['code', 'title', 'type', 'pay_term'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'turnover' => $this->turnover,
            'bonus' => $this->bonus,
            'cost' => $this->cost,
            'term' => $this->term,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'pay_term', $this->pay_term]);

        return $dataProvider;
    }
}
