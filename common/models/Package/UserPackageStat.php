<?php

namespace common\models\Package;

use Yii;
use common\models\Package\query\UserPackageStatQuery;
use yii\behaviors\TimestampBehavior;
use backend\modules\marketing\models\UserBonuses;
use backend\modules\marketing\models\UserPartnerBonuses;
use backend\modules\marketing\models\UserMarketingTeam;
use common\models\User as CommonUser;
use common\models\User\UserTree;
use common\models\Payment\UserBalance;

class UserPackageStat extends \yii\db\ActiveRecord
{

    const PACKAGE_CLIENT = 1;
    const PACKAGE_BUSINESS = 2;

    public $user;

    public function __construct()
    {
        $this->user = Yii::$app->user->identity;
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function tableName()
    {
        return 'user_package_stat';
    }


    public function rules()
    {
        return [
            [['user_id', 'package_id', 'bonus_count', 'bonus_type', 'pay_count', 'missing', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['active'], 'boolean'],
            [['title'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'package_id' => 'Package ID',
            'bonus_count' => 'Bonus Count',
            'bonus_type' => 'Bonus Type',
            'title' => 'Title',
            'data' => 'Data',
            'pay_count' => 'Pay Count',
            'missing' => 'Missing',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }





    public static function setPartnerBonusToUser($stat = null, $partnerBonuses = array())
    {


        $quantityActivePartners = Yii::$app->useridentity->countActivePartners();
        $stat->pay_count += 1;

      if($partnerBonuses)
      {
          foreach($partnerBonuses as $partnerBonus)
          {
              if($partnerBonus['partners_count'] == count($quantityActivePartners))
              {
                  $stat->bonus_count += $partnerBonus['amount'];
                  $message['title'] = Yii::t('frontend.api', 'your_balance_was_replenished_partner_bonus_title');
                  $message['text'] = Yii::t('frontend.api', 'your_balance_was_replenished_partner_bonus_text', ['bonus' => $partnerBonus['amount']]);
                  Yii::$app->notification->sendMessage($stat->user_id, $message, 'success');


              }


          }
      }




        $stat->save(false);

    }










    public static function setClientBonusToUser($stat = null, $bonuses = null)
    {

        $stat->pay_count +=1;
        if(
            $stat->pay_count >= $bonuses[$stat->bonus_type]['min'] &&
            $stat->pay_count <= $bonuses[$stat->bonus_type]['max']
           )
        {
            $stat->bonus_count += $bonuses[$stat->bonus_type]['amount'];
        }

        elseif(
            $stat->pay_count > $bonuses[$stat->bonus_type]['max'] &&
            $stat->bonus_type == 4
        )
        {
            $stat->bonus_count += $bonuses[4]['amount'];
        }

        else

        {

            if($stat->bonus_type > 4)
            {
                $stat->bonus_type = 4;
                $stat->bonus_count += $bonuses[$stat->bonus_type]['amount'];
            }

            else
            {
                $stat->bonus_type +=1;
                $stat->bonus_count += $bonuses[$stat->bonus_type]['amount'];

            }


        }

        $stat->save(false);

    }


    public static function getUserBonuses()
    {

        $user = Yii::$app->getUser();
        $stat = self::find()->
        where(['user_id' => $user->id])->
        andWhere(['package_id' => self::PACKAGE_CLIENT])->
        one();

        return (int)$stat->bonus_count;

    }


    public static function getClientBonuses()
    {
        $arr = [];
        $bonuses = UserBonuses::find()->all();
        foreach($bonuses as $bonus)
        {
            $arr[] = [
                'id' => $bonus->bonus_id,
                'min' => $bonus->min,
                'max' => $bonus->max,
                'amount' => $bonus->amount,
            ];
        }

        return $arr;
    }




    public static function getPartnerBonuses()
    {
        $arr = [];
        $bonuses = UserPartnerBonuses::find()->all();
        foreach($bonuses as $bonus)
        {
            $arr[] = [
                'id' => $bonus->id,
                'name' => $bonus->name,
                'partners_count' => $bonus->partners_count,
                'level' => $bonus->level,
                'amount' => $bonus->amount,
            ];
        }

        return $arr;
    }




    public static function countBonusForClient()
    {



        $bonuses = self::getClientBonuses();
        $user = Yii::$app->getUser();
        $type = self::find()->
        where(['user_id' => $user->id])->
        andWhere(['package_id' => self::PACKAGE_BUSINESS])->
        one();

        if($type)
        {
            return $bonuses[$type->bonus_type]['amount'];
        }
        else
        {
            return $bonuses[0]['amount'];
        }

    }



    public static function find()
    {
        return new UserPackageStatQuery(get_called_class());
    }



    public static function logHistory($package)
    {
        $clientBonuses = self::getClientBonuses();
        $partnerBonuses = self::getPartnerBonuses();

        $stat = self::find()->
        where(['user_id' => $package->user_id])->
        andWhere(['package_id' => $package->package_id])->
        one();

        if($package->package_id == self::PACKAGE_BUSINESS)
        {

            if(isset($stat))
            {
                self::setPartnerBonusToUser($stat, $partnerBonuses);
            }

            else
            {

                $new_stat = new self();
                $new_stat->user_id = $package->user_id;
                $new_stat->package_id = $package->package_id;
                $new_stat->bonus_type = 0;
                $new_stat->avaliable_till = $package->avaliable_till;
                $new_stat->pay_count = 1;
                $new_stat->missing = 0;
                $new_stat->active = true;
                $new_stat->save(false);


            }


        }

        if($package->package_id == self::PACKAGE_CLIENT)
        {

            if($stat)
            {
                self::setClientBonusToUser($stat, $clientBonuses);
            }
            else
            {

                $new_stat = new self([
                    'user_id' => $package->user_id,
                    'package_id' => $package->package_id,
                    'bonus_count' => $clientBonuses[0]['amount'],
                    'bonus_type' => $clientBonuses[0]['id'],
                    'avaliable_till' => $package->avaliable_till,
                    'pay_count' => 1,
                    'missing' => 0,
                    'active' => 1,
                ]);
                $new_stat->save(false);
            }

        }

    }


    public static function replenishTeamBonus($package, $amount)
    {
        $token = Yii::$app->user->identity->user_token;
        $teamBonuses = self::getTeamBonuses();
          if($package['package'] == self::PACKAGE_BUSINESS)
          {
              $node = UserTree::findOne(['name' => $token]);

              if(count($teamBonuses) > 0)
              {
                  foreach($teamBonuses as $teamBonus)
                  {
                      if($teamBonus['level'] == 1)
                      {
                          $parent = $node->parent;
                          $parentBalance = UserBalance::getUserBalanceByToken($parent->name);
                          $bonus = ($amount / 100) * $teamBonus['percent'];
                          $parentBalance->income += $bonus;
                          if($parentBalance->save(false))
                          {
                              $message['title'] = Yii::t('frontend.api', 'your_balance_was_replenished_partner_bonus_title');
                              $message['text'] = Yii::t('frontend.api', 'your_balance_was_replenished_partner_bonus_text', ['bonus' => $bonus]);
                             // Yii::$app->notification->sendMessageByToken($parent->name, $message, 'success');
                          }
                      }
                  }
              }

          }



    }




    public static function replenishRecursive($node, $teamBonusArray = array())
    {

    }




    public static function isFirstPayForBusiness()
    {

        $user = Yii::$app->getUser();
        $stat = self::find()->
        where(['user_id' => $user->id])->
        andWhere(['package_id' => self::PACKAGE_BUSINESS])->
        one();

        if($stat->pay_count == 1)
        {
              return true;
        }
        else
            {
              return false;
            }


    }


    public static function getTeamBonuses()
    {
        $arr = [];
        $bonuses = UserMarketingTeam::find()->all();
        foreach($bonuses as $bonus)
        {
            $arr[] = [
                'id' => $bonus->id,
                'name' => $bonus->name,
                'level' => $bonus->level,
                'percent' => $bonus->percent,
            ];
        }

        return $arr;
    }


}
