<?php

namespace common\models\Package\query;

/**
 * This is the ActiveQuery class for [[\common\models\Package\UserPackage]].
 *
 * @see \common\models\Package\UserPackage
 */
class UserPackageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Package\UserPackage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Package\UserPackage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
