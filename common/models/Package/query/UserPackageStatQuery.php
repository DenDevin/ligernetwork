<?php

namespace common\models\Package\query;

/**
 * This is the ActiveQuery class for [[UserPackageStat]].
 *
 * @see UserPackageStat
 */
class UserPackageStatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserPackageStat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserPackageStat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
