<?php

namespace common\models\Package;

use Yii;
use common\models\Package\UserPackage;
use common\models\Package\UserPackageStat;
use backend\modules\marketing\models\UserMarketingDirect;
use backend\modules\marketing\models\UserMarketing;


/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $title
 * @property string|null $type
 * @property int|null $turnover
 * @property int|null $bonus
 * @property int|null $cost
 * @property int|null $term
 * @property string|null $pay_term
 */
class Package extends \yii\db\ActiveRecord
{

    const PACKAGE_CLIENT = 1;
    const PACKAGE_BUSINESS = 2;

    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turnover', 'bonus', 'cost', 'term', 'direct_bonus'], 'integer'],
            [['code', 'title', 'type', 'pay_term'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код пакета',
            'title' => 'Название',
            'type' => 'Тип',
            'turnover' => 'Оборот',
            'bonus' => 'Бонус',
            'direct_bonus' => 'Direct - Бонус',
            'cost' => 'Стоимость',
            'term' => 'Срок предоставления',
            'pay_term' => 'Срок оплаты',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\PackageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PackageQuery(get_called_class());
    }

    public function isActiveforUser($package)
    {
        $user = Yii::$app->getUser();
        $invoice = UserPackage::find()->
        where(['package_id' => $package])->
        andWhere(['user_id' => $user->id])->
        one();

        if($invoice->avaliable_till > time())
        {
            return true;
        }

        else
        {
            return false;
        }
    }


    public function countBonusForClient()
    {
            $bonus = UserPackageStat::countBonusForClient();
            return $bonus;

    }

    public function countBonusForBusiness()
    {
        $all_statuses = UserMarketing::getMarketingStatuses();
        $user_status = UserMarketing::getUserStatusesData();

        if(count($all_statuses) > 0)
        {
            foreach($all_statuses as $key => $status)
            {
              if($user_status['product_return'] < $status['product_return'])
              {

                   //  return $all_statuses[$key+1]['leader_bonus'];
              }
            }
        }




    }




    public function getDirectCost()
    {

        $direct = $this->direct;
        if(Yii::$app->useridentity->issetFirstPay())
        {
            return $direct->next_pay;
        }

        else
        {
            return $direct->first_pay;
        }


    }


    public static function isActive($package)
    {
        $user = Yii::$app->getUser();
        $invoice = UserPackage::find()->
        where(['package_id' => $package])->
        andWhere(['user_id' => $user->id])->
        one();

        if($invoice->avaliable_till > time())
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public function getDirect()
    {
        return $this->hasOne(UserMarketingDirect::className(), ['id' => 'direct_bonus']);
    }


    public function termForUser($package)
    {
        $user = Yii::$app->getUser();
        $invoice = UserPackage::find()->
        where(['package_id' => $package])->
        andWhere(['user_id' => $user->id])->
        one();

        if($invoice->avaliable_till > time())
        {
            return $invoice->avaliable_till;
        }




    }

}
