<?php

namespace common\models\Package;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\Package\Package;
use common\models\Package\UserPackageStat;


class UserPackage extends \yii\db\ActiveRecord
{


    const PACKAGE_CLIENT = 1;
    const PACKAGE_BUSINESS = 2;

    const EVENT_PAY_BUSINESS = 'event_pay_business';

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function tableName()
    {
        return 'user_package';
    }


    public function rules()
    {
        return [
            [['user_id', 'package_id', 'avaliable_till', 'active', 'created_at', 'updated_at'], 'integer'],
            [['avaliable_till', 'created_at', 'updated_at'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'package_id' => 'Package ID',
            'avaliable_till' => 'Avaliable Till',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function prolongPackage($post, $user_id)
    {
        $data = [];
        $pack = Package::findOne($post['data']['package']);
        $term = self::getPackageTerm($pack);
        $package = self::find()->
        where([
            'user_id' => $user_id,
            'package_id' => $post['data']['package']
        ])->
        one();
        if($package)
        {

                 $package->avaliable_till = time() + $term;
                 $package->save(false);
                 UserPackageStat::logHistory($package);
        }
        else
            {


                $package = new self([
                    'user_id' => $user_id,
                    'package_id' => $post['data']['package'],
                    'avaliable_till' => time() + $term, /// month // year
                    'active' => true
                ]);

                UserPackageStat::logHistory($package);


                $package->save(false);
                $data['avaliable_till'] = $package->avaliable_till;
                $data['package'] = $post['data']['package'];
                return $data;
            }
    }

    public static function getPackageTerm($package)
    {
        $pay_term = 0;
        if($package->pay_term == 'monthly')
        {
            $pay_term = 2592000;
        }
        elseif($package->pay_term == 'annually')
        {
            $pay_term = 31536000;
        }
        return $pay_term;
    }

    public static function find()
    {
        return new \common\models\Package\query\UserPackageQuery(get_called_class());
    }


}
