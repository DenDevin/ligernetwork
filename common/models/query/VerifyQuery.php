<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Verify]].
 *
 * @see \common\models\Verify
 */
class VerifyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Verify[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Verify|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
