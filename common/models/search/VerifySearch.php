<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Verify;

/**
 * VerifySearch represents the model behind the search form of `common\models\Verify`.
 */
class VerifySearch extends Verify
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'date_birth', 'doc_date', 'doc_valid_to', 'postcode', 'date_issue', 'qr_code', 'user_id', 'sort', 'active', 'is_new', 'created_at', 'updated_at'], 'integer'],
            [['name', 'surname', 'second_name', 'doc_serial_num', 'doc_country', 'pass_scan', 'address', 'city', 'country_issue', 'pass_photo', 'address_invoice'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Verify::find()->where(['is_new' => true]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->orderBy(['id' => SORT_DESC]);
        $this->load($params);



        $query->andFilterWhere([
            'id' => $this->id,
            'date_birth' => $this->date_birth,
            'doc_date' => $this->doc_date,
            'doc_valid_to' => $this->doc_valid_to,
            'postcode' => $this->postcode,
            'date_issue' => $this->date_issue,
            'qr_code' => $this->qr_code,
            'user_id' => $this->user_id,
            'sort' => $this->sort,
            'active' => $this->active,
            'is_new' => $this->is_new,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'second_name', $this->second_name])
            ->andFilterWhere(['like', 'doc_serial_num', $this->doc_serial_num])
            ->andFilterWhere(['like', 'doc_country', $this->doc_country])
            ->andFilterWhere(['like', 'pass_scan', $this->pass_scan])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'country_issue', $this->country_issue])
            ->andFilterWhere(['like', 'pass_photo', $this->pass_photo])
            ->andFilterWhere(['like', 'address_invoice', $this->address_invoice]);

        return $dataProvider;
    }
}
