<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "langs".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $title
 * @property string|null $url
 * @property string|null $local
 * @property string|null $name
 * @property string $default
 */
class Langs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{langs}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'title', 'url', 'local', 'name', 'default'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'title' => 'Title',
            'url' => 'Url',
            'local' => 'Local',
            'name' => 'Name',
            'default' => 'Default',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\LangsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LangsQuery(get_called_class());
    }


    public static function getAllLanguages()
    {

       return ['en', 'ru'];
    }

}
