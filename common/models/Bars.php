<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bars".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $text
 * @property string|null $type
 * @property float|null $weight
 * @property int|null $cost
 * @property string|null $date
 * @property int|null $can_buy
 * @property int|null $sort
 * @property int|null $active
 */
class Bars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'weight'], 'string'],
            [['cost', 'can_buy', 'sort', 'active'], 'integer'],
            [['title', 'type', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'text' => 'Текст',
            'type' => 'Тип',
            'weight' => 'Вес',
            'cost' => 'Стоимость',
            'date' => 'Дата',
            'can_buy' => 'Can Buy',
            'sort' => 'Сортировка',
            'active' => 'Активность',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\BarsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BarsQuery(get_called_class());
    }
}
