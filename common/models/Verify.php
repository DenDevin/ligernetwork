<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\models\Files;
use yii\behaviors\TimestampBehavior;
use common\models\Profile;
use yii\helpers\FileHelper;

class Verify extends \yii\db\ActiveRecord
{

    public $accept;

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function tableName()
    {
        return 'verify';
    }


    public function rules()
    {
        return [
            [['accept'], 'required', 'requiredValue' => 1, 'message' => Yii::t('frontend.profile', 'accept')],
            [['date_birth', 'doc_date', 'doc_valid_to', 'qr_code', 'user_id', 'sort', 'active',  'postcode', 'date_issue', 'user_id'], 'integer'],
            [['is_new'], 'safe'],
            [['name', 'surname', 'second_name', 'doc_serial_num', 'doc_country', 'address', 'city', 'country_issue'], 'string', 'max' => 255],

            [['pass_scan', 'pass_photo', 'address_invoice'], 'safe'],
            [['pass_scan', 'pass_photo', 'address_invoice'], 'file', 'extensions' => 'jpg, gif, png'],
            [['pass_scan', 'pass_photo', 'address_invoice'], 'file', 'maxSize' => '200000'],




        ];
    }



    public function afterValidate()
    {
        parent::afterValidate();

        $file = new Files();

        if(isset($this->pass_scan) && !is_null($this->pass_scan))
        {
            $this->saveImage($file, 'pass_scan');
        }
        if(isset($this->pass_photo) && !is_null($this->pass_photo))
        {
            $this->saveImage($file, 'pass_photo');
        }
        if(isset($this->address_invoice) && !is_null($this->address_invoice))
        {
           $this->saveImage($file, 'address_invoice');
        }
        $this->setUserId();

    }

    public function setUserId()
    {
      $this->user_id = Yii::$app->user->identity->id;
    }


    public function saveImage($file, $image_type)
    {
        $this->$image_type = UploadedFile::getInstance($this, $image_type);
        if($this->$image_type)
        {
            $path = $file->preparePath($this->$image_type, 'image');
            $this->$image_type->saveAs($path);
            $this->$image_type = $file->saveFile($this->$image_type, Yii::$app->user->identity->id, $image_type);
        }
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'second_name' => 'Отчество',
            'date_birth' => 'Дата рождения',
            'doc_serial_num' => 'Серия и номер док-та',
            'doc_country' => 'Страна выдачи',
            'doc_date' => 'Дата выдачи',
            'doc_valid_to' => 'Действителен до',
            'pass_scan' => 'Цветной скан пасспорта',
            'pass_photo' => 'Фото с паспортом',
            'address_invoice' => 'Квитанция с паспортом',
            'address' => 'Адрес',
            'city' => 'Город',
            'postcode' => 'Почтовый код',
            'qr_code' => 'Qr Код',
            'user_id' => 'User ID',
            'sort' => 'Сортировка',
            'active' => 'Видимость',
            'is_new' => 'Is New',
        ];
    }
    public function deleteImages($verify)
    {
        if(!empty($verify->pass_scan))
           {
               unlink(Yii::getAlias('@images').'/'.$verify->pass_scan);
           }

           if(!empty($verify->pass_photo))
           {
               unlink(Yii::getAlias('@images').'/'.$verify->pass_photo);
           }

           if(!empty($verify->address_invoice))
           {
               unlink(Yii::getAlias('@images').'/'.$verify->address_invoice);
           }

    }

    public function isVerified($user_id)
    {
        $user = Profile::findOne(['user_id' => $user_id]);
        if($user)
        {
            return $user->is_verified ? true : false;
        }
    }


    public function verifyUser($user_id, $model)
    {
        $user = Profile::findOne(['user_id' => $user_id]);

        if($user)
        {
                $user->is_verified = true;
                if($user->save())
                {
                    $model->is_new = 0;
                    if($model->save(false))
                    {
                        Yii::$app->notification->sendVerifyMessage($user);
                        return true;
                    }

                }


        }
    }

    public static function find()
    {
        return new \common\models\query\VerifyQuery(get_called_class());
    }
}
