<?php

namespace common\models\User;

use Yii;
use paulzi\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\User\query\TreeQuery;
use common\models\User;
/**
 * This is the model class for table "user_tree".
 *
 * @property int $id
 * @property int|null $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $name
 */
class UserTree extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_tree';
    }

    public function behaviors() {
        return [
            [

                'class' => NestedSetsBehavior::class,
                 'treeAttribute' => 'tree',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    public function rules()
    {
        return [
            [['tree', 'lft', 'rgt', 'depth'], 'integer'],
            [['name'], 'string'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tree' => 'Tree',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'name' => 'Name',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\User\query\UserTreeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\User\query\UserTreeQuery(get_called_class());
    }

    public static function getTree($token)
    {
        $node = self::findOne(['name' => $token]);
        $childs = $node->getDescendants(1, false)->all();
        return $childs;

    }


    public static function getFirstLine($user)
    {
        $node = self::findOne(['name' => $user->user_token]);
        $childs = $node->getDescendants(1, false)->all();
        return $childs;
    }


    public function getUsername($token)
    {
        $user =  User::findOne(['user_token' => $token]);
        return $user->username;
    }



    public static function createRelation($parent, $child)
    {
        $parentOne = self::findOne(['name' => $parent->user_token]);
        if($parentOne)
        {
            $node = new self();
            $node->name = $child->user_token;
            $node->appendTo($parentOne)->save();
        }

        else
        {

            $parentOne = new UserTree(['name' => $parent->user_token]);
            if($parentOne->makeRoot()->save())
            {
                $node = new self();
                $node->name = $child->user_token;
                $node->appendTo($parentOne)->save();
            }


        }
    }


}
