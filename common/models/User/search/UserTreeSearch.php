<?php

namespace common\models\User\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User\UserTree;
use common\models\User;
use common\models\Countries;



class UserTreeSearch extends UserTree
{

    public $country;
    public $status;
    public $telegram;
    public $structure;

    public function rules()
    {
        return [
            [['id', 'tree', 'lft', 'rgt', 'depth'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
           'name' => Yii::t('frontend.team', 'user_login'),
           'country' => Yii::t('frontend.team', 'user_country'),
           'status' => Yii::t('frontend.team', 'user_status'),
           'telegram' => Yii::t('frontend.team', 'user_telegram'),
           'structure' => Yii::t('frontend.team', 'user_structure'),
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function searchFirstChildren($params, $token)
    {

        $node = self::findOne(['name' => $token]);
        $query = $node->getChildren(1);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 2
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tree' => $this->tree,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'depth' => $this->depth,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function search($params)
    {

        $query = UserTree::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tree' => $this->tree,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'depth' => $this->depth,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function getUserName($token)
    {
        return User::findOne(['user_token' => $token])->username;

    }


    public function getUserStatus($token)
    {
        return Yii::$app->useridentity->getPartnerStatus($token);
    }

    public function getUserCountry($token)
    {
        $country_code =  User::findOne(['user_token' => $token])->country;
        if($country_code)
        {
           foreach(Countries::getCountriesArray() as $key => $country)
           {
               if($country_code == $key)
               {
                   return $country;
               }
           }

        }

    }

    public function getUserTelegram($token)
    {
        return User::findOne(['user_token' => $token])->profile->telegram;

    }

    public function getUserStructure($token)
    {
        $user = User::findOne(['user_token' => $token]);

    }

}
