<?php

namespace common\models\User\query;

use paulzi\nestedsets\NestedSetsQueryTrait;


class TreeQuery extends \yii\db\ActiveQuery
{
  use NestedSetsQueryTrait;
}