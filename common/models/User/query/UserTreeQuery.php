<?php

namespace common\models\User\query;

/**
 * This is the ActiveQuery class for [[\common\models\User\UserTree]].
 *
 * @see \common\models\User\UserTree
 */
class UserTreeQuery extends \yii\db\ActiveQuery
{



    public function all($db = null)
    {
        return parent::all($db);
    }


    public function one($db = null)
    {
        return parent::one($db);
    }
}
