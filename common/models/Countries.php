<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $title
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'title'], 'string', 'max' => 255],
            [['sort', 'active'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код страны',
            'title' => 'Заголовок',
            'sort' => 'Порядок',
            'active' => 'Активность (видимость)',
        ];
    }

    public static function getAllCountries()
    {
        return ArrayHelper::map(
            self::findAll(
                [ 'active' => true ]
            ), 'code', 'code');
    }

    public static function getCountries()
    {

        return self::find()->
        where(['active' => true])->
        orderBy(['sort' => SORT_ASC])->
        asArray()->
        all();

    }


    public static function getCountriesArray()
    {

        return ArrayHelper::map(self::find()->
        where(['active' => true])->
        all(), 'code', 'title');

    }
}
