<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\models\Files;
use yii\helpers\FileHelper;


/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $text
 * @property string|null $type
 * @property int|null $bonus
 * @property int|null $cost
 * @property int|null $cost_type
 * @property string|null $date
 * @property int|null $can_buy
 * @property int|null $sort
 * @property int|null $active
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['bonus', 'cost', 'cost_type', 'can_buy', 'sort', 'active'], 'integer'],
            [['title', 'date'], 'string', 'max' => 255],
            [['type'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }


    public function afterValidate()
    {
        parent::afterValidate();

        $file = new Files();

        if(isset($this->type) && !is_null($this->type))
        {
            $this->saveImage($file, 'type');
        }


    }



    public function saveImage($file, $image_type)
    {
        $this->type = UploadedFile::getInstance($this, 'type');
        if($this->type)
        {
            $path = $file->preparePath($this->type, 'image');
            $this->type->saveAs($path);
            $this->type = $file->saveFile($this->type, Yii::$app->user->identity->id, $image_type);
        }
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'type' => 'Изображение события',
            'bonus' => 'Бонус',
            'cost' => 'Стоимость',
            'cost_type' => 'Тип стоимости',
            'date' => 'Дата и место проведения',
            'can_buy' => 'Доступно к покупке',
            'sort' => 'Сортировка',
            'active' => 'Активность',
        ];
    }


    public static function find()
    {
        return new \common\models\query\EventQuery(get_called_class());
    }




}
