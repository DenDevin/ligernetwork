<?php

namespace common\models;

use Yii;


class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }


    public function rules()
    {
        return [
            [['text'], 'string'],
            [['date', 'from_userid', 'to_userid', 'sort', 'active', 'is_new'], 'integer'],
            [['title', 'type'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'type' => 'Type',
            'date' => 'Date',
            'from_userid' => 'From Userid',
            'to_userid' => 'To Userid',
            'sort' => 'Sort',
            'active' => 'Active',
            'is_new' => 'Is New',
        ];
    }


    public static function find()
    {
        return new \common\models\query\NotificationQuery(get_called_class());
    }


    public function sendClarifyMessage($notification)
    {

        $notification->is_new = true;
        $notification->save(false);
        Yii::$app->session->setFlash('success', 'Уточнение верификации успешно отправлено!');

    }

}
