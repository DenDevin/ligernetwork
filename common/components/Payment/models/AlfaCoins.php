<?php

namespace common\components\Payment\models;

use Yii;

use common\models\Payment\UserBalance;
use GuzzleHttp\Exception\ClientException;
use yii\httpclient\Client;
use yii\web\Response;
use common\models\User;


class AlfaCoins {


    private $nameApp = 'Liger Network';
    private $passApp = 'Rossberg22';
    private $secretKey = '87706da6a4b87e106da65b165acab814';
    private $apiUrl = 'https://www.alfacoins.com/api';




    public function getApi()
    {

        $client = new Client();

        $res = $client->request('POST', 'https://www.alfacoins.com/api/create.json',
            [


                'headers' => [
                    'content-type' => 'application/json'

                ],
                'query' => [
                    'name' => 'Liger Network',
                    'secret_key' => $this->secretKey,
                    'password' => strtoupper(md5($this->passApp)),

                ],


            ]


            );


    }

    public function createOrder($post)
    {
       // Yii::$app->response->format = Response::FORMAT_JSON;
        $user_id = Yii::$app->user->identity->getId();
        $order_id = Yii::$app->security->generateRandomString(16);


        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->apiUrl.'/create.json')
            ->setFormat(Client::FORMAT_JSON)
            ->setData([
                'name' => $this->nameApp,
                'secret_key' => $this->secretKey,
                'password' => strtoupper(md5($this->passApp)),
                'type' => 'bitcoin',
                'amount' =>  $post['data']['cost'],
                'order_id' => $order_id,
                'currency' =>  'EUR',
                'description' => 'Payment #'.$order_id,
                'options' => [
                    'notificationURL' => 'https://liger.network/users/wallet/notification',
                    'redirectURL' => 'https://liger.network/users/wallet/callback',

                ]




            ])
            ->send();
            $responseData = $response->getData();
          if($responseData)
          {
              $responseData['user_id'] = $user_id;
              $responseData['order_id'] = $order_id;
              $responseData['order_summ_eur'] = $post['data']['cost'];
              $responseData['order_pure_summ_eur'] = $post['data']['cost_no_rent'];
              $responseData['order_type'] = 'replenishment';
              $responseData['order_system'] = 'alfacoins';
              return $responseData;
          }


    }


    public function getOrderStatus($order_id)
    {
        // Yii::$app->response->format = Response::FORMAT_JSON;
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->apiUrl.'/status.json')
            ->setFormat(Client::FORMAT_JSON)
            ->setData([
                'name' => $this->nameApp,
                'secret_key' => $this->secretKey,
                'password' => strtoupper(md5($this->passApp)),
                'txn_id' => $order_id





            ])
            ->send();
        $responseData = $response->getData();
        if($responseData)
        {
            return $responseData;
        }


    }


    public function createWithdraw($post)
    {
        $user = Yii::$app->user->identity;
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->apiUrl.'/bitsend.json')
            ->setFormat(Client::FORMAT_JSON)
            ->setData([
                'name' => $this->nameApp,
                'secret_key' => $this->secretKey,
                'password' => strtoupper(md5($this->passApp)),
                'type' => 'bitcoin',
                'amount' =>  $post['data']['amount'],
                'currency' =>  'EUR',
                'recipient_name' => $user->username,
                'recipient_email' => $user->email,
                'reference' => 'Withdraw',
                'options' => [
                    'address' => $post['data']['wallet'],

                ]

            ])
            ->send();
        $responseData = $response->getData();

        if($responseData)
        {
            $responseData['user_id'] = $user->id;
            $responseData['order_type'] = 'withdraw';
            $responseData['order_id'] = $responseData['id'];
            $responseData['order_summ_eur'] = $post['data']['amount'];
            $responseData['address'] = $post['data']['wallet'];
            $responseData['order_system'] = 'alfacoins';

            return $responseData;
        }
    }


    public function getOrderWithdrawStatus($order_id)
    {

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->apiUrl.'/bitsend_status.json')
            ->setFormat(Client::FORMAT_JSON)
            ->setData([
                'name' => $this->nameApp,
                'secret_key' => $this->secretKey,
                'password' => strtoupper(md5($this->passApp)),
                'bitsend_id' => $order_id

            ])
            ->send();
        $responseData = $response->getData();
        if($responseData)
        {
            return $responseData;
        }


    }




    public function createAdminWithdraw($transaction)
    {
        $user = User::findOne($transaction->user_id);
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($this->apiUrl.'/bitsend.json')
            ->setFormat(Client::FORMAT_JSON)
            ->setData([
                'name' => $this->nameApp,
                'secret_key' => $this->secretKey,
                'password' => strtoupper(md5($this->passApp)),
                'type' => 'bitcoin',
                'amount' =>  $this->convertToEuro($transaction->payment_summ_eur),
                'currency' =>  'EUR',
                'recipient_name' => $user->username,
                'recipient_email' => $user->email,
                'reference' => 'Withdraw',
                'options' => [
                    'address' => $transaction->reciever_number,

                ]

            ])
            ->send();
        $responseData = $response->getData();
        return $responseData;

    }




    public function convertToEuro($cents)
    {
        $balance = $cents / 100;
        return $balance;

    }

    public function convertToCent($euro)
    {
        $balance = $euro * 100;

    }



}