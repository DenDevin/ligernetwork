<?php

namespace common\components\Notify;

use yii\base\Component;
use common\components\Notify\models\Notification;
use Yii;

class Notify extends Component {

    public $model;


    public function notifyCount()
    {

       $notifiesCount = Notification::find()->
       where([
           'to_userid' => Yii::$app->user->identity->id,
           'is_new' => true])->
       count();
       return $notifiesCount;
    }

    public function notifyArray()
    {

        $notifiesArray = Notification::find()->
        where([
            'to_userid' => Yii::$app->user->identity->id,
            'active' => true])->
        orderBy(['id' => SORT_DESC])->
        limit(10)->
        all();
        return $notifiesArray;
    }


    public function sendMessage($user_id, $message, $type)
    {

        if($user_id == 'superadmin')
        {
            $admins = Yii::$app->authManager->getUserIdsByRole('superadmin');
            if($admins && count($admins) > 0)
            {
                foreach($admins as $admin)
                {
                    $notify = new Notification([
                        'to_userid' => $admin,
                        'title' => $message['title'],
                        'text' => $message['text'],
                        'type' => $type,

                    ]);

                    $notify->save(false);
                }
            }
        }

        else
            {
                $notify = new Notification([
                    'to_userid' => $user_id,
                    'title' => $message['title'],
                    'text' => $message['text'],
                    'type' => $type,

                ]);
                $notify->save(false);
            }







    }



    public function sendMessageByToken($token, $message, $type)
    {


        if($user_id == 'superadmin')
        {
            $admins = Yii::$app->authManager->getUserIdsByRole('superadmin');
            if($admins && count($admins) > 0)
            {
                foreach($admins as $admin)
                {
                    $notify = new Notification([
                        'to_userid' => $admin,
                        'title' => $message['title'],
                        'text' => $message['text'],
                        'type' => $type,

                    ]);

                    $notify->save(false);
                }
            }
        }

        else
        {
            $notify = new Notification([
                'to_userid' => $user_id,
                'title' => $message['title'],
                'text' => $message['text'],
                'type' => $type,

            ]);
            $notify->save(false);
        }







    }





    public function resetNewMessages()
    {

        $notifies = Notification::find()->
        where([
            'to_userid' => Yii::$app->user->identity->id,
            'is_new' => true])->
       all();
       // print_r($notifies); die;
        foreach($notifies as $notify)
        {
            $notify->is_new = false;
            $notify->save(false);

        }

    }


    public function sendVerifyMessage($profile)
    {
        $notification = new Notification();
        Yii::$app->language = (!empty($profile->user->locale) ? $profile->user->locale : 'en');
        $notification->title = Yii::t('frontend.message', 'verify_done_title');
        $notification->text = Yii::t('frontend.message', 'verify_done_text');
        $notification->to_userid = $profile->user_id;
        $notification->type = 'success';
        $notification->is_new = true;
        $notification->save(false);
        Yii::$app->session->setFlash('success', 'Подтверждение верификации успешно отправлено!');
    }





}