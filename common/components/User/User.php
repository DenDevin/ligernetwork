<?php

namespace common\components\User;

use yii\base\Component;
use Yii;
use common\models\Package\UserPackage;
use common\models\Package\UserPackageStat;
use common\models\Payment\UserTransaction;
use common\components\User\models\UserModel as BaseUser;
use common\models\User as CommonUser;
use common\models\User\UserTree;
use common\models\Payment\UserBalance;
use backend\modules\marketing\models\UserMarketing;

class User extends Component

{




    public $identity;
    public $user;
    public $id;

    public function __construct(BaseUser $identity)
    {
        $this->identity = $identity;
        $this->user = Yii::$app->user->identity;
        $this->id = Yii::$app->user->identity->getId();
    }


    public function isActiveClient()
    {

        $user = Yii::$app->getUser();
        $invoice = UserPackage::find()->
        where(['package_id' => UserPackage::PACKAGE_CLIENT])->
        andWhere(['user_id' => $this->id])->
        one();

        if($invoice->avaliable_till > time())
        {
            return true;
        }

        else
        {
            return false;
        }



    }


    public function countActivePartners()
    {
        $count = [];
        $node = UserTree::findOne(['name' => $this->user->user_token]);
        $childs = $node->getDescendants(1, false)->all();

        if(count($childs) > 0)
        {
            foreach($childs as $child)
            {
               if(self::isActivePartnerStatic($child->name))
               {
                  $count[] = $child->name;
               }
            }
        }

        return $count;
    }


    public function findActivePartner($user_token)
    {

        $user = CommonUser::find()->
        where(['user_token' => $user_token])->
        orWhere(['username' => $user_token])->
        one();

        $packageStat = UserPackage::find()->
        where(['package_id' => UserPackage::PACKAGE_BUSINESS])->
        andWhere(['user_id' => $user->id])->
        one();

        if($packageStat)
        {
            if($packageStat->avaliable_till > time())
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }


    }

    public function isActivePartnerByToken($token)
    {

        $user = CommonUser::findOne(['user_token' => $token]);
        $packageStat = UserPackage::find()->
        where(['package_id' => UserPackage::PACKAGE_BUSINESS])->
        andWhere(['user_id' => $user->id])->
        one();

        if($packageStat->avaliable_till > time())
        {
            return true;
        }

        else
        {
            return false;
        }

    }



    public static function isActivePartnerStatic($token)
    {

        $user = CommonUser::findOne(['user_token' => $token]);
        $packageStat = UserPackage::find()->
        where(['package_id' => UserPackage::PACKAGE_BUSINESS])->
        andWhere(['user_id' => $user->id])->
        one();

        if($packageStat->avaliable_till > time())
        {
            return true;
        }

        else
        {
            return false;
        }

    }





    public function isActivePartner()
    {


                $packageStat = UserPackage::find()->
                where(['package_id' => UserPackage::PACKAGE_BUSINESS])->
                andWhere(['user_id' => $this->id])->
                one();

                if($packageStat->avaliable_till > time())
                {
                    return true;
                }

                else
                {
                    return false;
                }

    }


    public function getProductCirculate()
    {
        $user = Yii::$app->getUser();



    }


    public function getPartnerStatus($token)
    {
        $user = CommonUser::findOne(['user_token' => $token]);



    }



    public function getAllPartnersCount()
    {


        $partners = [];
        $user = CommonUser::findOne(['user_token' => Yii::$app->user->identity->user_token]);
        $node = UserTree::findOne(['name' => $user->user_token]);
        $childs = $node->getChildren()->all();

        foreach($childs as $child)
        {
           if($this->isActivePartnerByToken($child->name))
           {
               $partners[] = $child;
           }

        }

        return count($partners);

    }


    public function getAllClientsCount()
    {


        $clients = [];
        $user = CommonUser::findOne(['user_token' => Yii::$app->user->identity->user_token]);
        $node = UserTree::findOne(['name' => $user->user_token]);
        $childs = $node->getChildren()->all();

        foreach($childs as $child)
        {
            if(!$this->isActivePartnerByToken($child->name))
            {
                $clients[] = $child;
            }

        }

        return count($clients);

    }




    public function getMonthClientsCount()
    {


        $clients = [];
        $user = CommonUser::findOne(['user_token' => Yii::$app->user->identity->user_token]);
        $node = UserTree::findOne(['name' => $user->user_token]);
        $childs = $node->getChildren()->all();

        foreach($childs as $child)
        {
            if(!$this->isActivePartnerByToken($child->name))
            {
                $clients[] = $child;
            }

        }

        return count($clients);

    }




    public function getPartnersCountPerMonth($token)
    {
        $user = CommonUser::findOne(['user_token' => $token]);

    }

    public function getUserPartnerStatus()
    {

        $balance = UserBalance::getUserBalanceByToken(Yii::$app->user->identity->user_token);
        $statuses = UserMarketing::getMarketingStatuses();
        $product_return = $this->convertToEuro($balance->product_return);
        $cnt_arr = count($statuses);
        $isActivePartner = $this->isActivePartner();

        if(!$isActivePartner)
        {
            return 'client';
        }

        foreach($statuses as $key => $status)
        {

            if  ($product_return >= $status['product_return'] &&
               $product_return < $statuses[$key+1]['product_return'])
          {
              return $status['title'];
          }


            elseif  ($product_return < $status['product_return'] && $isActivePartner)
            {
                return 'partner';
            }

            elseif  (
                $product_return >= $statuses[$cnt_arr-1]['product_return']

            )
            {
                return $statuses[$cnt_arr-1]['title'];
            }


            else
          {
              continue;
          }

        }

       exit;



    }


    public function getUserPreviousStatus()
    {
        $balance = UserBalance::getUserBalanceByToken(Yii::$app->user->identity->user_token);
        $statuses = UserMarketing::getMarketingStatuses();
        $product_return = $this->convertToEuro($balance->product_return);

        $current_status = $this->getUserPartnerStatus();

        foreach($statuses as $key => $status)
        {

            if($status['title'] == $current_status)
            {

                if($key == 0)
                {
                   return $current_status;
                }
                else
                    {
                        return $statuses[$key-1];
                    }

            }

        }

    }


    public function getUserNextStatus()
    {
        $balance = UserBalance::getUserBalanceByToken(Yii::$app->user->identity->user_token);
        $statuses = UserMarketing::getMarketingStatuses();
        $product_return = $this->convertToEuro($balance->product_return);

        $current_status = $this->getUserPartnerStatus();

        foreach($statuses as $key => $status)
        {

            if($status['title'] == $current_status)
            {

                if($key == $statuses[count($statuses)-1])
                {
                    return $current_status;
                }
                else
                {
                    return $statuses[$key+1];
                }

            }

        }

    }


    public function getPrevProductReturn($status)
    {
        $statuses = UserMarketing::getMarketingStatuses();
        foreach($statuses as $key => $stat)
        {

            if($stat['title'] == $status)
            {
                return $stat['product_return'];
            }

        }
    }

    public function issetFirstPay()
    {
        $user = Yii::$app->user->identity;
        $stat = UserPackageStat::findOne([
           'user_id' => $user->id,
           'package_id' => UserPackage::PACKAGE_BUSINESS
       ]);
        if($stat)
        {
            if($stat->pay_count > 0)
            {
                return true;
            }
            else
                {
                    return false;
                }
        }

        else
        {
            return false;
        }

    }


    public function convertToCent($euro)
    {
        return $euro * 100;
    }

    public function convertToEuro($cent)
    {
        return $cent / 100;
    }



}