<?php
namespace common\components\User\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\Package\UserPackage;
use dektrium\user\models\User as BaseUser;


class UserModel extends BaseUser implements IdentityInterface
{

    public static function tableName()
    {
        return '{{%user}}';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['user_token'], 'string'],
        ];
    }




    public function findUserByToken($token, $model)
    {

        $this->user_token = $token;
        if($this->validate('user_token'))
        {

            $partner = self::findOne(['user_token' => $this->user_token]);

            if(isset($partner))
            {
                $model->user_token = $partner->username;
                $model->refferal_id = $partner->id;
                return $model;
            }
            else
            {
                return false;
            }

        }

    }

    public function findUserByLogin($login)
    {
        $this->username = $login;
        if($this->validate('username'))
        {
            $partner = self::findOne(['username' => $login]);

            if($partner)
            {

                return $partner->username;
            }
            else
            {

                return '';
            }
        }

        else
        {
            return '';
        }


    }


    public function attemptConfirmation($code)
    {
        $token = $this->finder->findTokenByParams($this->id, $code, Token::TYPE_CONFIRMATION);

        if ($token instanceof Token && !$token->isExpired) {
            $token->delete();
            if (($success = $this->confirm())) {
                /* Yii::$app->user->login($this, $this->module->rememberFor); */
                $message = \Yii::t('user', 'Thank you, registration is now complete.');
            } else {
                $message = \Yii::t('user', 'Something went wrong and your account has not been confirmed.');
            }
        } else {
            $success = false;
            $message = \Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.');
        }

        \Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);

        return $success;
    }



}