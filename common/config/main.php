<?php
return [
    'name' => 'Liger Network',
   // 'language' => 'ru-RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'useridentity' => [

            'class' => 'common\components\User\User'

        ],
        'notification' => [
            'class' => 'common\components\Notify\Notify'
        ],

        'payment' => [
            'class' => 'common\components\Payment\Payment'
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles'    => []
        ],

        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => '',
        ],

        'assetManager' => [
            'bundles' => [


            ]

        ],

         'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                         'frontend' => 'app.php',
                         'frontend.users' => 'frontend.users.php',
                         'frontend.wallet' => 'frontend.wallet.php',
                    ],
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'backend' => 'app.php',
                        'backend.countries' => 'backend.countries.php',
                    ],
                ],
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ]
            ],
        ],

        'view' => [
            'theme' => [
                'basePath' => '@app/themes/ligertheme/backend/web',
                'baseUrl' => '@app/backend/web',
                'pathMap' => [
                    '@backend/modules' => '@themes/ligertheme/backend/modules',
                    '@backend/views' => '@themes/ligertheme/backend/views',
                    '@dektrium/user/views' => '@themes/ligertheme/backend/modules/adminuser/views',
                    '@dektrium/rbac/views' => '@themes/ligertheme/backend/modules/adminrole/views',

                ],
            ],
        ],

    ],
    'modules' => [

        'billing' => [
                'class' => 'backend\modules\billing\Module',
        ],

        'admin' => [
                'class' => 'backend\modules\admin\Module',
        ],
        'adminuser' => [
                'class' => 'backend\modules\adminuser\Module',
        ],

        'adminrole' => [
            'class' => 'backend\modules\adminrole\Module',
        ],

        'marketing' => [
            'class' => 'backend\modules\marketing\Module',
        ],

        'users' => [
            'class' => 'frontend\modules\users\Module',
        ],

        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'enableGeneratingPassword' => false,
            'enablePasswordRecovery' => true,
            'enableConfirmation' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['Den', 'Pavel'],
            'modelMap' => [
              'User' => 'common\models\User',
            ],
            'controllerMap' => [
                'admin' => 'backend\modules\adminuser\controllers\AdminController',
                'registration' => 'frontend\modules\users\controllers\RegistrationController',
                'security' => [
                    'class' => \dektrium\user\controllers\SecurityController::className(),
                    'on ' . \dektrium\user\controllers\SecurityController::EVENT_AFTER_LOGIN => function ($e) {
                        Yii::$app->response->redirect(array('/users/office'))->send();
                        Yii::$app->end();
                    },
                ],
            ]

        ],

        'rbac' => [
           'class' =>  'dektrium\rbac\RbacWebModule',
            'controllerMap' => [
                'assignment' => 'backend\modules\adminrole\controllers\AssignmentController',
                'role' => 'backend\modules\adminrole\controllers\RoleController',
                'rule' => 'backend\modules\adminrole\controllers\RuleController',
            ],

        ],


        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*']
        ],


        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*']
        ],



    ],
];
