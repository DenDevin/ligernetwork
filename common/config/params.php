


<?php



return [

    'adminEmail' => 'mail@liger.network',
    'supportEmail' => 'mail@liger.network',
    'senderEmail' => 'mail@liger.network',
    'senderName' => 'Liger Network',
    'user.passwordResetTokenExpire' => 3600,

    'storagePath' => '@frontend/web/uploads/',
    'uploadVideoPath' => '@frontend/web/uploads/video/',
    'uploadImagePath' => '@frontend/web/uploads/image/',
    'uploadDocPath' => '@frontend/web/uploads/document/',

    'uploadVideoUrl' =>  Yii::$app->urlManager->baseUrl . '/uploads/video/',
    'uploadImageUrl' =>  Yii::$app->urlManager->baseUrl . '/uploads/image/',
    'uploadDocUrl' =>  Yii::$app->urlManager->baseUrl . '/uploads/document/',

];
