<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@themes', dirname(dirname(__DIR__)) . '/themes');
Yii::setAlias('@images', dirname(dirname(__DIR__)) . '/frontend/web/uploads/image');
Yii::setAlias('@videos', dirname(dirname(__DIR__)) . '/frontend/web/uploads/video');
Yii::setAlias('@documents', dirname(dirname(__DIR__)) . '/frontend/web/uploads/document');
