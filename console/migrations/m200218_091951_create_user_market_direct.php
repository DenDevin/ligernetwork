<?php

use yii\db\Migration;

/**
 * Class m200218_091951_create_user_market_direct
 */
class m200218_091951_create_user_market_direct extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%user_marketing_direct}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'title' => $this->string(),
            'first_pay' => $this->integer(),
            'level' => $this->integer(),
            'first_pay_percent' => $this->integer(),
            'next_pay' => $this->integer(),
            'next_pay_percent' => $this->integer(),
            'active' => $this->boolean(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%user_marketing_direct}}');
    }

}
