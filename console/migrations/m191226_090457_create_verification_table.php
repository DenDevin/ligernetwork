<?php

use yii\db\Migration;


class m191226_090457_create_verification_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%verify}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null(),
            'surname' => $this->string()->null(),
            'second_name' => $this->string()->null(),
            'date_birth' => $this->integer()->null(),
            'doc_serial_num' => $this->string()->null(),
            'doc_country' => $this->string()->null(),
            'doc_date' => $this->integer()->null(),
            'doc_valid_to' => $this->integer()->null(),
            'pass_scan' => $this->string()->null(),
            'address' => $this->string()->null(),
            'city' => $this->string()->null(),
            'postcode' => $this->integer()->null(),
            'country_issue' => $this->string()->null(),
            'date_issue' => $this->integer()->null(),
            'pass_photo' => $this->string()->null(),
            'address_invoice' => $this->string()->null(),
            'qr_code' => $this->integer()->null(),
            'user_id' => $this->integer()->null(),
            'sort' => $this->integer()->null(),
            'active' => $this->boolean()->defaultValue(1),
            'is_new' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),

        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%verify}}');
    }
}
