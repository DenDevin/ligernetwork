<?php

use yii\db\Migration;

/**
 * Class m191216_224433_add_countries_table
 */
class m191216_224433_add_countries_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%countries}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->null(),
            'title' => $this->string()->null(),
            'sort' => $this->integer()->null(),
            'active' => $this->integer()->notNull()->defaultValue(1),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%countries}}');
    }


}
