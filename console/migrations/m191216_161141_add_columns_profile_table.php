<?php

use yii\db\Migration;

/**
 * Class m191216_161141_add_columns_profile_table
 */
class m191216_161141_add_columns_profile_table extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'lang', $this->string(16)->defaultValue(null));
        $this->addColumn('{{%profile}}', 'current_status', $this->integer()->defaultValue(null));
        $this->addColumn('{{%profile}}', 'verify_status', $this->integer()->defaultValue(null));
        $this->addColumn('{{%profile}}', 'partner_status', $this->integer()->defaultValue(null));
    }


    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'lang');
        $this->dropColumn('{{%user}}', 'current_status');
        $this->dropColumn('{{%user}}', 'verify_status');
        $this->dropColumn('{{%user}}', 'partner_status');
    }


}
