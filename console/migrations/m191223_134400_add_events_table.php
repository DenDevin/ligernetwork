<?php

use yii\db\Migration;

/**
 * Class m191223_134400_add_events_table
 */
class m191223_134400_add_events_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->null(),
            'text' => $this->text()->null(),
            'type' => $this->string()->null(),
            'bonus' => $this->integer()->null(),
            'cost' => $this->integer()->null(),
            'cost_type' => $this->integer()->null(),
            'date' => $this->string()->null(),
            'can_buy' => $this->boolean()->defaultValue(1),
            'sort' => $this->integer()->null(),
            'active' => $this->boolean()->defaultValue(1),

        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%event}}');
    }
}
