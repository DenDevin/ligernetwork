<?php

use yii\db\Migration;

/**
 * Class m191224_113656_add_columns_profile_table
 */
class m191224_113656_add_columns_profile_table extends Migration
{
    public function safeUp()
    {

        $this->addColumn('{{%profile}}', 'surname', $this->string(64)->null());
        $this->addColumn('{{%profile}}', 'second_name', $this->string(64)->null());
        $this->addColumn('{{%profile}}', 'postal_index', $this->string(32)->null());
        $this->addColumn('{{%profile}}', 'doc_serial_num', $this->string()->null());
        $this->addColumn('{{%profile}}', 'doc_country', $this->string()->null());
        $this->addColumn('{{%profile}}', 'doc_date', $this->integer());
        $this->addColumn('{{%profile}}', 'doc_valid_to', $this->integer());
        $this->addColumn('{{%profile}}', 'pass_scan', $this->string()->null());
        $this->addColumn('{{%profile}}', 'pass_photo', $this->string()->null());
        $this->addColumn('{{%profile}}', 'address_invoice', $this->string()->null());
        $this->addColumn('{{%profile}}', 'phone', $this->string(32)->null());
        $this->addColumn('{{%profile}}', 'telegram', $this->string(32)->null());
        $this->addColumn('{{%profile}}', 'skype', $this->string(32)->null());
        $this->addColumn('{{%profile}}', 'date_birth', $this->integer());
        $this->addColumn('{{%profile}}', 'is_verified', $this->boolean()->defaultValue(0));

    }


    public function safeDown()
    {


        $this->dropColumn('{{%profile}}', 'surname');
        $this->dropColumn('{{%profile}}', 'second_name');
        $this->dropColumn('{{%profile}}', 'postal_index');
        $this->dropColumn('{{%profile}}', 'doc_serial_num');
        $this->dropColumn('{{%profile}}', 'doc_country');
        $this->dropColumn('{{%profile}}', 'doc_date');
        $this->dropColumn('{{%profile}}', 'doc_valid_to');
        $this->dropColumn('{{%profile}}', 'pass_scan');
        $this->dropColumn('{{%profile}}', 'pass_photo');
        $this->dropColumn('{{%profile}}', 'address_invoice');
        $this->dropColumn('{{%profile}}', 'phone');
        $this->dropColumn('{{%profile}}', 'telegram');
        $this->dropColumn('{{%profile}}', 'skype');
        $this->dropColumn('{{%profile}}', 'date_birth');
        $this->dropColumn('{{%profile}}', 'is_verified');
    }
}
