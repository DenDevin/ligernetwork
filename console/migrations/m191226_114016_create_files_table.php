<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%files}}`.
 */
class m191226_114016_create_files_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%files}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'size' => $this->string(),
            'ext' => $this->string(),
            'unit' => $this->string(),
            'url' => $this->string(),
            'description' => $this->text(),
            'parent_id' => $this->integer(),
            'created_by' => $this->integer(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),


        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%files}}');
    }
}
