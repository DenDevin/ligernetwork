<?php

use yii\db\Migration;

/**
 * Class m191216_210210_add_columns_user_table
 */
class m191216_210210_add_columns_user_table extends Migration
{

    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'user_token', $this->string(32)->defaultValue(null));
        $this->addColumn('{{%user}}', 'country', $this->string(32)->defaultValue(null));
        $this->addColumn('{{%user}}', 'refferal_id', $this->integer()->defaultValue(null));
        $this->addColumn('{{%user}}', 'locale', $this->string(32)->defaultValue(null));

    }


    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'user_token');
        $this->dropColumn('{{%user}}', 'country');
        $this->dropColumn('{{%user}}', 'refferal_id');
        $this->dropColumn('{{%user}}', 'locale');
    }

}
