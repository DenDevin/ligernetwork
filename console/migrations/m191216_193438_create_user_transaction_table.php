<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_transaction}}`.
 */
class m191216_193438_create_user_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_transaction}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'payment_type' => $this->string(),
            'transaction_id' => $this->integer()->null(),
            'payment_title' => $this->string(),
            'invoice_link' => $this->string(),
            'payment_summ_btc' => $this->decimal(10, 8),
            'payment_summ_eur' => $this->integer()->null(),
            'payment_summ_eur' => $this->integer()->null(),
            'payment_summ_pure_eur' => $this->integer()->null(),
            'payment_system' => $this->integer()->null(),
            'payment_status' => $this->integer()->null(),
            'payment_bank' => $this->string()->defaultValue(null),
            'routing' => $this->string()->defaultValue(null),
            'account_number' => $this->string()->defaultValue(null),
            'account_type' => $this->string()->defaultValue(null),
            'sender_number' => $this->string()->defaultValue(null),
            'reciever_number' => $this->string()->defaultValue(null),
            'sender_id' => $this->integer()->null(),
            'reciever_id' => $this->integer()->null(),
            'benefitiary_name' => $this->string()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_transaction}}');
    }
}
