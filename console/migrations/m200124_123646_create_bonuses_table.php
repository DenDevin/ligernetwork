<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_bonuses}}`.
 */
class m200124_123646_create_bonuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_bonuses}}', [
            'id' => $this->primaryKey(),
            'bonus_id' => $this->integer(),
            'min' => $this->integer(),
            'max' => $this->integer(),
            'amount' => $this->integer(),
            'data' => $this->text(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_bonuses}}');
    }
}
