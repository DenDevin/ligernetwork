<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_balance}}`.
 */
class m191216_162001_create_user_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_balance}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'user_token' => $this->string()->notNull(),
            'dm'   => $this->integer()->notNull(),
            'euro' => $this->integer()->null(),
            'income' => $this->integer()->null(),
            'product_return' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_balance}}');
    }
}
