<?php

use yii\db\Migration;

/**
 * Class m191223_135016_add_bars_type_table
 */
class m191223_135016_add_bars_type_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%bars}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->null(),
            'text' => $this->text()->null(),
            'type' => $this->string()->null(),
            'weight' => $this->string()->null(),
            'cost' => $this->integer()->null(),
            'date' => $this->string()->null(),
            'can_buy' => $this->boolean()->defaultValue(1),
            'sort' => $this->integer()->null(),
            'active' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%bars}}');
    }
}
