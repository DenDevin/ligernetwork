<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_package}}`.
 */
class m200118_152932_create_user_package_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%user_package}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'package_id' => $this->integer(),
            'avaliable_till' => $this->integer()->notNull(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%user_package}}');
    }
}
