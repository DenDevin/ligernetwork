<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_system}}`.
 */
class m200124_122145_create_payment_system_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_system}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'application_title' => $this->string(),
            'pass' => $this->string(),
            'secret_key' => $this->string(),
            'currency_type' => $this->string(),
            'data' => $this->text(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_system}}');
    }
}
