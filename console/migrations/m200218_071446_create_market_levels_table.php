<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%market_levels}}`.
 */
class m200218_071446_create_market_levels_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_marketing_levels}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'level_id' => $this->integer(),
            'bonus' => $this->integer(),
            'percent' => $this->integer(),
            'active' => $this->boolean(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_marketing_levels}}');
    }
}
