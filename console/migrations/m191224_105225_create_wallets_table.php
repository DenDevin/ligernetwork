<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%wallets}}`.
 */
class m191224_105225_create_wallets_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%wallets}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->null(),
            'text' => $this->text()->null(),
            'type' => $this->string()->null(),
            'currency' => $this->double()->null(),
            'date' => $this->string()->null(),
            'can_buy' => $this->boolean()->defaultValue(1),
            'can_sell' => $this->boolean()->defaultValue(1),
            'user_id' => $this->integer()->null(),
            'sort' => $this->integer()->null(),
            'active' => $this->boolean()->defaultValue(1),

        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%wallets}}');
    }
}
