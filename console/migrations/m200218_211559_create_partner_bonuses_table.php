<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_bonuses}}`.
 */
class m200218_211559_create_partner_bonuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_partner_bonuses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'partners_count' => $this->integer(),
            'level' => $this->integer(),
            'amount' => $this->integer(),
            'data' => $this->text(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_partner_bonuses}}');
    }
}
