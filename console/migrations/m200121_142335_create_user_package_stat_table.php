<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_package_stat}}`.
 */
class m200121_142335_create_user_package_stat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_package_stat}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'package_id' => $this->integer(),
            'bonus_count' => $this->integer(),
            'bonus_type' => $this->integer(),
            'avaliable_till' => $this->integer(),
            'title' => $this->string(),
            'data' => $this->text(),
            'pay_count' => $this->integer(),
            'missing' => $this->integer()->notNull(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_package_stat}}');
    }
}
