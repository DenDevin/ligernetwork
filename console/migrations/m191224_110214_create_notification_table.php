<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%notification}}`.
 */
class m191224_110214_create_notification_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->null(),
            'text' => $this->text()->null(),
            'type' => $this->string()->null(),
            'date' => $this->integer()->null(),
            'from_userid' => $this->integer()->null(),
            'to_userid' => $this->integer()->null(),
            'sort' => $this->integer()->null(),
            'active' => $this->boolean()->defaultValue(1),
            'is_new' => $this->boolean()->defaultValue(1),

        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%notification}}');
    }
}
