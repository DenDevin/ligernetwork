<?php

use yii\db\Migration;

/**
 * Class m191221_220635_add_status_table
 */
class m191221_220635_add_package_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%package}}', [
            'id' => $this->primaryKey(),
            'code'    => $this->string()->null(),
            'title' => $this->string()->null(),
            'type' => $this->string()->null(),
            'turnover' => $this->integer()->null(),
            'bonus' => $this->integer()->null(),
            'cost' => $this->integer()->null(),
            'term' => $this->integer()->null(),
            'pay_term' => $this->string()->null(),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%package}}');
    }
}
