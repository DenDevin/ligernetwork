<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_marketing}}`.
 */
class m200210_104958_create_user_marketing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_marketing}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'product_return' => $this->integer()->notNull()->defaultValue(0),
            'income' => $this->integer()->notNull()->defaultValue(0),
            'leader_bonus' => $this->integer()->notNull()->defaultValue(0),
            'month_income' => $this->integer()->notNull()->defaultValue(0),
            'data' => $this->text(),
            'active' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_marketing}}');
    }
}
