<?php

use yii\db\Migration;

/**
 * Class m200218_091845_create_user_marketing_team
 */
class m200218_091845_create_user_marketing_team extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_marketing_team}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'title' => $this->string(),
            'level' => $this->integer(),
            'bonus' => $this->integer(),
            'percent' => $this->integer(),
            'active' => $this->boolean(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_marketing_team}}');
    }

}
