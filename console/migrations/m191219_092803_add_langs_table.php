<?php

use yii\db\Migration;

/**
 * Class m191219_092803_add_langs_table
 */
class m191219_092803_add_langs_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%langs}}', [
            'id' => $this->primaryKey(),
            'code'    => $this->string()->null(),
            'title' => $this->string()->null(),
            'url' => $this->string()->null(),
            'local' => $this->string()->null(),
            'name' => $this->string()->null(),
            'default' => $this->string()->notNull()->defaultValue('en'),
        ]);
    }


    public function safeDown()
    {
        $this->dropTable('{{%langs}}');
    }

}
