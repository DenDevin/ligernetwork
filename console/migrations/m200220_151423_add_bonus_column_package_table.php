<?php

use yii\db\Migration;

/**
 * Class m200220_151423_add_bonus_column_package_table
 */
class m200220_151423_add_bonus_column_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('package', 'direct_bonus', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('package', 'direct_bonus');
    }


}
