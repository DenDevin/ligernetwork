<?php

use yii\db\Migration;


/**
 * Handles the creation of table `{{%users_tree}}`.
 */
class m200131_141637_create_users_tree_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_tree}}', [

            'id' => $this->primaryKey(),
            'tree' => $this->integer()->null(),
            'lft'   => $this->integer()->notNull(),
            'rgt'   => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'name'  => $this->string()->null(),

        ], $tableOptions);
        $this->createIndex('lft', '{{%user_tree}}', ['tree', 'lft', 'rgt']);
        $this->createIndex('rgt', '{{%user_tree}}', ['tree', 'rgt']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_tree}}');
    }
}
