<?php


return [


    'paysuccess' => 'Payment was successfully!',
    'transexpired' => 'Payment time was exceed!',
    'withdraw_nomoney' => 'There is not enough money in your account to withdraw!',
    'withdraw_success' => 'Transaction completed successfully! Your transfer has been sent for processing!',
    'nomoney' => 'No money for payment!',
    'packageactive' => 'Package already active!',
    'payerror' => 'Payment Error! There is not enough money on the balance!',
    'your_balance_replenished_title' => 'Your balance has been replenished!',
    'your_balance_replenished_text' => 'The system confirmed your transaction, as a result of which your account was replenished!',
    'your_balance_was_payed_title' => 'You paid the payment from the balance!',
    'your_balance_was_payed_text' => 'The system confirmed your transaction, as a result of which you paid the payment from the balance!',
    'your_balance_withdraw_title' => 'The withdrawal was successful',
    'your_balance_withdraw_text' => 'The system confirmed your transaction and the money was successfully withdrawn',
    'withdraw_not_enable_title' => 'Withdraw not enable!',
    'withdraw_not_enable_text' => 'It is not possible to withdraw funds: there is not enough money in your account to conduct a transaction!',
    'admin_withdraw_message_title' => 'The user of the system was invoiced to withdraw funds. Confirm or reject the transaction in the admin panel!',
    'admin_withdraw_message_text' => 'The user of the system was invoiced to withdraw funds. Confirm or reject the transaction in the admin panel!',

];