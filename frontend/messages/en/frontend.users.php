<?php


return [

	// Login

    'entrance' => 'Sign in',
    'nickname' => 'Login',
    'password' => 'Password',
    'log_in' => 'Sign in',
    'forgot_password' => 'Forgot password?',
    'register' => 'Sign up',
    'resend' => 'Resend confinmation',

    // Registration

    'registration' => 'Sign up',
    'partner_login' => 'Partner login',
    'your_login' => 'Your login',
    'email' => 'E-mail',
    'choose_country' => 'Choose country',
    'register_go' => 'Sign up',
    'ru_country' => 'Russia',
    'us_country' => 'USA',
    'uk_country' => 'Ukraine',
    'de_country' => 'Germany',
    'tr_country' => 'Turkey',
    'button_login' => 'Have account? Sign in!',


    // users/forgot

    'recovery_password' => 'Password recovery',
    'continue' => 'Continue',

    // users/resend

    'resend_request' => 'Resend confirmation',
    'user_token_not_exist' => 'User not exist',
];