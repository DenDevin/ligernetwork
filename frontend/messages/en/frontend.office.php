<?php


return [

    // office/index

    'my_account' => 'My Profile',
    'adminpanel' => 'Adminpanel',
    'logout' => 'Logout',
    'main' => 'Main',
    'wallet' => 'Wallet',
    'marketplace' => 'Marketplace',
    'my_purchases' => 'My Purchases',
    'my_team' => 'My Team',
    'news' => 'News',
    'profile' => 'Profile',
    'faq' => 'FAQ',

    'your_status' => 'Your status',
    'not_verified' => 'Your account <span class = "medium"> has not been verified </span>',
    'pass_verification' => 'Verify now',
    'client' => 'Client',
    'partner' => 'Partner',
    'bonus' => 'Bonus',
    'trade_turnover' => 'Trade turnover',
    'bronze_partner' => 'Bronze partner',
    'silver_partner' => 'Silver partner',
    'gold_partner' => 'Gold partner',
    'platinum_partner' => 'Platinum partner',
    'for_status_you_lack' => 'You are not enough <span class = "green-text"> 1500 € </span> to become a Gold Partner',
    'current_product_return' => 'Current circulation',
    'general_information' => 'General information',
    'withdrawn_funds' => 'Withdrawn funds',
    'date' => 'Date',
    'description' => 'Description',
    'price' => 'Price',
    'status_action' => 'Status / Action',
    'valid_until' => 'Valid until:',
    'order_delivery' => 'View invoice',
    'view_ticket' => 'View ticket',
    'your_income' => 'Your income',
    'wallet_balance' => 'Wallet balance',
    'lp_balance' => 'Balance DM',
    'replenish' => 'Replenish',
    'withdraw' => 'Withdraw',
    'increase_income' => 'How to increase <br> your income?',
    'transactions_hash' => '#transactions',
    'naming' => 'Naming',
    'status' => 'Status',
    'waiting' => 'Waiting',
    'error' => 'Error',
    'confirmed' => 'Confirmed',
    'pay' => 'Pay',
    'cancel' => 'Cancel',
    'your_get' => 'You get',
    '12_dec' => '12 Dec 2018, 19:39',
    'ticket_event' => 'Ticket for the event "World Conference"',
    'partner_license' => 'Partner License for a Year',
    'view_invoice' => 'View Invoice',
    'gold_bar_10g' => 'Gold Bar 10 g',
    'waiting_activate' => 'Activation Pending',
    'recent_purchases' => 'Recent purchases',
    'date_number' => '12 Dec 2018, 19:39',
    '12_ticket' => 'Ticket for the event "World Conference"',
    'gold_rate' => 'Gold rate',
    'liger_rate' => 'Liger rate',
    'user_dynamics' => 'User engagement dynamics',





];