<?php



return [
    'verify_sent' => 'Verification data sent for verification. You will be notified of the results in a message.',
    'no_notifications' => 'You have no notifications',
    'must_verify' => 'To access other sections of the site, you must pass verification',
    'verify_done_title' => 'Verification passed!',
    'verify_done_text' => 'We are pleased to inform you that you have successfully passed the verification! Now you can fully enjoy all the benefits of a verified user.'
];

