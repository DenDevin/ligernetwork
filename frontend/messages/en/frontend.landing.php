<?php


return [

    // landing/index

    'liger_title' => 'Liger Network - investment in gold',
    'liger_descr' => 'European, international platform for large and novice investors. Invest in gold - secure a future in prosperity for yourself and your loved ones.',
    'about_us' => 'About us',
    'proposal' => 'Offer',
    'partnership' => 'Affiliate',
    'plans' => 'Plans',
    'button_up' => 'Up',
    'button_down' => 'Down',
    'menu' => 'Menu',
    'gold' => 'Gold',
    'title_gold' => ' is the No. 1 source of beautiful life',
    'gold_text_1' => 'For centuries, gold has been associated with wealth, luxury',
    'gold_text_2' => ' and return on investment',
    'know_more' => 'Learn more',
    'buy' => '<span> Buy </span> <br>gold bars',

    'buy_text_1' => 'When you buy gold bars, you multiply yours <br>
                     cash resources while providing <br>
                     them protection against inflation and financial crises',

    'boss_title_1' => '<span> You are the owner </span> <br> own business',
    'boss_title_2' => 'This status gives you additional income for an 8-level <br> affiliate program. Guaranteed profit for you <br> from investments of your clients and partners',
    'about_company_title' => '<span>About</span> company',
    'about_company_text' => '
                                <p> LIGER NETWORK - European, international platform
                                for large and novice investors, equipped with effective software
                                and professional investment tools
                                and doing business on the principle of network marketing. </p>
                                <p> The company has its own cryptocurrency LIGER, which opens up alternative sources for our partners to increase and maintain their personal capital. The profitability of the investment of our customers and partners is ensured by certified gold bars. </p>
                                <p> LIGER NETWORK\'s goal is customer financial independence
                                and partners as well as a guarantee of stable profitability
                                their investment.</p>',
    'what_we_offer' => '<span> What we offer </span>',
    'what_we_offer_text' => '<p> LIGER NETWORK is a modern Internet platform, the main priority of which is to attract customers and provide their investments with the most profitable profitability. </p>
                             <p> Because the incredible value of gold, not only in its chemical properties (for its storage does not require special conditions). Investing in gold is always attractive to a wide variety of investors. Cryptocurrency Liger, opens up another niche for earning to customers and partners of our company. </p>
                             <p>The unique marketing plan of LIGER NETWORK, has no analogues in the market. Our customers can withdraw their accumulated funds in gold bullion, purchase a LIGER token that is constantly growing in price, or convert to other fiat money at their discretion. </p>',

    'safe_title' => '<span> Safe </span> Investments <br> <span> and Competitive </span> Benefits',
    'warranty_01' => 'Refund guarantee <br> deposit amount without <br> loss of funds within <br> two weeks after <br> crediting a transaction',
    'warranty_02' => 'The price of gold, <br> unlike other assets, is not subject to inflation.',
    'warranty_03' => 'For storage of gold <br> no special conditions required',
    'warranty_04' => 'Gold bars have <br> high liquidity, <br> they are easy to sell',
    'warranty_05' => 'Manage your <br> assets in your personal account',
    'begin_now' => 'Get started now!',
    'begin_text' => 'The decision today is the golden guarantee of stable income and material well-being tomorrow',
    'registration' => 'Registration',
    'facts_about_gold' => '<span>Facts about</span> gold',
    'fact_01' => 'Gold is a symbol of kings and a treasure of a nation; everyone aspires to it, but few possess it.',
    'fact_02' => 'Gold can be purchased in the form of coins or bars in grams, ounces or kilograms.',
    'fact_03' => 'Investing in gold is a guarantee of protection against instability in the financial markets.',
    'fact_04' => 'The safest way to save capital is physical gold. The purity of the precious metal is measured in carats, the maximum indicator is 24 carats.',
    'fact_05' => 'Gold is the most reliable asset in the world. In any economic situation, its relevance has no competition.',
    'partners_title' => '<span>Affiliate</span> program',
    'partners_subtitle' => 'You deserve more',
    'icon_refferal' => 'Becoming an active partner of our company, <br> you will increase your income by receiving cash rewards for all your partners <br> in percentage terms.',
    'icon_marketing' => 'The marketing plan of Liger Network has no analogues. <br> 8-level payment system for each client, <br> + 25% of the personal partner. Monthly cash <br> reward for rank confirmation.',
    'icon_team' => 'Make a challenge for yourself. Attend company training events <br> participate in promotions <br> and special offers, become a <br> mentor for your team. Super Bonuses are waiting for you.',
    'icon_diamond' => 'With Liger Network you are not just a business owner, <br> new horizons for personal growth and wealth are opened for you. You are a financially independent owner of your life!',
    'roadmap_title' => '<span>Company</span> roadmap',
    'r_desc_1' => 'Idea. Market research. <br> Goal setting',
    'r_date_1' => 'April - June 2018',
    'r_desc_2' => 'Business plan. Planning <br> and prioritizing <br> company development',
    'r_date_2' => 'July - September 2018',
    'r_desc_3' => 'Marketing Plan Development',
    'r_date_3' => 'October - November 2018',
    'r_desc_4' => 'Software Content <br> Website. Creation of back offices',
    'r_date_4' => 'April-June 2019',
    'r_desc_5' => '<span>Website Development</span>',
    'r_date_5' => 'December 2018 - March 2019',
    'r_desc_6' => 'Testing the site. <br> Registration of partners',
    'r_date_6' => 'July - September 2019',
    'r_desc_7' => 'Opening and registration of a company',
    'r_date_7' => 'October 2019',
    'invest_in_gold' => 'Invest in gold - secure <br> the future for yourself and your loved ones!',
    'liger_rules' => 'Terms of Use',
    'liger_policy' => 'Privacy policy',




];