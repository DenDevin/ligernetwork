<?php


return [

    'packages' => 'Packages and Licenses',
    'package' => 'Package',
    'client' => 'Client',
    'monthly' => 'Monthly',
    'pay' => 'Pay',
    'license' => 'License',
    'partner' => 'Partner',
    'annually' => 'Annually',
    'paid' => 'Paid <br> to',
    'events' => 'Events',
    'buy' => 'Buy',
    'gold_bars' => 'Gold bars',
    'buy_package' => 'Buy package',
    'more' => 'More...',
    'g' => 'g',
    'you_have' => 'You have an <span class = "text-white"> 2 </span> bars',
    'package_business_meta' => '180 Euro',

];