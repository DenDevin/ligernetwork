<?php

return [

    'total_profit_title' => 'Common profit per month',
    'your_structure_title' => 'Your structure',
    'entire_period' => 'All period',
    'partners' => 'Partners',
    'clients' => 'Clients',
    'client' => 'Client',
    'invitations' => 'Invitations per month',
    'add_new_partner' => 'Add new partner',
    'partner_login' => 'Partner login',
    'partner_password' => 'Partner Password',
    'generate_password' => 'Password generating',
    'add_partner' => 'Add Partner',
    'cancel' => 'Cancel',
    'structure_tree' => 'Structure tree',
    'first_line' => 'First line',
    'bronze_partner' => 'Bronze Partner',
    'gold_partner' => 'Gold Partner',
    'platinum_partner' => 'Platinum Partner',
    'waiting_activate' => 'Waiting Activate',
    'user_login' => 'User Login',
    'user_country' => 'Country',
    'user_status' => 'Status',
    'user_telegram' => 'Telegram',
    'user_structure' => 'Structure',



];