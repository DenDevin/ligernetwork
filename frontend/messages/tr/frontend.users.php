<?php


return [
	// Login

    'entrance' => 'Giriş',
    'nickname' => 'Giriş',
    'password' => 'Şifre',
    'log_in' => 'Giriş yap',
    'forgot_password' => 'Şifrenizi mi unuttunuz?',
    'register' => 'Kayıt',
    'resend' => 'Onay e-postası gelmedi mi?',

   // Registration

    'registration' => 'Kayıt',
    'partner_login' => 'Ortak Giriş',
];