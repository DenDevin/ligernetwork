<?php


return [

	// Login
	
    'entrance' => 'Вхід',
    'nickname' => 'Логін',
    'password' => 'Пароль',
    'log_in' => 'Увійти',
    'forgot_password' => 'Забули пароль?',
    'register' => 'Реєстрація',
    'resend' => 'Не отримали листа підтвердження?',

    // Registration

    'registration' => 'Реєстрація',
    'partner_login' => 'Логін партнера',
];