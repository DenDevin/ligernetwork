<?php


return [
	// Login

    'entrance' => 'Einloggen',
    'nickname' => 'Benutzername',
    'password' => 'Passwort',
    'log_in' =>   'Einloggen',
    'forgot_password' => 'Passwort vergessen?',
    'register' => 'Registrierung',
    'resend' => 'Sie haben keine Bestätigungsmail erhalten?',

   // Registration

    'registration' => 'Registrierung',
    'partner_login' => 'Partner Login',
];