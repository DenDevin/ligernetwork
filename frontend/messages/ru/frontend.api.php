<?php


return [
    'paysuccess' => 'Платеж оплачен успешно!',
    'transexpired' => 'Время ожидания платежа превышено!',
    'payerror' =>   'Ошибка платежа!',
    'your_balance_replenished_title' => 'Ваш баланс был пополнен!',
    'your_balance_replenished_text' => 'Система подтвердила вашу транзакцию, в результате чего ваш счет был пополнен!',
    'your_balance_was_payed_title' => 'Вы оплатили платеж с баланса!',
    'your_balance_was_payed_text' => 'Система подтвердила вашу транзакцию, в результате чего Вы оплатили платеж с баланса!',
    'admin_withdraw_message_title' => 'Пользователем системы был выставлен счет на вывод средств. Подтвердите или отклоните транзакцию в админпанели!',
    'admin_withdraw_message_text' => 'Пользователем системы был выставлен счет на вывод средств. Подтвердите или отклоните транзакцию в админпанели!',
    'withdraw_nomoney' => 'На вашем счету недостаточно средств для вывода!',
    'withdraw_success' => 'Транзакция успешно завершена! Ваш перевод отправлен на обработку!',
    'nomoney' => 'Недостаточно денег для платежа!',
    'packageactive' => 'Пакет успешно активирован!',
    'your_balance_withdraw_title' => 'Вывод денег выполнен успешно!',
    'your_balance_withdraw_text' => 'Система подтвердила вашу транзакцию и деньги были успешно выведены!',
    'your_balance_was_replenished_partner_bonus_title' => 'Начислен бонус!',
    'your_balance_was_replenished_partner_bonus_text' => 'Начислен бонус!',
    'your_balance_was_replenished_partner_bonus_text' => 'Вам был начислен бонус в размере {bonus} DM в рамках реферальной системы!',


];