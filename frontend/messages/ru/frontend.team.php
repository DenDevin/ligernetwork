<?php

return [

    'total_profit_title' => 'Общая прибыль за месяц',
    'your_structure_title' => 'Ваша структура',
    'entire_period' => 'За весь период',
    'partners' => 'Партнеров',
    'clients' => 'Клиентов',
    'client' => 'Клиент',
    'invitations' => 'Приглашения за текущий месяц',
    'add_new_partner' => 'Добавить нового партнера',
    'partner_login' => 'Логин партнера',
    'partner_password' => 'Пароль партнера',
    'generate_password' => 'Генерация пароля',
    'add_partner' => 'Добавить партнера',
    'cancel' => 'Генерация пароля',
    'structure_tree' => 'Дерево структуры',
    'first_line' => 'Первая линия',
    'bronze_partner' => 'Бронзовый партнер',
    'gold_partner' => 'Золотой партнер',
    'platinum_partner' => 'Платиновый партнер',
    'waiting_activate' => 'Ожидает активации',
    'user_login' => 'Логин пользователя',
    'user_country' => 'Страна',
    'user_status' => 'Статус',
    'user_telegram' => 'Телеграм',
    'user_structure' => 'Структура',



];