<?php


return [

	// users/login

    'entrance' => 'Вход',
    'nickname' => 'Логин',
    'password' => 'Пароль',
    'log_in' => 'Войти',
    'forgot_password' => 'Забыли пароль?',
    'register' => 'Регистрация',
    'resend' => 'Не получили письмо подтверждения?',
    'your_account_has_been_created' => 'Ваш аккаунт был создан! Проверьте ваш почтовый ящик!',
    'register_witout_token_imposible' => 'Регистрация без партнерской ссылки невозможна!',
   // users/registration/register

    'registration' => 'Регистрация',
    'partner_login' => 'Логин партнера',
    'your_login' => 'Ваш логин',
    'email' => 'E-mail',
    'choose_country' => 'Выберите страну',
    'register_go' => 'Зарегистрироваться',
    'ru_country' => 'Россия',
    'us_country' => 'США',
    'uk_country' => 'Украина',
    'de_country' => 'Германия',
    'tr_country' => 'Турция',
    'button_login' => 'Есть аккаунт? Авторизируйтесь!',
    'user_token_not_exist' => 'Пользователя с таким токеном не существует',
    'this_country_absent_inlist' => 'Такой страны нет в списке стран',
    'user_is_not_partner' => 'Регистрация по данному токену запрещена! Пользователь не является партнером сети.',


   // users/forgot

    'recovery_password' => 'Восстановление пароля',
    'reset_password' => 'Сброс пароля',
    'continue' => 'Продолжить',
    'finish' => 'Завершить',

     // users/resend

    'resend_request' => 'Повторное подтверждение',

    // users/reset

    'new_password' => 'Новый пароль',

    //



];