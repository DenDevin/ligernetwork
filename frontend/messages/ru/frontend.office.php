<?php


return [

    // office/index

    'my_account' => 'Мой профиль',
    'adminpanel' => 'Админпанель',
    'logout' => 'Выйти',
    'main' => 'Главная',
    'wallet' => 'Кошелек',
    'marketplace' => 'Маркетплейс',
    'my_purchases' => 'Мои покупки',
    'my_team' => 'Моя команда',
    'news' => 'Новости',
    'profile' => 'Профиль',
    'faq' => 'FAQ',
    'your_status' => 'Ваш статус',
    'not_verified' => 'Ваш аккаунт <span class="medium">не верифицирован</span>',
    'pass_verification' => 'Пройти верификацию сейчас',
    'client' => 'Клиент',
    'partner' => 'Партнер',
    'bonus' => 'Бонус',
    'trade_turnover' => 'Товарооборот',
    'for_status_you_lack' => 'Вам не хватает',
    'till_status' => ' до статуса ',
    'current_product_return' => 'Текущий товарооборот',
    'general_information' => 'Общая информация',
    'withdrawn_funds' => 'Снятые средства',
    'date' => 'Дата',
    'description' => 'Описание',
    'price' => 'Цена',
    'status_action' => 'Статус / действие',
    'valid_until' => 'Действительна до:',
    'order_delivery' => 'Посмотреть инвойс',
    'view_ticket' => 'Посмотреть билет',
    'your_income' => 'Ваш доход',
    'wallet_balance' => 'Баланс кошелька',
    'lp_balance' => 'Баланс DM',
    'replenish' => 'Пополнить',
    'withdraw' => 'Вывести',
    'increase_income' => 'Как увеличить <br> свой доход?',
    'transactions_hash' => '#транзакции',
    'naming' => 'Наименование',
    'status' => 'Статус',
    'waiting' => 'В ожидании',
    'error' => 'ошибка',
    'confirmed' => 'подтверждён',
    'pay' => 'Оплатить',
    'cancel' => 'Отменить',
    'your_get' => 'Вы получите',
    '12_dec' => '12 Дек 2018, 19:39',
    'ticket_event' => 'Билет на мероприятие "Мировая конференция"',
    'partner_license' => 'Лицензия "Партнер" на год',
    'view_invoice' => 'Посмотреть инвойс',
    'gold_bar_10g' => 'Золотой слиток 10 г',
    'waiting_activate' => 'Ожидается активация',
    'recent_purchases' => 'Последние покупки',
    '12_ticket' => '12 Билет на мероприятие "Мировая конференция',
    'date_number' => '12 Дек 2018, 19:39',
    'gold_rate' => 'Курс золота',
    'liger_rate' => 'Страны',
    'user_dynamics' => 'Динамика привлечения пользователей',
    'bronze_partner' => 'Бронзовый Партнер',
    'silver_partner' => 'Серебряный Партнер',
    'gold_partner' => 'Золотой Партнер',
    'platinum_partner' => 'Платиновый Партнер',
    'bronze_director' => 'Бронзовый Директор',
    'silver_director' => 'Серебряный Директор',
    'gold_director' => 'Золотой Директор',
    'platinum_director' => 'Платиновый Директор',
    'union_director' => 'Совет Директоров',











];