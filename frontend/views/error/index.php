<?php

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\widgets\LanguageSelector\LanguageSelector;
use common\widgets\Alert;
//print_r($exception); die;
?>


<div id="my-content" class="p-register my-content">

    <div class="container-fluid">


        <section class="register">
            <div class="liger-logo">
                <?= Html::img('/images/logo.svg') ?>
            </div>
            <div class="accent-title">

                <h1 class="animated heartBeat delay-1s infinite">
                    <b><?=$exception->statusCode?></b>
                </h1>
            </div>

            <div class="custom-popup">
                <? if($exception->statusCode == 404) : ?>
                <p><?= Yii::t('frontend.landing', '404') ?></p>
                <? endif; ?>
                <? if($exception->statusCode == 503) : ?>
                    <p><?= Yii::t('frontend.landing', '503') ?></p>
                <? endif; ?>
                <? if($exception->statusCode == 500) : ?>
                    <p><?= Yii::t('frontend.landing', '500') ?></p>
                <? endif; ?>
            </div>
        </section>
        <!----------------- Background effect START ----------------->
        <div id="background-effect" class="background-effect"></div>
        <!----------------- Background effect END ----------------->
        <!----------------- Login END ----------------->
    </div>
</div>

