<?php

namespace frontend\modules\users\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Package\Package;
use common\models\Event;
use common\models\Bars;
use common\models\Payment\UserBalance;
use common\models\Payment\UserTransaction;


class ApiController extends Controller
{
    public $enableCsrfValidation = false;


    public function actionPay()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            $order = Yii::$app->payment->alfacoins->createOrder($post);
            if($order)
            {

                $transaction = UserTransaction::create($order);
                return [
                    'success' => true,
                    'order' => $order,
                    'transaction' => $transaction,
                    'message' => Yii::t('frontend.api', 'paysuccess'),

                ];
            }

            else
            {
                return [
                    'success' => false,
                    'message' => Yii::t('frontend.api', 'payerror'),

                ];
            }






        }


    }


    public function actionExceed()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            $data = UserTransaction::setExeeded($post);
                return [
                    'success' => $data['status'],
                    'message' => $data['message'],
                ];


        }


    }



    public function actionWithdraw()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            if(UserBalance::withdrawEnable($post))
            {

                $transaction = UserTransaction::createWithdrawTransaction($post);
                    return [
                        'success' => true,
                        'transaction' => $transaction,
                        'message' => Yii::t('frontend.api', 'withdraw_success'),
                    ];
            }

            else
                {
                    return [
                        'success' => false,
                        'message' => Yii::t('frontend.api', 'withdraw_nomoney'),
                    ];

                }



        }


    }




    public function actionPayaccount()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            $data = UserBalance::payPackageFromBalance($post);

            if($data)
            {
                return [
                    'status' => $data['status'],
                    'message' => $data['message'],
                    'avaliable_till' => Yii::$app->formatter->asDate($data['avaliable_till']),

                ];
            }

        }


    }



}