<?php

namespace frontend\modules\users\controllers;

use common\models\Payment\UserTransaction;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Payment\UserBalance;
use yii\web\Response;
use yii\helpers\Json;
use common\models\Payment\search\UserTransactionSearch;

class WalletController extends Controller
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if(Yii::$app->user->identity->profile->is_verified)
                            {
                                return true;
                            }
                            else
                            {
                                return Yii::$app->getResponse()->redirect(['users/profile', '#' => 'verification__wrap']);
                            }

                        }

                    ],
                ],
            ]
        ];
    }


    public function actionNotification()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();


        return $post;


    }



    public function actionIndex()
    {

        $searchModel = new UserTransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        UserBalance::checkUserTransactions();
        $transactions = UserTransaction::getUserTransactions();
        $balance = UserBalance::getUserBalance(Yii::$app->user->identity->user_token);
        $balance_data = UserBalance::getUserBalanceData();

        return $this->render('index', compact(
            'balance',
            'balance_data',
            'transactions',
            'dataProvider'
        ));
    }

    public function actionSuccess()
    {

        $balance = UserBalance::getUserBalance(Yii::$app->user->identity->user_token);

        return $this->render('index', [
            'balance' => $balance,
        ]);
    }
}

