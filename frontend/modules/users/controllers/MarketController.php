<?php

namespace frontend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Package\Package;
use common\models\Package\UserPackageStat;
use common\models\Event;
use common\models\Bars;
use common\models\Payment\UserBalance;

class MarketController extends Controller
{
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'pay'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if(Yii::$app->user->identity->profile->is_verified)
                            {
                                return true;
                            }
                            else
                            {
                                return Yii::$app->getResponse()->redirect(['users/profile', '#' => 'verification__wrap']);
                            }

                        }

                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {

        $packages = Package::find()->all();
        $events = Event::find()->where(['active' => true])->all();
        $bars = Bars::find()->where(['active' => true])->all();
        $balance = UserBalance::getUserBalance(Yii::$app->user->identity->user_token);
        $balance_data = UserBalance::getUserBalanceData();
        return $this->render('index', compact(
            'packages',
            'events',
            'bars',
            'balance',
            'balance_data'
        ));
    }





}