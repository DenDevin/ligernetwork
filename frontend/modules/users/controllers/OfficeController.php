<?php
namespace frontend\modules\users\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Verify;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\authclient\AuthAction;
use common\models\Payment\UserBalance;
use backend\modules\marketing\models\UserMarketing;
use dektrium\user\controllers\SecurityController as BaseSecurityController;

/**
 * Site controller
 */
class OfficeController extends BaseSecurityController
{
   // public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [


            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'account', 'index'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'account', 'api'],
                        'allow' => true,
                        'roles' => ['@'],

                    ],

                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if(Yii::$app->user->identity->profile->is_verified)
                            {
                                return true;
                            }
                            else
                            {
                                return Yii::$app->getResponse()->redirect(['users/profile', '#' => 'verification__wrap']);
                            }

                        }

                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $verify = new Verify();
        $user = Yii::$app->user->identity;
        $marketing = UserMarketing::find()->where(['active' => true])->all();
        $statuses = UserMarketing::getUserStatusesData();
        $balance = UserBalance::getUserBalance($user->user_token);
        $balance_data = UserBalance::getUserBalanceData();


        return $this->render('index',
            compact(
                'verify',
                'user',
                'balance',
                'balance_data',
                'marketing',
                'statuses'

            )

        );
    }

    public function actionAccount()
    {
        return $this->render('index');
    }


}
