<?php
namespace frontend\modules\users\controllers;


use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\users\models\Profile;
use common\models\Verify;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'index'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],

                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }




    public function actionIndex()
    {


        $model = Profile::findOne(['user_id' => Yii::$app->user->identity->id]);
        Yii::$app->notification->resetNewMessages();
        $notifications = Yii::$app->notification->notifyArray();


        if(!$model) $model = new Profile();
        $user = Yii::$app->user->identity;
        $verify = new Verify();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $model->save(false);
        }
        if($verify->load(Yii::$app->request->post()) && $verify->validate())
        {
            
            if($verify->save(false))
            {
                Yii::$app->session->setFlash('success', Yii::t('frontend.message', 'verify_sent'));
            }
        }
            return $this->render('index',
                [
                    'model' => $model,
                    'verify' => $verify,
                    'user' => $user,
                    'notifications' => $notifications
                ]);

    }





}
