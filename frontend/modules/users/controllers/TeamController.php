<?php

namespace frontend\modules\users\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Package\Package;
use common\models\Package\UserPackageStat;
use common\models\Event;
use common\models\Bars;
use common\models\Payment\UserBalance;
use common\models\User\UserTree;
use common\models\User\search\UserTreeSearch;

class TeamController extends Controller
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'pay'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if(Yii::$app->user->identity->profile->is_verified)
                            {
                                return true;
                            }
                            else
                            {
                                return Yii::$app->getResponse()->redirect(['users/profile', '#' => 'verification__wrap']);
                            }

                        }

                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {

        $searchModel = new UserTreeSearch();
        $dataProvider = $searchModel->searchFirstChildren(Yii::$app->request->queryParams, Yii::$app->user->identity->user_token);



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }





}