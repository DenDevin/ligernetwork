<?php

namespace frontend\modules\users\controllers;

use Yii;
use frontend\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use dektrium\user\Finder;
use frontend\modules\users\models\RegistrationForm;
use common\models\Countries;
use dektrium\user\models\ResendForm;
use dektrium\user\traits\AjaxValidationTrait;
use dektrium\user\traits\EventTrait;
use yii\web\NotFoundHttpException;
use common\models\Langs;
use dektrium\user\filters\AccessRule;

use dektrium\user\controllers\RegistrationController as BaseRegisterController;
/**
 * Default controller for the `admin` module
 */
class RegistrationController extends BaseRegisterController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        //  'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['register', 'login'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function()
                        {
                            return Yii::$app->getResponse()->redirect(['users/office']);
                        }
                    ],
                ],
            ],

        ];
    }



    public function actionRegister($token = null)
    {

        if (!$this->module->enableRegistration)
        {
            throw new NotFoundHttpException();
        }

                $model = \Yii::createObject(RegistrationForm::className());
                $partner = $model->findUserByToken($token);
                $event = $this->getFormEvent($model);
                $this->trigger(self::EVENT_BEFORE_REGISTER, $event);
                $this->performAjaxValidation($model);

                if ($model->load(\Yii::$app->request->post()) && $model->register()) {
                    $this->trigger(self::EVENT_AFTER_REGISTER, $event);

                    return $this->render('/message', [
                        'title'  =>  Yii::t('user', 'Your account has been created and a message with further instructions has been sent to your email'),
                        'module' => $this->module,
                    ]);
                }

                return $this->render('register', compact(
                    'model',
                    'module',
                    'partner'
                ));
    }

}
