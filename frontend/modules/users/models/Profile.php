<?php

namespace frontend\modules\users\models;

use Yii;
use frontend\modules\users\models\query\ProfileQuery;
use common\models\User;
use common\models\Profile as BaseProfile;


class Profile extends BaseProfile
{
    public static $usernameRegexp = '/^[A-zА-я\s]+$/u';
    public static $userphoneRegexp = '#^[-+0-9()\s]+$#';
    public $old_pass;
    public $new_pass;
    public $repeat_pass;

    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'current_status', 'verify_status', 'doc_date', 'doc_valid_to', 'is_verified'], 'integer'],
            [['bio'], 'string'],
            [['public_email'], 'email'],
            [['name', 'surname', 'phone', 'gravatar_email', 'location', 'website', 'avatar', 'doc_serial_num', 'doc_country', 'pass_scan', 'pass_photo', 'address_invoice'], 'string', 'max' => 255],

            ['name', 'match', 'pattern' => static::$usernameRegexp],
            ['surname', 'match', 'pattern' => static::$usernameRegexp],
            ['phone', 'match', 'pattern' => static::$userphoneRegexp],

            [['gravatar_id', 'postal_index', 'telegram', 'skype', 'date_birth'], 'string', 'max' => 32],
            [['timezone'], 'string', 'max' => 40],
            [['lang'], 'string', 'max' => 16],
            [['surname', 'second_name'], 'string', 'max' => 64],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'public_email' => 'Public Email',
            'gravatar_email' => 'Gravatar Email',
            'gravatar_id' => 'Gravatar ID',
            'location' => 'Location',
            'website' => 'Website',
            'bio' => 'Bio',
            'timezone' => 'Timezone',
            'lang' => 'Lang',
            'current_status' => 'Current Status',
            'verify_status' => 'Verify Status',
            'avatar' => 'Avatar',
            'surname' => 'Surname',
            'second_name' => 'Second Name',
            'postal_index' => 'Postal Index',
            'doc_serial_num' => 'Doc Serial Num',
            'doc_country' => 'Doc Country',
            'doc_date' => 'Doc Date',
            'doc_valid_to' => 'Doc Valid To',
            'pass_scan' => 'Pass Scan',
            'pass_photo' => 'Pass Photo',
            'address_invoice' => 'Address Invoice',
            'phone' => 'Phone',
            'telegram' => 'Telegram',
            'skype' => 'Skype',
            'date_birth' => 'Date Birth',
            'is_verified' => 'Is Verified',
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public static function find()
    {
        return new ProfileQuery(get_called_class());
    }
}
