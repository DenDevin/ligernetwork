<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace frontend\modules\users\models;

use dektrium\user\traits\ModuleTrait;
use Yii;
use yii\base\Model;
use frontend\models\User;
use common\models\Package\UserPackage;
use common\models\Countries;
use common\models\Payment\UserBalance;
use common\models\User\UserTree;


/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationForm extends Model
{
    use ModuleTrait;
    /**
     * @var string User email address
     */
    public $email;

    /**
     * @var string Username
     */
    public $username;

    /**
     * @var string UserToken
     */
    public $user_token;

    /**
     * @var string UserTokenLogin
     */
    public $user_token_login;



    /**
     * @var string Password
     */
    public $password;

    /**
     * @var string Password
     */
    public $country;


    public $refferal_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $user = $this->module->modelMap['User'];


        return [

            ['refferal_id', 'integer'],

            'validCountry' => [
                'country', 'in',
                'range' => Countries::getAllCountries(),
                'message' => Yii::t('frontend.users', 'this_country_absent_inlist')
            ],

            // username rules

            'usernameTrim'     => ['username', 'trim'],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernamePattern'  => ['username', 'match', 'pattern' => $user::$usernameRegexp],
            'usernameRequired' => ['username', 'required'],


            'usernameUnique'   => [
                'username',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This username has already been taken')
            ],


            // email rules
            'emailTrim'     => ['email', 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern'  => ['email', 'email'],

            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $user,
                'message' => Yii::t('user', 'This email address has already been taken')
            ],

            'userTokenValid' => [
                'user_token',
                'string',
            ],

            'userTokenIsset' => [
                'user_token',
                'isIssetToken',
            ],

             'activePartner' => [
                'user_token',
                'isActivePartner',
            ],


            'passwordRequired' => ['password', 'required', 'skipOnEmpty' => $this->module->enableGeneratingPassword],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'register-form';
    }


    public function register()
    {


        if (!$this->validate()) {

            return false;
        }


        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->saveCredentials($user);
        $this->loadAttributes($user);
        if (!$user->register()) {
            return false;
        }

        return true;
    }


    public function findUserByToken($token)
    {

        if(!is_null($token))
          {
            $this->user_token = $token;

            if($this->validate('user_token'))
            {

                if (Yii::$app->useridentity->findActivePartner($this->user_token))
                {
                    $partner = User::findOne(['user_token' => $this->user_token]);
                    if(isset($partner))
                    {
                        $this->user_token = $partner->username;
                        $this->refferal_id = $partner->id;
                        return $this;
                    }
                    else
                    {

                        return false;
                    }

                }

            }

        }

    }


    protected function loadAttributes(User $user)
    {
        $user->setAttributes($this->attributes);
    }

    public function getCountries()
    {
     return Countries::getCountries();
    }

    public function saveCredentials($user)
    {
        $user->user_token = Yii::$app->security->generateRandomString(8);
        UserBalance::createBalance($user->user_token);
        $user->country = $this->country;
        $user->refferal_id = $this->refferal_id;
        $user->locale = Yii::$app->language;
        $user_ref = $user::findOne($this->refferal_id);
        UserTree::createRelation($user_ref, $user);

    }

    public function isActivePartner($attribute_name)
    {
        $isActive = Yii::$app->useridentity->findActivePartner($this->user_token);
        if(!$isActive)
        {
            $this->addError($attribute_name, Yii::t('frontend.users', 'user_is_not_partner'));
        }
    }

    public function isIssetToken($attribute_name)
    {
      $user = User::find()->
      where(['user_token' => $this->user_token])->
      orWhere(['username' => $this->user_token])->
      one();
      if($user)
      {
          return true;
      }

      else
      {
          $this->addError($attribute_name, Yii::t('frontend.users', 'user_token_not_exist'));
      }




return true;




    }



}
