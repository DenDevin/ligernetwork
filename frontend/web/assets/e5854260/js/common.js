$(document).ready(function() {

	$('.preloader').fadeOut('slow');

	// Select picker
	$('select').selectpicker();

	// Menu active
	$('#my-menu a').each(function() {
		var path = window.location.href;
		var link = this.href;
		if (path == link) {
			$(this).addClass('menu-active');
		}
	});
	// ----------------- Wallet popup 1 radio START ----------------- //

	$('.wallet-popup__radio-first input').on('change', function() {
		var copyInput = $('.radio-first__copy input');
		var thisDataValue = $(this).data('value');
		copyInput.attr('value', thisDataValue)
	});
	// ----------------- Wallet popup 1 radio END ----------------- //

	// ----------------- Wallet popup 2 radio START ----------------- //
	$('.withdraw-radio__item input').on('change', function() {
		var tabBtn = $(this).data('wallet-tab-btn');
		var tabContent = $('[data-wallet-tab]');
		$('.withdraw-radio__item').removeClass('active');
		$(this).parent().addClass('active');

		tabContent.each(function() {
			var tabContentData = $(this).data('wallet-tab');
			if (tabBtn != tabContentData) {
				$(this).removeClass('tab-active');
				$(this).hide();
			}
			else {
				$(this).addClass('tab-active');
				$(this).fadeIn(200);
			}
		});
	})
	// ----------------- Wallet popup 2 radio END ----------------- //
	

	// ----------------- Copy link START ----------------- //
	new ClipboardJS('.clipboard-btn, .clipboard-btn__wallet');
	// ----------------- Copy link END ----------------- //

	// ----------------- Menu START ----------------- //
	var menu = $('#my-menu');
	menu.mmenu({
		navbar: {
			classes: 'my-custom-width',
			title: '<div class="logo"><img src="/images/logo.png"></div>',
			extensions: [
			'border-none'
			],
		},
		extensions: [
		'border-none',
		],
		sidebar: {
			use: true,
			collapsed: {
				use: 992,
				hideNavbar: true,
				hideDivider: true,
			},
			expanded: {
				use: false,
			},
			blockMenu: true
		}
	});
	var hamburger = $('.hamburger');
	var menuApi = menu.data('mmenu')

	menuApi.bind('open:finish', function() {
		hamburger.addClass('is-active');
	});

	menuApi.bind('close:finish', function() {
		hamburger.removeClass('is-active');
	});
	// ----------------- Menu END ----------------- //


	// ----------------- Shop increment START ----------------- //
	var incrementValue = document.getElementsByClassName('value');
	var inc = document.getElementsByClassName('plus');

	// Инкремент +
	var incrementPlus = $('.event-ticket__item .plus');

	incrementPlus.on('click', function(e) {
		e.preventDefault();
		this.parentNode.querySelector('input[type=number]').stepUp();
	})

	// Инкремент -
	var incrementMinus = $('.event-ticket__item .minus');

	incrementMinus.on('click', function(e) {
		e.preventDefault();
		this.parentNode.querySelector('input[type=number]').stepDown();
	})
	// ----------------- Shop increment END ----------------- //


	// Ticket slider
	$('.event-ticket-slider').slick({
		slidesToShow: 2,
		infinite: true,
		arrows: false,
		dots: true,
		autoplay: true,
		dotsClass: 'custom-dots',
		pauseOnFocus: false,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 1,
				}
			},
			]
	});





	var stHeight = $('.slick-track').height();
	$('.slick-slide').css('height',stHeight + 'px' );

	// Custom tab
	$('.custom__tab li').on('click', function(e) {
		e.preventDefault();
		$(this).parent('.custom__tab').find('li').removeClass('active');
		$(this).addClass('active');

		var tab = $(this).data('tab');
		$('[data-tab-content]').removeClass('tab-active');
		$('[data-tab-content]').hide();
		$('[data-tab-content]').each(function() {
			var content = $(this).data('tab-content');
			if ( tab == content ) {
				$(this).addClass('tab-active');
				$(this).fadeIn('fast');
			}
		});
	});

	//---------------------- Profile tab change START ----------------------//
	$('.profile__tab a').on('click', function() {
		var attrHref = $(this).attr('href');
		$('.personal-tab__content').addClass('hidden');
		$(attrHref).removeClass('hidden');
		$(attrHref).addClass('active');
	});

	var hash = window.location.hash;
	if (hash == '#verification__wrap') {
		$('.personal-tab__content').addClass('hidden');
		$(hash).removeClass('hidden');
		$(hash).addClass('active');
		$('.profile__tab li').removeClass('active');
		$('#profile-tab__personal-verification').addClass('active');
	}
	//---------------------- Profile tab change END ----------------------//

	// Partners
	$('.tree__wrap a').on('click', function(e) {
		e.preventDefault();

		var thsSub = $(this).parent().find('ul');
		var child = $(this).find('a');

		thsSub.toggle();
		if ($(this).hasClass('straight-line')) {
			$(this).removeClass('straight-line');
		}
		else {
			$(this).addClass('straight-line');
		}
	});

	$('.tree__item .mdi-plus-circle').on('click', function() {
		return false;
	});

	// New partner magnific popup
	$('.tree__wrap .mdi-plus-circle').magnificPopup({
		callbacks: {
			open: function() {
				$('.mfp-wrap').addClass('mfp-ready-new-partner');
				$('.new-partner-popup__wrap .cancel').on('click', function(e) {
					e.preventDefault();
					$.magnificPopup.close();
				});
			}
		},
		mainClass: 'mfp-fade',
		removalDelay: 500,
	});

	// Custom Magnific popup
	$('.custom-magnific-popup').magnificPopup({
		removalDelay: 500,
		mainClass: 'mfp-fade',
		callbacks: {
			open: function() {
				$('.mfp-wrap').addClass('mfp-ready-new-partner');
				$('.cancel-custom').on('click', function() {
					$.magnificPopup.close();
				});
			},
		},
		
	});

	// Zoom
	$('.zoom__wrap .mdi-plus').on('click', function() {
		$('.team-tree__zoom').css('transform', 'scale(1.5)');
	});

	$('.zoom__wrap .mdi-minus').on('click', function() {
		$('.team-tree__zoom').css('transform', 'none');
	});


	// Profile custom ScrollBar
	$('.notification__wrap').customScrollbar({
		skin: "default-skin", 
		hScroll: false,
		updateOnWindowResize: true,
		preventDefaultScroll: true,
		hScroll: true,
		animationSpeed: 500,
	});

	$('.verification-information').on('click', function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$(href).toggleClass('hidden');
		var close = $(href).find('.mdi-close');
		close.on('click', function() {
			$(this).parent().addClass('hidden');
		});
	});

	// Upload file
	$('.tagged-upload input').on('change', function() {
		var val = $(this).val();
		console.log(val);

		$(this).parent().find('.file-name').remove();
		$(this).parent().append('<div class="file-name"><p>'+val+'</p><span class="mdi mdi-close"></span></div>');

		$('.file-name .mdi-close').on('click', function() {
			$(this).parent().remove();
		});
	});


	//---------------- SMS code START ----------------//
	$('.activate-code input').on('keyup', function(e) {
		let value = $(this).val();
		let len = value.length;
		let curTabIndex = parseInt($(this).attr('tabindex'));
		let nextTabIndex = curTabIndex + 1;
		let prevTabIndex = curTabIndex - 1;
		if (len >= 3) {
			$(this).val(value.substr(0, 3));
			$('[tabindex=' + nextTabIndex + ']').focus();
		} else if (len == 0 && prevTabIndex !== 0) {
			$('[tabindex=' + prevTabIndex + ']').focus();
		}
	});

	$('.activate-code .input-first').on('keyup', function() {
		let value = $(this).val();
		let len = value.length;
		$('.activate-code span').each(function() {
			var data = $(this).data('number-first');
			if ( data == len ) {
				$(this).addClass('accent-bg');
			}
			else if ( data > len ) {
				$(this).removeClass('accent-bg');
			}
		});
	});

	$('.activate-code .input-second').on('keyup', function() {
		let value = $(this).val();
		let len = value.length;
		$('.activate-code span').each(function() {
			var data = $(this).data('number-second');
			if ( data == len ) {
				$(this).addClass('accent-bg');
			}
			else if ( data > len ) {
				$(this).removeClass('accent-bg');
			}
		});
	});

	$('.mask-number').mask('0000-0000');
	//---------------- SMS code END ----------------//


	//---------------- FAQ accordion START ----------------//
	$('.faq__button').on('click', function() {
		var parent = $(this).parents('.faq__item');
		var collapseContent = parent.find('.collapse');
		var sign = $('.faq__wrap').find('.mdi');
		var signThis = parent.find('.mdi');

		if ( !collapseContent.hasClass('show') ) {
			sign.each(function() {
				if ($(this).hasClass('mdi-minus-circle-outline')) {
					$(this).removeClass('mdi-minus-circle-outline');
					$(this).addClass('mdi-plus-circle-outline');
				}
			});

			$('.faq__item').removeClass('faq__active');
			parent.addClass('faq__active');
			parent.removeClass('border-left-accent');
			parent.addClass('border-left-orange');
			signThis.removeClass('mdi-plus-circle-outline');
			signThis.addClass('mdi-minus-circle-outline');
		}

		else if ( collapseContent.hasClass('show') ) {
			sign.removeClass('mdi-minus-circle-outline');
			sign.addClass('mdi-plus-circle-outline');
			parent.removeClass('faq__active');
			parent.removeClass('border-left-orange');
			parent.addClass('border-left-accent');
		}
	});
	//---------------- FAQ accordion END ----------------//


	//---------------- Wallet popup radio START ----------------//
	$('.radio__item input, .radio__item label').on('click', function() {
		$('.radio__item').removeClass('active');
		$(this).parent().addClass('active');
	});
	//---------------- Wallet popup radio END ----------------//

})


// TEST
window.onload = function() {
	var ru = '?lang=ru';
	var en = '?lang=en';
	var location = window.location.href;
	var page = location.split('?').pop();
	// console.log(page);

	if (page == 'lang=ru') {
		console.log('language: ru');
	}
	else if (page == 'lang=en') {
		console.log('language: en');
	}
	else {
		console.log('language: default');
	}
}