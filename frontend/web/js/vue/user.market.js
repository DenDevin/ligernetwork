var vue = new Vue({
    el: '#app',
    data: function()
    {
        return  {
            submitData: {},
            loading: false,
            pure_price: '',
            wallet_bitcoin: '',
            euro_balance: 0,
            needBalance: true,
            paymentSuccess: false,
            paymentFailed: false,
        };
    },
    methods: {
        callPopup: function(pure_price, euro_balance)
        {

               this.pure_price = pure_price,
               this.euro_balance = euro_balance,
               this.needBalance = this.isNeedBalance

        },

        submitPay: function()
        {


                 this.loading = true,
                 this.submitData = {
                  'pure': this.pure_price,
                  'wallet': this.wallet_bitcoin
              }


            this.$http.post('/api/pay', {data: this.submitData}).then(response => {
            if(response.body.status == 'success')
            {
                $('body').notify({
                    message: 'Hello World',
                    type: 'danger'
                });
                this.loading = false
               this.paymentFailed = false
                this.paymentSuccess = true

            }
            else if(response.body.status == 'failed')
            {
                this.loading = false
               this.paymentSuccess = false
                this.paymentFailed = true
            }


        })


        },



    },
    computed: {
        isNeedBalance: function()
        {

            if(this.euro_balance > this.pure_price)
            {

                   this.needBalance = false
                   return this.needBalance
            }
            else
                {
                    return this.needBalance
                }
        }

    },
    created: function()
    {


    }



});