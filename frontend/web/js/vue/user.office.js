
am4core.ready(function() {







    am4core.useTheme(am4themes_animated);
// Themes end
    var countries = {
        "AD": {
            "country": "Andorra",
            "continent_code": "EU",
            "continent": "Europe",
            "maps": [ "andorraLow", "andorraHigh" ]
        },
        "AE": {
            "country": "United Arab Emirates",
            "continent_code": "AS",
            "continent": "Asia",
            "maps": [ "uaeLow", "uaeHigh" ]
        },
        "AF": {
            "country": "Afghanistan",
            "continent_code": "AS",
            "continent": "Asia",
            "maps": []
        },
        "AG": {
            "country": "Antigua and Barbuda",
            "continent_code": "NA",
            "continent": "North America",
            "maps": [ "antiguaBarbudaLow", "antiguaBarbudaHigh" ]
        }


    };

    var continents = {
        "AF": 0,
        "AN": 1,
        "AS": 2,
        "EU": 3,
        "NA": 4,
        "OC": 5,
        "SA": 6
    }




// Create map instance
    var chart = am4core.create("chartdiv", am4maps.MapChart);

// Set map definition
    chart.geodata = am4geodata_worldLow;


// Set projection
    chart.projection = new am4maps.projections.Miller();

// Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Exclude Antartica
    polygonSeries.exclude = ["AQ"];

// Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

// Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = chart.colors.getIndex(0).lighten(0.5);

// Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(0);




    // Create country specific series (but hide it for now)
    var countrySeries = chart.series.push(new am4maps.MapPolygonSeries());
    countrySeries.useGeodata = true;
    countrySeries.hide();
    countrySeries.geodataSource.events.on("done", function(ev) {
        worldSeries.hide();
        countrySeries.show();
    });

    var countryPolygon = countrySeries.mapPolygons.template;
    countryPolygon.tooltipText = "{name}";
    countryPolygon.nonScalingStroke = true;
    countryPolygon.strokeOpacity = 0.5;
    countryPolygon.fill = am4core.color("#eee");

    var hs = countryPolygon.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(9);


// Set up data for countries
    var data = [];
    for(var id in countries) {
        if (countries.hasOwnProperty(id)) {
            var country = countries[id];
            if (country.maps.length) {
                data.push({
                    id: id,
                    color: chart.colors.getIndex(continents[country.continent_code]),
                    map: country.maps[0]
                });
            }
        }
    }
  //  worldSeries.data = data;

// Zoom control
    chart.zoomControl = new am4maps.ZoomControl();


























// Add image series
    var imageSeries = chart.series.push(new am4maps.MapImageSeries());
    imageSeries.mapImages.template.propertyFields.longitude = "longitude";
    imageSeries.mapImages.template.propertyFields.latitude = "latitude";
    imageSeries.data = [ {
        "title": "Brussels",
        "latitude": 50.8371,
        "longitude": 4.3676
    }, {
        "title": "Copenhagen",
        "latitude": 55.6763,
        "longitude": 12.5681
    }, {
        "title": "Paris",
        "latitude": 48.8567,
        "longitude": 2.3510
    }, {
        "title": "Reykjavik",
        "latitude": 64.1353,
        "longitude": -21.8952
    }, {
        "title": "Moscow",
        "latitude": 55.7558,
        "longitude": 37.6176
    }, {
        "title": "Madrid",
        "latitude": 40.4167,
        "longitude": -3.7033
    }, {
        "title": "London",
        "latitude": 51.5002,
        "longitude": -0.1262,
        "url": "http://www.google.co.uk"
    }, {
        "title": "Peking",
        "latitude": 39.9056,
        "longitude": 116.3958
    }, {
        "title": "New Delhi",
        "latitude": 28.6353,
        "longitude": 77.2250
    }, {
        "title": "Tokyo",
        "latitude": 35.6785,
        "longitude": 139.6823,
        "url": "http://www.google.co.jp"
    }, {
        "title": "Ankara",
        "latitude": 39.9439,
        "longitude": 32.8560
    }, {
        "title": "Buenos Aires",
        "latitude": -34.6118,
        "longitude": -58.4173
    }, {
        "title": "Brasilia",
        "latitude": -15.7801,
        "longitude": -47.9292
    }, {
        "title": "Ottawa",
        "latitude": 45.4235,
        "longitude": -75.6979
    }, {
        "title": "Washington",
        "latitude": 38.8921,
        "longitude": -77.0241
    }, {
        "title": "Kinshasa",
        "latitude": -4.3369,
        "longitude": 15.3271
    }, {
        "title": "Cairo",
        "latitude": 30.0571,
        "longitude": 31.2272
    }, {
        "title": "Pretoria",
        "latitude": -25.7463,
        "longitude": 28.1876
    } ];








// add events to recalculate map position when the map is moved or zoomed
    chart.events.on( "ready", updateCustomMarkers );
    chart.events.on( "mappositionchanged", updateCustomMarkers );

// this function will take current images on the map and create HTML elements for them
    function updateCustomMarkers( event ) {

        // go through all of the images
        imageSeries.mapImages.each(function(image) {
            // check if it has corresponding HTML element
            if (!image.dummyData || !image.dummyData.externalElement) {
                // create onex
                image.dummyData = {
                    externalElement: createCustomMarker(image)
                };
            }

            // reposition the element accoridng to coordinates
            var xy = chart.geoPointToSVG( { longitude: image.longitude, latitude: image.latitude } );
            image.dummyData.externalElement.style.top = xy.y + 'px';
            image.dummyData.externalElement.style.left = xy.x + 'px';
        });

    }

// this function creates and returns a new marker element
    function createCustomMarker( image ) {

        var chart = image.dataItem.component.chart;

        // create holder
        var holder = document.createElement( 'div' );
        holder.className = 'map-marker';
        holder.title = image.dataItem.dataContext.title;
        holder.style.position = 'absolute';

        // maybe add a link to it?
        if ( undefined != image.url ) {
            holder.onclick = function() {
                window.location.href = image.url;
            };
            holder.className += ' map-clickable';
        }

        // create dot
        var dot = document.createElement( 'div' );
        dot.className = 'dot';
        holder.appendChild( dot );

        // create pulse
        var pulse = document.createElement( 'div' );
        pulse.className = 'pulse';
        holder.appendChild( pulse );

        // append the marker to the map container
        chart.svgContainer.htmlElement.appendChild( holder );

        return holder;
    }

}); // end am4core.ready()



//----------------- Chart START -----------------//

//----------------- 1 Diagram Gold START -----------------//
var ctxGold = document.getElementById('myChart').getContext('2d');
var yLabelsGold = {
    0 : '€1 235',
    1 : '€1 236',
    2 : '€1 237',
    3 : '€1 238',
    4	: '€1 239',
    5 : '€1 240',
    6 : '€1 241',
}

var chart = new Chart(ctxGold, {
    type: 'line',
    data: {
        labels: [22, 23, 24, 25, 26, 27, 28,],
        datasets: [{
            label: 'My first label',
            borderColor: '#caad6e',
            data: [0, 1, 2, 3, 4, 5, 6]
        }]
    },

    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return yLabelsGold[value];
                    }
                }
            }]
        },
        plugins: {
            filter: {
                propagate: true
            }
        }
    }
})
//----------------- 1 Diagram Gold END -----------------//



//----------------- 3 Diagram Dynamic START -----------------//
var ctxDynamic = document.getElementById('myChart3').getContext('2d');

var yLabelsDynamic = {
    0 : '0',
    5 : '5',
    10 : '10',
    15 : '15',
    20 : '20',
    25 : '25',
    30 : '30',
}
var chartDynamic = new Chart(ctxDynamic, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['22/12', '23/12', '24/12', '25/12', '26/12', '27/12', '28/12'],
        datasets: [{
            label: false,
            borderColor: '#bba269',
            data: [0, 5, 10, 15, 20, 25, 30],
        }],
    },

    // Configuration options go here
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return yLabelsDynamic[value];
                    }
                }
            }]
        },
    }
});
//----------------- 3 Diagram Dynamic END -----------------//


//----------------- Chart END -----------------//












var vue = new Vue({
    el: '#app',
    data: function()
    {
        return  {
            info: [],
        };
    },
    methods: {
        viewTicket: function()
        {


        },



    },
    computed: {


    },
    created: function()
    {


    }



});