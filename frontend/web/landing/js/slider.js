	var fsContainer = $('.full-screen-slider'),
		fsSlide = $('[data-slide]'),
		fsSlideActive = $('[data-slide].active'),
		fsSlideText = $('.slide-text'),
		fsSlideImage = $('.fs-image'),
		prevButton = $('.prev-btn'),
		nextButton = $('.next-btn'),
		dot = $('.dot'),
		slideLength = fsSlide.length/4,
		fsTimeout = 6000,
		// setTimer = setInterval(autoplaySlider, fsTimeout),
		slideTriger = $('.fs-triger'),
		circle = $('.next-btn .circle');
		TweenMax.to(circle, 6, {strokeDashoffset: '195', ease: Linear.easeNone});
	
	function init(){
		TweenMax.set(fsSlide.not(fsSlideActive), {autoAlpha:0})
	}
	init();
	function setAnimation(curentSlideImage, nextSlideImage, curentSlide, nextSlide, curentSlideText, nextSlideText){
		var tlNext = new TimelineMax();
		TweenMax.set(dot, {pointerEvents:'none'})
		TweenMax.set(nextSlideText, {x: '-100', autoAlpha: 0})
		TweenMax.set(nextSlide, {className: '+=active'})
		TweenMax.set(curentSlide, {className: '-=active'})
		TweenMax.set(slideTriger, {x:'-100%'})
		tlNext
			  .set(nextButton, {pointerEvents:'none'})
			  .set(prevButton, {pointerEvents:'none'})
			  .to(slideTriger, .5, {x:'0%', ease: Power2.easeOut}, 0, 'fsTriger')
			  .to(curentSlideImage, .8,{x:'+10%', ease: Power2.easeOut},0)
			  .to(nextSlide, .3, {autoAlpha:1, ease: Power2.easeOut}, 'fsTriger+=0')
			  .to(curentSlide, .3, {autoAlpha:0, ease: Power2.easeOut}, 'fsTriger+=0')
			  .to(slideTriger, .8, {x:'+=100%', ease: Power2.easeOut}, 'slideTriger')
			  .fromTo(nextSlideImage, .8,{x:'-10%', ease: Power2.easeOut}, {x:'0%', ease: Power2.easeOut}, 'slideTriger+=0')
			  .to(curentSlideText, 1, {x: '100', autoAlpha: 1, ease: Power2.easeOut}, 'fsTriger+=0')
			  .to(nextSlideText, 1, {x: '0', autoAlpha: 1, ease: Power2.easeOut}, 'fsTriger+=0.5')
			  .set(nextButton, {pointerEvents:'auto'})
			  .set(prevButton, {pointerEvents:'auto'})
			  .set(dot, {pointerEvents:'auto'})
		TweenMax.fromTo(tlNext, 1, {timeScale:0}, {timeScale:2})
		TweenMax.lagSmoothing(500, 33)

	}
	function goToNextSlide(curentSlide, nextSlide){
		var curentSlideImage = curentSlide.find(fsSlideImage),
			nextSlideImage = nextSlide.find(fsSlideImage);
			curentSlideText = curentSlide.find(fsSlideText),
			nextSlideText = nextSlide.find(fsSlideText);

			// curentSlide.removeClass('active')
			// nextSlide.addClass('active')

			setAnimation(curentSlideImage, nextSlideImage, curentSlide, nextSlide, curentSlideText, nextSlideText)

			// console.log(slideLength)
	}
	nextButton.on('click', function(e){
		e.preventDefault();
		var curentSlide = $('[data-slide].active'), 
			nextSlide = curentSlide.next(fsSlide),
			otherSlide = fsSlide.not(fsSlideActive),
			thisIndex = curentSlide.index() + 1;
		if(thisIndex === slideLength){
			nextSlide = $('[data-slide=1]')
		}
	// 	clearInterval(setTimer);
	// 	setTimer = setInterval(autoplaySlider, fsTimeout)
		goToNextSlide(curentSlide, nextSlide, thisIndex)
	})
	prevButton.on('click', function(e){
		e.preventDefault();
		var curentSlide = $('[data-slide].active'), 
			prevSlide = curentSlide.prev(fsSlide),
			thisIndex = curentSlide.index() + 1;
		if(thisIndex === 1){
			prevSlide = $('[data-slide='+slideLength+']')
		}
	// 	clearInterval(setTimer);
	// 	setTimer = setInterval(autoplaySlider, fsTimeout)
		goToNextSlide(curentSlide, prevSlide, thisIndex)
	})
	dot.on('click', function(e){
		e.preventDefault();
		var curentSlide = $('[data-slide].active'),
			dotIndex = $(this).data('slide'),
			otherSlide = $('[data-slide='+dotIndex+']');
	// 	clearInterval(setTimer);
	// 	setTimer = setInterval(autoplaySlider, fsTimeout)
		goToNextSlide(curentSlide, otherSlide);
	})
	// function autoplaySlider(){
	// 	var curentSlide = $('[data-slide].active'), 
	// 		nextSlide = $(curentSlide.next(fsSlide),
	// 		thisIndex = curentSlide.index() + 1;
	// 	if(thisIndex === slideLength){
	// 		nextSlide = $('[data-slide=1]')
	// 	}
	// 	goToNextSlidde(curentSlide, nextSlide);
	// }