

var vue = new Vue({
    el: '#app',


    data: function()
    {
        return  {
            package_id: 0,
            package_cost: 0,
            amount_replenish: '',
            amount_withdraw: '',
            amount_withdraw_clear: '',
            wallet_withdraw: '',
            amountWithoutRent: '',
            coin_amount: 0,
            address: '',
            submitData: {},
            loading: false,
            paymentRecieved: false,
            user_balance: 0,
            euro_balance: 0,
            needBalance: true,
            counter: 960,
            progress : 100,
            minutes: 0,
            seconds: 0,
            time: '',
            txn_id: 0,
            paymentExceeded: false,
            paymentExceededText: ''


        };
    },
    methods: {

        withdrawMoney: function(balance)
        {


            if(this.amount_withdraw == '')
            {
                this.pushNotify('Enter amount withdraw!', 'error');
                return false;
            }
            else if( this.wallet_withdraw == '')
            {
                this.pushNotify('Enter btc - wallet for withdraw!', 'error');
                return false;
            }

            this.amount_withdraw = Number(this.amount_withdraw)
            this.amountWithoutRent = this.amount_withdraw
            this.amount_withdraw = this.pushRentWithdraw(this.amount_withdraw).toFixed(2)

            this.submitData = {
                'wallet': this.wallet_withdraw,
                'amount': this.amount_withdraw,
                'amount_pure': this.amountWithoutRent
            }
console.log(this.submitData)
                    this.$http
                        .post('/users/api/withdraw',
                            {data: this.submitData})
                        .then(response => {
                        if(response.body.success)
                    {
                        this.pushNotify(response.body.message);
                        this.paymentRecieved = true

                    }

                    else
                        {
                            this.pushNotify(response.body.message, 'error');
                        }

                })

        },


        countDownTimer: function(id) {
            this.txn_id = id
            if(this.counter > 0) {
                setTimeout(() => {
                --this.counter
                this.seconds = this.counter % 60
                this.minutes = parseInt(this.counter / 60, 10) % 60
                if(this.seconds < 10)
                {
                    this.seconds = '0'+this.seconds
                }
                if(this.minutes < 10)
                {
                    this.minutes = '0'+this.minutes
                }
                this.time = this.minutes+':'+this.seconds
                this.countDownTimer(this.txn_id)
            }, 1000)
            }
            else {
              //  alert(this.txn_id)
                this.submitData = { 'txn_id': this.txn_id}

                this.$http
                    .post('/users/api/exceed',
                        {data: this.submitData})
                    .then(response => {
                    if(response.body.success)
                {
                    this.paymentExceededText = response.body.message
                    this.paymentExceeded = true
                    this.paymentRecieved = false

                }

            })


            }
        },

        pushNotify: function(texting,
                             typing = 'success',
                             delaying = 5000,
                             positioning = 'right')
        {
            $.notify(texting, {
                className: typing,
                autoHideDelay: delaying,
                position: positioning,

            })
        },

        submitPayCoins: function()
        {

            if(this.amount_replenish == '')
            {
                this.pushNotify('Enter amount replenish!', 'error');
                return false;
            }


            this.amount_replenish = Number(this.amount_replenish)
            this.amountWithoutRent = this.amount_replenish
            this.amount_replenish = this.pushRentReplenish(this.amount_replenish)
            this.loading = true
            this.submitData = {
                'cost': this.amount_replenish,
                'cost_no_rent': this.amountWithoutRent

            }

            this.$http
                .post('/users/api/pay',
                    {data: this.submitData})
                .then(response => {
                if(response.body.success)
            {

                this.coin_amount = response.body.order.coin_amount
                this.address = response.body.order.address
                this.paymentExceeded = false
                this.paymentRecieved = true
                this.countDownTimer(response.body.order.id)
                this.loading = false

            }

        })


        },


        submitPayBalance: function(package_id, package_cost, user_balance)
        {

            this.package_id = package_id
            this.package_cost = this.convertToCent(package_cost)
            this.user_balance = user_balance

            if(this.user_balance >= this.package_cost)
            {

                this.submitData = {
                    'cost': this.package_cost,
                    'package': this.package_id
                }

                this.$http
                    .post('/users/api/payaccount',
                        {data: this.submitData})
                    .then(response => {
                    if(response.body.success)
                {
                    this.pushNotify(response.body.message);
                }
            else
                {
                    this.pushNotify(response.body.message, 'error');

                }
            })



            }



        },

        convertToCent: function(summ){

            return summ * 100
        },


        pushRentWithdraw: function(summ){
            let amount = summ;
            let rent = parseInt(summ) / 100 * 2;

            return amount + rent;

        },


        pushRentReplenish: function(summ){
            let amount = summ;
            let rent = parseInt(summ) / 100;

            return amount + rent;

        }



    },
    computed: {


    },
    created: function()
    {


    }



});