

var vue = new Vue({
    el: '#app',


    data: function()
    {
        return  {
            package_id: 0,
            package_cost: 0,
            amount_replenish: '',
            amount_withdraw: '',
            wallet_withdraw: '',
            coin_amount: 0,
            address: '',
            submitData: {},
            loading: false,
            paymentRecieved: false,
            user_balance: 0,
            euro_balance: 0,
            counter: 960, //seconds // 15 minutes
            progress : 100,
            minutes: 0,
            seconds: 0,
            time: '',
            txn_id: 0,
            disablePay1: false,
            disablePay2: false,
            avaliable_till: '',
            rentReplenish: 1, // percent
            rentWithdraw: 2, // percent
            amountWithoutRent: '',
            paymentExceeded: false,
            paymentExceededText: ''


        };
    },
    methods: {


        countDownTimer: function(id)
        {
            this.txn_id = id
            if(this.counter > 0) {
                setTimeout(() => {
                --this.counter
                this.seconds = this.counter % 60
                this.minutes = parseInt(this.counter / 60, 10) % 60
                if(this.seconds < 10)
                {
                    this.seconds = '0'+this.seconds
                }
                if(this.minutes < 10)
                {
                    this.minutes = '0'+this.minutes
                }
                this.time = this.minutes+':'+this.seconds
                this.countDownTimer(this.txn_id)
            }, 1000)
            }
            else {
                        this.submitData = { 'txn_id': this.txn_id}
                        this.$http
                            .post('/users/api/exceed',
                                {data: this.submitData})
                            .then(response => {
                            if(response.body.success)
                        {
                            this.paymentExceededText = response.body.message
                            this.paymentExceeded = true
                            this.paymentRecieved = false
                        }
                    })
                }
        },

        pushNotify: function(texting,
                             typing = 'success',
                             delaying = 5000,
                             positioning = 'right')
        {
            $.notify(texting, {
                className: typing,
                autoHideDelay: delaying,
                position: positioning,

            })
        },

        submitPayCoins: function()
       {

           if(this.amount_replenish == '')
           {
               this.pushNotify('Enter amount replenish!', 'error');
               return false;
           }
           this.amountWithoutRent = this.amount_replenish
           this.amount_replenish = this.pushRentReplenish(this.amount_replenish)
            this.loading = true
            this.submitData = {
                'cost': this.amount_replenish,
                'cost_no_rent': this.amountWithoutRent
           }

            this.$http
                .post('/api/pay',
                    {data: this.submitData})
                .then(response => {
                if(response.body.success)
            {

                this.coin_amount = response.body.order.coin_amount
                this.address = response.body.order.address
                this.paymentRecieved = true
                this.countDownTimer(response.body.order.id)
                this.loading = false

            }

        })


        },


        submitPayBalance: function(package_id, package_cost, user_balance)
        {

            this.package_id = package_id
            this.user_balance = user_balance
            this.amountWithoutRent = package_cost
            this.package_cost = this.convertToCent(this.amountWithoutRent)

            if(this.user_balance >= this.package_cost)
                {

                        this.submitData = {
                            'cost': this.package_cost,
                            'package': this.package_id
                        }
                        this.$http
                            .post('/users/api/payaccount',
                                {data: this.submitData})
                            .then(response => {
                            if(response.body.status)
                            {
                                this.pushNotify(response.body.message, response.body.status)
                                this.avaliable_till = response.body.avaliable_till
                                if(this.package_id == 1)
                                {
                                    this.disablePay1 = true
                                }
                                if(this.package_id == 2)
                                {
                                    this.disablePay2 = true
                                }

                            }

                    })


                }

        },

        convertToCent: function(summ)
        {
            return summ * 100
        },

        pushRentWithdraw: function(summ)
        {
            let amount = summ
            let rent = parseInt(summ) / 100 * this.rentWithdraw
            return amount + rent
        },


        pushRentReplenish: function(summ)
        {
            let amount = summ
            let rent = parseInt(summ) / 100 * this.rentReplenish
            return amount + rent
        }
    },
    computed: {


    },
    created: function()
    {


    }

});