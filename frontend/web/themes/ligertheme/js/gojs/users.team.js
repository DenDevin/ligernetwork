var $ = go.GraphObject.make;
var myDiagram =
    $(go.Diagram, "myDiagramDiv",
        {
            "undoManager.isEnabled": false,
            "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
            initialContentAlignment: go.Spot.Center,
            allowDelete: false,
            allowCopy: false,
            allowDrop: false,
            allowLink: false,
            allowMove: false,
            layout: $(go.TreeLayout, // specify a Diagram.layout that arranges trees
                { angle: 0, layerSpacing: 50 })
        });


myDiagram.linkTemplate =
    $(go.Link, go.Link.Orthogonal,
        { corner: 5, relinkableFrom: true, relinkableTo: true },
        $(go.Shape, { strokeWidth: 1.5, stroke: "#BFA265", strokeDashArray: [4, 2] }));


myDiagram.nodeTemplate =

    $(go.Node, "Auto",
        $(go.Shape, "Rectangle",
            new go.Binding("fill", "status"),
            {
                name: "SHAPE",
                fill: "#1E252A",
                stroke: "#B4965B",
                strokeWidth: 1.5,
                strokeJoin: "round",
                portId: "",
                fromLinkable: true,
                toLinkable: true,
                cursor: "pointer"
            }),

        $(go.Panel, "Horizontal",
            { width: 220, height: 70 }, // panel properties

            $(go.Panel, "Table",
                {
                    margin: 12,
                },
                $(go.TextBlock,
                   new go.Binding("text", "name"),

                    {
                        overflow: go.TextBlock.OverflowClip,
                        verticalAlignment: go.Spot.Center,
                       // maxLines: 1,
                        margin: 4,
                        row: 1, column: 1,
                        stroke: "#FFFFFF", font: "16px sans-serif",
                        editable: true, isMultiline: false,
                      //  minSize: new go.Size(20, 26)
                    }



            ),

            $(go.TextBlock,
                "Default Text",
                {
                    row: 2, column: 1,
                    verticalAlignment: go.Spot.Center,
                    margin: 4, stroke: "#677F75", font: "14px sans-serif",
                    editable: true, isMultiline: false,
                   // minSize: new go.Size(20, 26)
                }



            ),

                $("TreeExpanderButton",
                    {
                        width: 14,
                        alignment: go.Spot.Bottom,
                        alignmentFocus: go.Spot.Top,
                        visible: true,
                        "ButtonIcon.stroke": "#000000",
                        "ButtonBorder.fill": "#B48B56",
                        "ButtonBorder.stroke": "transparent",
                        "_buttonFillOver": "#B48B56",
                        "_buttonStrokeOver": "#CEA570"

                    })




)

)

    );


var model = $(go.TreeModel);

var DataArray = JSON.stringify(window.nodeDataArray);
model.nodeDataArray = JSON.parse(DataArray);
myDiagram.model = model;

