<?php

namespace frontend\controllers;

use common\models\Payment\UserBalance;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use common\models\Payment\UserTransaction;




class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    public function actionPay()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            $order = Yii::$app->payment->alfacoins->createOrder($post);
            if($order)
            {

                $transaction = UserTransaction::create($order);
                return [
                    'success' => true,
                    'order' => $order,
                    'transaction' => $transaction,
                    'message' => Yii::t('app', 'paysuccess'),

                ];
            }

            else
            {
                return [
                    'success' => false,
                    'message' => Yii::t('app', 'payerror')

                ];
            }






        }


    }



    public function actionExceed()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
            $data = UserTransaction::setExeeded($post);
            return [
                'status' => $data['status'],
                'message' => $data['message'],
            ];


        }


    }


    public function actionPayaccount()
    {

        if (Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = json_decode(file_get_contents('php://input'), true);
             if(UserBalance::payFromBalance($post))
             {
                 return [
                     'success' => true,
                     'message' => Yii::t('app', 'paysuccess'),

                 ];
             }

             else
                 {
                     return [
                         'success' => false,
                         'message' => Yii::t('app', 'payerror'),

                     ];
                 }












        }


    }


}