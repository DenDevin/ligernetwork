<?php
return [

    'adminEmail' => 'mail@liger.network',
    'supportEmail' => 'mail@liger.network',
    'senderEmail' => 'mail@liger.network',
    'senderName' => 'Liger Network',
    'user.passwordResetTokenExpire' => 3600,
];
