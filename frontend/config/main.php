<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'components' => [

        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',

        ],
        'session' => [

            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],

        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => common\models\Langs::getAllLanguages(),
            'enableDefaultLanguageUrlCode' => true,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'ignoreLanguageUrlPatterns' => [

                '#^api/#' => '#^api/#',
                '#^users/api/#' => '#^users/api/#',

            ],
            'rules' => [
                '' => 'landing/index',
                'id/<token:[\w-]+>' => 'user/registration/register',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                  //  'css' => []
                ],
                'yii\web\JqueryAsset' => [
                 //   'js' => []
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                   // 'js' => []
                ],
            ]

        ],

        'view' => [
            'theme' => [
                'basePath' => '@app/themes/ligertheme/frontend/web',
                'baseUrl' => '@app/web',
                'pathMap' => [
                    '@frontend/modules' => '@themes/ligertheme/frontend/modules',
                    '@frontend/views' => '@themes/ligertheme/frontend/views',
                    '@dektrium/user/views' => '@themes/ligertheme/frontend/modules/users/views'

                ],
            ],
        ],



    ],


    'modules' => [


        'users' => [
            'class' => 'frontend\modules\users\Module',
        ],
    ],





    'params' => $params,
];
