<?php

namespace frontend\widgets\LanguageSelector;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class LanguageSelector extends Widget 
{

 public $array_languages;
 public $language;
 public $layout;

 public function init()
 {
  parent::init();
  $language = Yii::$app->language; //текущий язык
  $languages = Yii::$app->urlManager->languages;
  $this->array_languages = $languages;
  $this->language = $language;

  

 }

public function run()
 {

 	 return $this->render($this->layout, [
                'curr_lang' => $this->language,
                'languages' => $this->array_languages,
        ]);
 }


}