   <?php

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use yii\helpers\Url;

  ?>   
            <div class="lang">
                <ol>
                     <li><a href="<?=Url::to(['', 'language' => 'en'])?>">En</a></li>
                     <li><a href="<?=Url::to(['', 'language' => 'ru'])?>">Ru</a></li>
                </ol>
            </div>