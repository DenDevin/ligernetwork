  <?php

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use yii\helpers\Url;
?>      


          <div class="login-register__lang dropdown show">
            <? foreach(Yii::$app->urlManager->languages as $lang) : ?>
               <? if($lang == Yii::$app->language) : ?>
               <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="photo">
                    <?= Html::img('/images/flags/'.$lang.'.png') ?>
                </div>
                <p class="text"><?=ucfirst($lang)?></p>
            </a>
                 <? endif; ?>
            <? endforeach; ?>
           
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <? foreach(Yii::$app->urlManager->languages as $lang) : ?>
                <a class="dropdown-item" href="<?=Url::to(['', 'language' => $lang])?>">
                    <div class="photo">
                        <?= Html::img('/images/flags/'.$lang.'.png') ?>
                    </div>
                    <p class="text"><?=ucfirst($lang)?></p>
                </a>

                  <?php endforeach; ?>
                
                 
            </div>
        </div>