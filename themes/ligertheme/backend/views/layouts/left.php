<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [

                    [
                        'label' => 'Пользователи',
                        'icon' => 'users',
                        'url' => ['#'],
                        'items' => [

                            ['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/adminuser/admin']],
                            ['label' => 'Должности', 'icon' =>  'bookmark', 'url' => ['/adminrole/role']],
                            ['label' => 'Верификация', 'icon' =>  'bookmark', 'url' => ['/adminuser/verify']],


                        ],


                    ],
                    [
                        'label' => 'Маркетинг',
                        'icon' => 'market',
                        'url' => ['#'],
                        'items' => [

                            ['label' => 'Клиентские DM - бонусы', 'icon' => 'user', 'url' => ['/marketing/client']],
                            ['label' => 'Партнерские DM - бонусы', 'icon' => 'user', 'url' => ['/marketing/business']],
                            ['label' => 'Team - бонусы', 'icon' => 'user', 'url' => ['/marketing/team']],
                            ['label' => 'Статусные бонусы', 'icon' => 'user', 'url' => ['/marketing/status']],
                            ['label' => 'Директ - бонусы', 'icon' => 'user', 'url' => ['/marketing/direct']],


                        ],


                    ],
                    [
                        'label' => 'Биллинг',
                        'icon' => 'money',
                        'url' => ['#'],
                        'items' => [

                            ['label' => 'Платежные системы', 'icon' => 'user', 'url' => ['/billing/payment-system']],
                            ['label' => 'Транзакции', 'icon' =>  'bookmark', 'url' => ['/billing/transaction']],

                        ],


                    ],
                    [
                        'label' => 'Маркетплейс',
                        'icon' => 'users',
                        'url' => ['#'],
                        'items' => [

                            ['label' => 'Пакеты и лицензии', 'icon' => 'user', 'url' => ['/admin/package']],
                            ['label' => 'Мероприятия', 'icon' =>  'bookmark', 'url' => ['/admin/event']],
                            ['label' => 'Слитки', 'icon' =>  'bookmark', 'url' => ['/admin/bars']],

                        ],


                    ],


                         [
                         'label' => 'Локализация',
                         'icon' => 'globe',
                         'url' => '#',
                         'items' => [

                             ['label' => 'Страны', 'icon' => 'flag', 'url' => ['/admin/countries'],],
                             ['label' => 'Языки', 'icon' =>  'language', 'url' => ['/admin/langs'],],

                         ],
                     ],

                   /*
                        [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    */
                ],
            ]
        ) ?>

    </section>

</aside>
