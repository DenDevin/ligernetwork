<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\adminuser\models\Status */

$this->title = 'Добавить пакет';
$this->params['breadcrumbs'][] = ['label' => 'Пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
