<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Verify */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Верификация пользователя', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="verify-view">
<? $form = ActiveForm::begin(['action' => ['verify/notice'], 'options' => ['method' => 'post']]); ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLabel">Уточнение верификации</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Заголовок сообщения</label>
                        <?= $form->field($notification, 'title')->textInput(['class' => 'form-control'])->label(false)?>
                        <label for="exampleInputEmail1">Тип сообщения</label>
                        <?= $form->field($notification, 'type')->
                        dropDownList(
                                [
                                        'success' => 'Успех',
                                        'info' => 'Информация',
                                        'warning' => 'Предупреждение',
                                        'danger' => 'Ошибка',

                                ],
                                ['class' => 'form-control'])->label(false)?>

                        <label for="exampleInputEmail1">Текст сообщения (причина возврата на уточнение)</label>
                        <?= $form->field($notification, 'id')->hiddenInput(['value' => $model->id])->label(false)?>
                        <?= $form->field($notification, 'to_userid')->hiddenInput(['value' => $model->user_id])->label(false)?>
                        <?= $form->field($notification, 'text')->textarea(['class' => 'form-control', 'rows' => 6])->label(false)?>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
    <? ActiveForm::end(); ?>





    <h1><?= Html::encode($this->title) ?></h1>

    <p>


        <? if ($model->isVerified($model->user_id)) : ?>
        <?= Html::a('Верифицирован', ['verify', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <? else: ?>
            <?= Html::a('Подтвердить верификацию', ['verify/confirm', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <? endif; ?>
        <?= Html::a('Отправить на уточнение', ['#'], ['class' => 'btn btn-warning', 'data-toggle' => 'modal', 'data-target' => '#exampleModal']) ?>


        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'surname',
            'name',
            'second_name',
            'date_birth',
            'doc_serial_num',
            'doc_country',
            'doc_date',
            'doc_valid_to',
            [
                'attribute' => 'pass_scan',
                'contentOptions' => [
                    'style' => 'height: 500px;'
                ],
                'value'=> '@web/uploads/image/'.$model->pass_scan,
                'format' => ['image',['width'=>'100%', 'height'=>'100%']],

            ],

            [
                'attribute' => 'pass_photo',
                'contentOptions' => [
                    'style' => 'height: 500px;'
                ],
                'value'=> '@web/uploads/image/'.$model->pass_photo,
                'format' => ['image',['width'=>'100%','height'=>'100%']],

            ],

            [
                'attribute' => 'address_invoice',
                'contentOptions' => [
                    'style' => 'height: 500px;'
                ],
                'value'=> '@web/uploads/image/'.$model->address_invoice,
                'format' => ['image',['width'=>'100%','height'=>'100%']],

            ],
            'address',
            'city',
            'postcode',
            'country_issue',
            'date_issue',
            'qr_code',
          //  'user_id',
          //  'sort',
          //  'active',
          //  'is_new',
          //  'created_at',
          //  'updated_at',
        ],
    ]) ?>

</div>
