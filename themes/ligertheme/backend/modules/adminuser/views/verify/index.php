<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = 'Верификация пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="verify-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'surname',
                'format' => 'raw',
                'contentOptions' => [
                    'style'=>'white-space: normal;',
                    'width'=>'50px;',
                ]

            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'contentOptions' => [
                    'style'=>'white-space: normal;',
                    'width'=>'50px;',
                ]

            ],

            [
                'attribute' => 'second_name',
                'format' => 'raw',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'50px;',
                ]

            ],

            [
                'attribute' => 'date_birth',
                'format' => 'raw',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'40px;',
                ]

            ],

            //'doc_serial_num',
            //'doc_country',
            //'doc_date',
            //'doc_valid_to',
            [
               'attribute' => 'pass_scan',
                'format' => 'html',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'70px;',
                ],
                'value' => function($model)
                {
                    return Html::a(Html::img('@web/uploads/image/'.$model->pass_scan,  ['width' => '80px', 'height' => '80px']), Url::to(['#']));
                }

            ],
            [
                'attribute' => 'pass_photo',
                'format' => 'html',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'70px;',
                ],
                'value' => function($model)
                {
                    return Html::a(Html::img('@web/uploads/image/'.$model->pass_photo,  ['width' => '80px', 'height' => '80px']), Url::to(['#']));
                }

            ],

            [
                'attribute' => 'address_invoice',
                'format' => 'html',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'70px;',
                ],
                'value' => function($model)
                {
                    return Html::a(Html::img('@web/uploads/image/'.$model->address_invoice,  ['width' => '80px', 'height' => '80px']), Url::to(['#']));
                }

            ],

            [
                'attribute' => 'address',
                'format' => 'html',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'60px;',
                ],
            ],
            [
                'attribute' =>  'city',
                'format' => 'html',
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'60px;',
                ],
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'format' => 'html',
                'value' => function($model)
                {
                   if($model->isVerified($model->user_id))
                       {
                           return Html::tag('span', 'Верифицирован', ['class' => 'btn btn-success']);
                       }
                   else
                       {
                           return Html::tag('span', 'Ожидает верификации', ['class' => 'btn btn-danger']);
                       }

                },
                'contentOptions' => [
                    'style'=>'word-wrap: break-word;',
                    'width'=>'70px;',
                ],
            ],


            //'country_issue',
            //'date_issue',
            //'qr_code',
            //'user_id',
            //'sort',
            //'active',
            //'is_new',
            //'created_at',
            //'updated_at',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {verify} {delete}',
                    'buttons' => [

                    'verify' => function ($url,$model,$key) {
                        return Html::a(
                            '<span class="fa fa-check"></span>',
                             Url::to(['verify/confirm', 'id' => $model->user_id]));
                    },
                ],




            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
