<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Verify */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="verify-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_birth')->textInput() ?>

    <?= $form->field($model, 'doc_serial_num')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'doc_date')->textInput() ?>

    <?= $form->field($model, 'doc_valid_to')->textInput() ?>

    <?= $form->field($model, 'pass_scan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postcode')->textInput() ?>

    <?= $form->field($model, 'country_issue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_issue')->textInput() ?>

    <?= $form->field($model, 'pass_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_invoice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qr_code')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'active')->textInput() ?>

    <?= $form->field($model, 'is_new')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
