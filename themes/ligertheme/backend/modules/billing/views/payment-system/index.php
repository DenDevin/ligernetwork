<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\billing\models\search\PaymentSystemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежные системы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-system-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать платежную систему', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'application_title',
            'pass',
            'secret_key',
            //'currency_type',
            //'data:ntext',
            'active',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
