<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\UserTransaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'payment_type',
            'transaction_id',
            'payment_title',
            'invoice_link',
            'payment_summ_btc',
            'payment_summ_eur',
            'payment_system',
            'payment_status',
            'payment_bank',
            'routing',
            'account_number',
            'account_type',
            'sender_number',
            'reciever_number',
            'sender_id',
            'reciever_id',
            'benefitiary_name',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
