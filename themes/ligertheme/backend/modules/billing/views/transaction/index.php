<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\billing\models\search\UserTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Транзакции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-transaction-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',

            [
                'attribute' => 'user_id',
                'value' => function($model)

                {
                    return Html::a($model->getUserById($model->user_id), Url::to(['/adminuser/admin/update', 'id' => $model->user_id]) );
                },

                'format' => 'raw'

            ],
            [
                'attribute' => 'payment_type',
                'filter' => Html::activeDropDownList($searchModel, 'payment_type', ['withdraw' => 'Вывод', 'replenish' => 'Пополнение'], ['prompt' => '', 'class' => 'form-control form-control-sm']),
                'value' => function($model)
                {
                   if($model->payment_type == 'replenishment')
                   {
                       return '<span class="text-success">Пополнение</span>';
                   }
                   else
                       {
                           return '<span class="text-danger">Вывод</span>';
                       }
                },

                'format' => 'raw'

            ],

            'transaction_id',
            //'payment_title',
            //'invoice_link',
            'payment_summ_btc',

            [
              'attribute' => 'payment_summ_eur',
                'format' => 'raw',
                'value' => function($model)
                {
                    return $model->convertToEuro($model->payment_summ_eur);
                }

            ],
            //'payment_system',
            [
                'attribute' => 'payment_status',

                'value' => function($model)
                {
                    if($model->payment_status == 0)
                    {
                        return '<span class="text-warning">В ожидании</span>';
                    }
                    elseif($model->payment_status == 1)
                    {
                        return '<span class="text-success">Подтвержден</span>';
                    }
                    elseif($model->payment_status == 2)
                    {
                        return '<span class="text-danger">Просрочен</span>';
                    }
                },

                'format' => 'raw'

            ],
            //'payment_bank',
            //'routing',
            //'account_number',
            //'account_type',
            //'sender_number',
            //'reciever_number',
            //'sender_id',
            //'reciever_id',
            //'benefitiary_name',
            //'created_at',
            //'updated_at',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete} {approve}',

                    'buttons' => [
                    'approve' => function ($url, $model, $key) {

                      if($model->payment_status == 0 && $model->payment_type == 'withdraw')
                      {
                          return Html::a('Подтвердить', $url, []);
                      }



                    }


                ],



            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
