<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\search\UserTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'payment_type') ?>

    <?= $form->field($model, 'transaction_id') ?>

    <?= $form->field($model, 'payment_title') ?>

    <?php // echo $form->field($model, 'invoice_link') ?>

    <?php // echo $form->field($model, 'payment_summ_btc') ?>

    <?php // echo $form->field($model, 'payment_summ_eur') ?>

    <?php // echo $form->field($model, 'payment_system') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'payment_bank') ?>

    <?php // echo $form->field($model, 'routing') ?>

    <?php // echo $form->field($model, 'account_number') ?>

    <?php // echo $form->field($model, 'account_type') ?>

    <?php // echo $form->field($model, 'sender_number') ?>

    <?php // echo $form->field($model, 'reciever_number') ?>

    <?php // echo $form->field($model, 'sender_id') ?>

    <?php // echo $form->field($model, 'reciever_id') ?>

    <?php // echo $form->field($model, 'benefitiary_name') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
