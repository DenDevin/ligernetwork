<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\UserTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'payment_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transaction_id')->textInput() ?>

    <?= $form->field($model, 'payment_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoice_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_summ_btc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_summ_eur')->textInput() ?>

    <?= $form->field($model, 'payment_system')->textInput() ?>

    <?= $form->field($model, 'payment_status')->textInput() ?>

    <?= $form->field($model, 'payment_bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'routing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sender_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reciever_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sender_id')->textInput() ?>

    <?= $form->field($model, 'reciever_id')->textInput() ?>

    <?//= $form->field($model, 'benefitiary_name')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
