<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\UserTransaction */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Транзакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-transaction-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
