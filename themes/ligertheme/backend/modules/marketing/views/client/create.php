<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\UserBonuses */

$this->title = 'Создать DM - бонус';
$this->params['breadcrumbs'][] = ['label' => 'DM - Бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-bonuses-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
