<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\billing\models\UserBonuses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-bonuses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bonus_id')->textInput() ?>

    <?= $form->field($model, 'min')->textInput() ?>

    <?= $form->field($model, 'max')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?//= $form->field($model, 'data')->textarea(['rows' => 6]) ?>

    <?//= $form->field($model, 'active')->textInput() ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
