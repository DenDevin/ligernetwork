<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\billing\models\search\UserBonusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиент DM - Бонусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-bonuses-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'bonus_id',
            'min',
            'max',
            'amount',
            //'data:ntext',
            //'active',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
