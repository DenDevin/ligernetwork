<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketingDirect */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Директ - бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-marketing-direct-view">

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            'first_pay',
            'level',
            'first_pay_percent',
            'next_pay',
            'next_pay_percent',
            'active',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
