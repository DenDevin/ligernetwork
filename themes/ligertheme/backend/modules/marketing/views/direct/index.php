<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\search\UserMarketingDirectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Директ - бонусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-direct-index">

    <p>
        <?= Html::a('Создать бонус', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'title',
            'first_pay',
            'level',
            'first_pay_percent',
            'next_pay',
            'next_pay_percent',
            'active',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
