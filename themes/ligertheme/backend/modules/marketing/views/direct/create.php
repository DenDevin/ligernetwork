<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketingDirect */

$this->title = 'Создание Директ - бонуса';
$this->params['breadcrumbs'][] = ['label' => 'Директ - бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-direct-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
