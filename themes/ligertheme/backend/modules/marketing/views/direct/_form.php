<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketingDirect */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-marketing-direct-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_pay')->textInput() ?>

    <?= $form->field($model, 'level')->textInput() ?>

    <?= $form->field($model, 'first_pay_percent')->textInput() ?>

    <?= $form->field($model, 'next_pay')->textInput() ?>

    <?= $form->field($model, 'next_pay_percent')->textInput() ?>

    <?= $form->field($model, 'active')->checkbox([1 => 'Активный', 0 => 'Неактивный']) ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
