<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\search\UserPartnerBonusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Партнерские DM - бонусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-partner-bonuses-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'name',
            'partners_count',
            'level',
            'amount',
            //'data:ntext',
            //'active',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
