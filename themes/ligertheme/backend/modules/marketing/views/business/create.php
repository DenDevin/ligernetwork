<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserPartnerBonuses */

$this->title = 'Партнерские DM - бонусы';
$this->params['breadcrumbs'][] = ['label' => 'Маркетинг', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-partner-bonuses-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
