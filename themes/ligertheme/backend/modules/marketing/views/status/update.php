<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketing */

$this->title = 'Обновление бонуса: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статусные бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="user-marketing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
