<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\search\UserMarketingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статусные бонусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-index">

    <p>
        <?= Html::a('Создать бонус', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'title',
            'product_return',
            'income',
            'leader_bonus',
            'month_income',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
