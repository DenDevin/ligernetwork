<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketing */

$this->title = 'Создание бонуса';
$this->params['breadcrumbs'][] = ['label' => 'Статусные бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
