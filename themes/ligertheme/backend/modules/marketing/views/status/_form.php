<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-marketing-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_return')->textInput() ?>

    <?= $form->field($model, 'income')->textInput() ?>

    <?= $form->field($model, 'leader_bonus')->textInput() ?>

    <?= $form->field($model, 'month_income')->textInput() ?>

    <?//= $form->field($model, 'data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
