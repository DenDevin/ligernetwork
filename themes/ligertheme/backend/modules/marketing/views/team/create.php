<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketingTeam */

$this->title = 'Созать Team - бонус';
$this->params['breadcrumbs'][] = ['label' => 'Team - бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-team-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
