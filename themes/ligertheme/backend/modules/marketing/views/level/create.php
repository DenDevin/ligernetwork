<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\marketing\models\UserMarketingLevels */

$this->title = 'Создание бонуса';
$this->params['breadcrumbs'][] = ['label' => 'Level - бонусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-levels-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
