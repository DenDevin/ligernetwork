<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\marketing\models\search\UserMarketingLevelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Level - бонусы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-marketing-levels-index">

    <p>
        <?= Html::a('Создать бонус', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'title',
            'level_id',
            'bonus',
            'percent',
            //'active',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
