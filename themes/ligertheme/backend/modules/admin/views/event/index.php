<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EventSeatch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <p>
        <?= Html::a('Создать мероприятие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            [
                'attribute' => 'text',
                'format' => 'html',
                'contentOptions' => [
                        'style' => 'word-wrap: break-word; white-space:normal;'
                ]


            ],

            [
                'attribute' => 'type',
                'format' => 'html',
                'value'=> function($model)
                {
                    return Html::img('@web/uploads/image/'.$model->type,
                        ['width' => '300px', 'height' => '300px']);
                },


            ],
            'bonus',
            //'cost',
            //'cost_type',
            //'date',
            //'can_buy',
            //'sort',
            //'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
