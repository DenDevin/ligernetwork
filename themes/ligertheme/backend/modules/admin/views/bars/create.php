<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bars */

$this->title = 'Создать слитки';
$this->params['breadcrumbs'][] = ['label' => 'Слитки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bars-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
