<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BarsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слитки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bars-index">

    <p>
        <?= Html::a('Создать слитки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'title',
            'text:ntext',
            'type',
            'weight',
            'cost',
            //'date',
            //'can_buy',
            //'sort',
            //'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
