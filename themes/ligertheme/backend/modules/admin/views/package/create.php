<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Package */

$this->title = 'Создание пакета';
$this->params['breadcrumbs'][] = ['label' => 'Пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
