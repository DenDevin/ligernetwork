<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\marketing\models\UserMarketingDirect;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Package */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([
             'package' => 'Пакет',
             'license' => 'Лицензия'
    ], ['custom-select' => true]) ?>

    <?= $form->field($model, 'pay_term')->dropDownList([
        'monthly' =>  'Ежемесячно',
        'annually' => 'Ежегодно'
    ], ['custom-select' => true]) ?>
    <? if($model->id == $model::PACKAGE_BUSINESS) : ?>
    <?= $form->field($model, 'direct_bonus')->dropDownList(ArrayHelper::map(UserMarketingDirect::find()->all(), 'id', 'name'), ['custom-select' => true]) ?>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Данные о прикрепленном директ - бонусе</h3>
            <div class="box-tools pull-right">
                <span class="label label-danger">Активный</span>
            </div>
        </div>
        <div class="box-body">
            <ul class="nav">
                <li>Название: <b><?=$model->direct->title?></b></li>
                <li>Первый платеж: <b><?=$model->direct->first_pay?> EURO</b></li>
                <li>Уровень: <b><?=$model->direct->level?></b></li>
                <li>Процент за первый платеж: <b><?=$model->direct->first_pay_percent?>%</b></li>
                <li>Каждый следующий платеж: <b><?=$model->direct->next_pay?> EURO</b></li>
                <li>Процент за каждый следующий платеж: <b><?=$model->direct->next_pay_percent?>%</b></li>
            </ul>
        </div>
    </div>
<? endif; ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
