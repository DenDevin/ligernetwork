<?php

namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class GoJsAsset extends AssetBundle
{

    public $sourcePath = '@themes/ligertheme/frontend/web';

    public $css = [
        'js/gojs/assets/css/api.css',
        'js/gojs/assets/css/highlight.css',
        'js/owl/assets/owl.carousel.min.css',


    ];


    public $js = [
        'js/gojs/release/go-debug.js',
        'js/gojs/assets/js/goDoc.js',
        'js/gojs/assets/js/highlight.js',
        'js/owl/owl.carousel.min.js',

    ];


    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}