<?php

namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{

    public $publishOptions = ['forceCopy' => true];
    public $sourcePath = '@themes/ligertheme/frontend/web';
    public $baseUrl = '@web';

    public function init()
    {
        parent::init();

      //  $this->js[] = 'js/vue/app.js';
    }


}