<?php

namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LandingAsset extends AssetBundle
{
   

    public $css = [
    'landing/css/materialdesignicons.min.css',
    'landing/css/fontello.css',
    'landing/css/animate.css',
    'landing/css/style.css',
    'landing/css/responcive.css'
        
    ];


    public $js = [
    'landing/js/jquery.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js',
    'landing/js/animation.gsap.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js',
    'landing/js/DrawSVGPlugin.min.js',
    'landing/js/SplitText.min.js',
    '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js',
    'landing/js/work.js',
    ];

   
    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}