<?php

namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LigerThemeAsset extends AssetBundle
{
    public $sourcePath = '@themes/ligertheme/frontend/web';

    public $css = [
        'fonts/Material_design_icons/css/materialdesignicons.min.css',
        'css/libs.min.css',
        'css/main.css',
        'css/animate.min.css',


    ];
    public $js = [

        'js/libs.min.js',
        'js/common.js',
        'js/notify/notify.min.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',

    ];
}
