<?php


namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MapAsset extends AssetBundle
{
    public $sourcePath = '@themes/ligertheme/frontend/web';

    public $css = [];
    public $js = [
        'js/map/core.js',
        'js/map/maps.js',
        'js/map/worldLow.js',
        'js/map/animated.js',

    ];
    public $depends = [


    ];
}
