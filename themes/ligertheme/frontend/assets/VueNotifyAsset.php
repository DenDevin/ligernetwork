<?php


namespace themes\ligertheme\frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VueNotifyAsset extends AssetBundle
{
    public $sourcePath = '@themes/ligertheme/frontend/web';

    public $css = [];
    public $js = [

      
    ];
    public $depends = [
        

    ];
}
