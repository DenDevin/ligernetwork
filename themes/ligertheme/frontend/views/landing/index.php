<?php


use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\LanguageSelector\LanguageSelector;

?>


<div class="site-container">
    <div id="top"></div>
    <nav id="fixed-navigation">
        <div class="logo">
            <h1>
                <svg version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
                     x="0px" y="0px" width="1203.5px" height="200px" viewBox="0 0 1203.5 200"
                     style="overflow:scroll;enable-background:new 0 0 1203.5 200;" xml:space="preserve">
                    <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .logo-text{fill:#ffffff;}
                        .st1{fill:url(#SVGID_1_);}
                        .st2{fill:url(#SVGID_2_);}
                        .st3{fill:url(#SVGID_3_);}
                        .st4{fill:url(#SVGID_4_);}
                        .st5{fill:url(#SVGID_5_);}
                        .st6{fill:url(#SVGID_6_);}
                        .st7{fill:url(#SVGID_7_);}
                        .st8{fill:url(#SVGID_8_);}
                        .st9{fill:url(#SVGID_9_);}
                        .st10{fill:url(#SVGID_10_);}
                    </style>
                    <defs>
                    </defs>
                    <g>
                        <path class="st0" d="M100.8,0c-2.7,0-5.2,2.2-5.2,5.2v34c0,1.9,1,3.6,2.6,4.5l39,22.5c0.8,0.5,1.7,0.7,2.6,0.7
                            c0.9,0,1.8-0.2,2.6-0.7l29.5-17c3.5-2,3.5-7,0-9L103.3,0.7C102.5,0.2,101.6,0,100.8,0L100.8,0z"/>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="46.1227" y1="-393.1098" x2="141.7536" y2="-237.3239" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st1" d="M46.5,66.3l38.9-22.5c1.6-0.9,2.6-2.7,2.6-4.5v-34c0-4-4.4-6.5-7.8-4.5L11.8,40.2c-3.5,2-3.5,7,0,9l29.5,17
                            C42.9,67.2,44.9,67.2,46.5,66.3z"/>
                        <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-11.8642" y1="-357.514" x2="83.7668" y2="-201.728" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st2" d="M39.9,122.4V77.1c0-1.9-1-3.6-2.6-4.5l-29.5-17c-3.5-2-7.8,0.5-7.8,4.5v79.4c0,4,4.4,6.5,7.8,4.5l29.5-17
                            C38.9,126,39.9,124.3,39.9,122.4z"/>
                        <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="100.0675" y1="-426.2245" x2="195.6985" y2="-270.4385" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st3" d="M146.3,126.9l29.5,17c3.5,2,7.8-0.5,7.8-4.5V60.2c0-4-4.4-6.5-7.8-4.5l-29.5,17c-1.6,0.9-2.6,2.7-2.6,4.5v45.2
                            C143.7,124.2,144.6,126,146.3,126.9z"/>
                        <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="42.1514" y1="-390.672" x2="137.7824" y2="-234.8861" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st4" d="M137.5,133.5l-39.4,22.8c-1.6,0.9-2.6,2.7-2.6,4.5v34c0,4,4.4,6.5,7.8,4.5l68.9-39.8c3.5-2,3.5-7,0-9l-29.5-17
                            C141.1,132.5,139.2,132.5,137.5,133.5z"/>
                        <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-13.2708" y1="-356.6505" x2="82.3602" y2="-200.8645" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st5" d="M85.5,156.2l-39.3-22.7c-1.6-0.9-3.6-0.9-5.2,0l-29.5,17c-3.5,2-3.5,7,0,9l68.8,39.7c3.5,2,7.8-0.5,7.8-4.5
                            v-34C88.1,158.9,87.1,157.1,85.5,156.2z"/>
                    </g>
                    <path class="logo-text" d="M303.4,120.7h28c0,6.6,0,11.1,0,17.7c-15.8,0-32.1,0-47.8,0v-77c6.6,0,13.1,0,19.8,0
                        C303.4,101.2,303.4,87.9,303.4,120.7z M367.7,121.1V78.5h9V61.3h-37.5v17.2h9v42.6h-9.8v17.3h39.1v-17.3H367.7z M385.7,99.9
                        c0.1,26.3,19.9,39.4,39.5,39.4c14.2,0,28.8-6.2,34.3-22.3c2.6-7.5,2.6-14.8,2.2-22.6h-36.4v17h16.4c-3.5,7.4-8.6,9.4-16.5,9.4
                        c-11.6,0-19.1-8.4-19.1-21.1c0-11.8,6.7-21.5,19.1-21.5c7.8,0,12.9,2.6,16.1,9.3h19.4c-3.7-19.1-19.9-27.2-35.4-27.3
                        C405.5,60.4,385.7,73.6,385.7,99.9z M525.3,61.4c-18,0-30.2,0-48.1,0c0,25.5,0,51.2,0,77c17.8,0,31,0,49,0c0-5.8,0-11.8,0-17.5
                        c-11.5,0-18.1,0-29.5,0c0-4.7,0-9,0-13.5H523c0-5.8,0-11.6,0-17.5h-26.2V79c11.3,0,17,0,28.5,0C525.3,73,525.3,67.1,525.3,61.4z
                         M593.3,111.1c19.4-13.2,14-49.6-16.3-49.7c-11.2,0-22.7,0-33.8,0c0,25.7,0,51.4,0,77c6.4,0,13.1,0,19.7,0v-22.7h10.2l13.2,22.7h22
                        v-2.9L593.3,111.1z M576.9,98.2h-14.1c0-6.1,0-13,0-19.2c4.7,0,9.5-0.1,14.1,0C588.2,79,587.6,98.2,576.9,98.2z M709.6,138.5h7V61.3
                        c-6.7,0-13.4,0-20.1,0v37.8L657.6,61h-7.2v77.3c6.7,0,13.5,0,20.4,0v-37.8L709.6,138.5z M784.3,61.4c-18,0-30.2,0-48.1,0
                        c0,25.5,0,51.2,0,77c17.8,0,31,0,49,0c0-5.8,0-11.8,0-17.5c-11.5,0-18.1,0-29.5,0c0-4.7,0-9,0-13.5h26.2c0-5.8,0-11.6,0-17.5h-26.2
                        V79c11.3,0,17,0,28.5,0C784.3,73,784.3,67.1,784.3,61.4z M813.5,79v59.4c6.5,0,13,0,19.5,0V79h18.8c0-5.9,0-11.8,0-17.6h-57.2
                        c0,5.8,0,11.6,0,17.6H813.5z M904.9,62.6l-12.9,41h-0.3l-3.4-13.1l-9.9-29.1h-19.9v2.9l27,74.8h10.9l12.3-37.1h0.3l12.3,37.1h11.3
                        l26.7-74.8v-2.9h-19.6L930,90.4l-3.4,13.1h-0.3l-13.1-41C910.1,62.6,907.9,62.6,904.9,62.6z M1045.2,100c0-53.3-79-53.3-79,0
                        C966.2,153.3,1045.2,153.3,1045.2,100z M986.4,100c0-27.7,38.6-27.7,38.6,0C1025.1,127.9,986.4,127.9,986.4,100z M1110.1,111.1
                        c19.4-13.2,14-49.6-16.3-49.7c-11.2,0-22.7,0-33.8,0c0,25.7,0,51.4,0,77c6.4,0,13.1,0,19.7,0v-22.7h10.2l13.2,22.7h22v-2.9
                        L1110.1,111.1z M1093.7,98.2h-14.1c0-6.1,0-13,0-19.2c4.7,0,9.5-0.1,14.1,0C1104.9,79,1104.4,98.2,1093.7,98.2z M1157.1,138.4V109
                        c2.5,0,5,0,7.6-0.4l16.9,29.8h21.9v-3.1l-20.9-33.9c15.3-9.7,14.8-24.3,14.8-40c-6.8,0-13.5,0-20.2,0c0,14.5,1,29.2-18.7,29.2h-1.4
                        V61.4c-6.9,0-13.4,0-20,0v77C1143.7,138.4,1150.2,138.4,1157.1,138.4z"/>
                </svg>
            </h1>
        </div>
        <ul>
            <li><span><?= Yii::t('frontend.landing', 'about_us') ?></span><a href="#about" class="scroll"><?= Yii::t('frontend.landing', 'about_us') ?></a></li>
            <li><span><?= Yii::t('frontend.landing', 'proposal') ?></span><a href="#offer" class="scroll"><?= Yii::t('frontend.landing', 'proposal') ?></a></li>
            <li><span><?= Yii::t('frontend.landing', 'partnership') ?></span><a href="#for-partners" class="scroll"><?= Yii::t('frontend.landing', 'partnership') ?></a></li>
            <li><span><?= Yii::t('frontend.landing', 'plans') ?></span><a href="#roaad-map" class="scroll"><?= Yii::t('frontend.landing', 'plans') ?></a></li>
        </ul>
        <div class="btns-up-down">
            <a href="#about" class="scroll btn-down"><span><?= Yii::t('frontend.landing', 'button_down') ?></span></a>
            <a href="#top" class="scroll btn-upp"><span class="up-word"><?= Yii::t('frontend.landing', 'button_up') ?></span><span class="arrow"></span></a>
        </div>
    </nav>
    <nav id="mobile-navigation">
        <div class="logo">
            <div>
                <svg viewBox="0 0 1203.5 200">
                    <style type="text/css">
                        .st10{fill:#FFFFFF;}
                        .logo-text1{fill:#ffffff;}
                        .st11{fill:url(#SVGID_11_);}
                        .st12{fill:url(#SVGID_12_);}
                        .st13{fill:url(#SVGID_13_);}
                        .st14{fill:url(#SVGID_14_);}
                        .st15{fill:url(#SVGID_15_);}
                        .st16{fill:url(#SVGID_16_);}
                        .st17{fill:url(#SVGID_17_);}
                        .st18{fill:url(#SVGID_18_);}
                        .st19{fill:url(#SVGID_19_);}
                        .st20{fill:url(#SVGID_20_);}
                    </style>
                    <defs>
                    </defs>
                    <g>
                        <path class="st10" d="M100.8,0c-2.7,0-5.2,2.2-5.2,5.2v34c0,1.9,1,3.6,2.6,4.5l39,22.5c0.8,0.5,1.7,0.7,2.6,0.7
                            c0.9,0,1.8-0.2,2.6-0.7l29.5-17c3.5-2,3.5-7,0-9L103.3,0.7C102.5,0.2,101.6,0,100.8,0L100.8,0z"/>
                        
                            <linearGradient id="SVGID_11_" gradientUnits="userSpaceOnUse" x1="46.1227" y1="-393.1098" x2="141.7536" y2="-237.3239" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st11" d="M46.5,66.3l38.9-22.5c1.6-0.9,2.6-2.7,2.6-4.5v-34c0-4-4.4-6.5-7.8-4.5L11.8,40.2c-3.5,2-3.5,7,0,9l29.5,17
                            C42.9,67.2,44.9,67.2,46.5,66.3z"/>
                        
                            <linearGradient id="SVGID_12_" gradientUnits="userSpaceOnUse" x1="-11.8642" y1="-357.514" x2="83.7668" y2="-201.728" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st12" d="M39.9,122.4V77.1c0-1.9-1-3.6-2.6-4.5l-29.5-17c-3.5-2-7.8,0.5-7.8,4.5v79.4c0,4,4.4,6.5,7.8,4.5l29.5-17
                            C38.9,126,39.9,124.3,39.9,122.4z"/>
                        
                            <linearGradient id="SVGID_13_" gradientUnits="userSpaceOnUse" x1="100.0675" y1="-426.2245" x2="195.6985" y2="-270.4385" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st13" d="M146.3,126.9l29.5,17c3.5,2,7.8-0.5,7.8-4.5V60.2c0-4-4.4-6.5-7.8-4.5l-29.5,17c-1.6,0.9-2.6,2.7-2.6,4.5v45.2
                            C143.7,124.2,144.6,126,146.3,126.9z"/>
                        
                            <linearGradient id="SVGID_14_" gradientUnits="userSpaceOnUse" x1="42.1514" y1="-390.672" x2="137.7824" y2="-234.8861" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st14" d="M137.5,133.5l-39.4,22.8c-1.6,0.9-2.6,2.7-2.6,4.5v34c0,4,4.4,6.5,7.8,4.5l68.9-39.8c3.5-2,3.5-7,0-9l-29.5-17
                            C141.1,132.5,139.2,132.5,137.5,133.5z"/>
                        
                            <linearGradient id="SVGID_15_" gradientUnits="userSpaceOnUse" x1="-13.2708" y1="-356.6505" x2="82.3602" y2="-200.8645" gradientTransform="matrix(1 0 0 1 0 414)">
                            <stop  offset="0.2544" style="stop-color:#CAAD6E"/>
                            <stop  offset="0.4074" style="stop-color:#C5A86A"/>
                            <stop  offset="0.5929" style="stop-color:#B7995E"/>
                            <stop  offset="0.7952" style="stop-color:#9F814A"/>
                            <stop  offset="1" style="stop-color:#80602F"/>
                        </linearGradient>
                        <path class="st15" d="M85.5,156.2l-39.3-22.7c-1.6-0.9-3.6-0.9-5.2,0l-29.5,17c-3.5,2-3.5,7,0,9l68.8,39.7c3.5,2,7.8-0.5,7.8-4.5
                            v-34C88.1,158.9,87.1,157.1,85.5,156.2z"/>
                    </g>
                    <path class="logo-text1" d="M303.4,120.7h28c0,6.6,0,11.1,0,17.7c-15.8,0-32.1,0-47.8,0v-77c6.6,0,13.1,0,19.8,0
                        C303.4,101.2,303.4,87.9,303.4,120.7z M367.7,121.1V78.5h9V61.3h-37.5v17.2h9v42.6h-9.8v17.3h39.1v-17.3H367.7z M385.7,99.9
                        c0.1,26.3,19.9,39.4,39.5,39.4c14.2,0,28.8-6.2,34.3-22.3c2.6-7.5,2.6-14.8,2.2-22.6h-36.4v17h16.4c-3.5,7.4-8.6,9.4-16.5,9.4
                        c-11.6,0-19.1-8.4-19.1-21.1c0-11.8,6.7-21.5,19.1-21.5c7.8,0,12.9,2.6,16.1,9.3h19.4c-3.7-19.1-19.9-27.2-35.4-27.3
                        C405.5,60.4,385.7,73.6,385.7,99.9z M525.3,61.4c-18,0-30.2,0-48.1,0c0,25.5,0,51.2,0,77c17.8,0,31,0,49,0c0-5.8,0-11.8,0-17.5
                        c-11.5,0-18.1,0-29.5,0c0-4.7,0-9,0-13.5H523c0-5.8,0-11.6,0-17.5h-26.2V79c11.3,0,17,0,28.5,0C525.3,73,525.3,67.1,525.3,61.4z
                         M593.3,111.1c19.4-13.2,14-49.6-16.3-49.7c-11.2,0-22.7,0-33.8,0c0,25.7,0,51.4,0,77c6.4,0,13.1,0,19.7,0v-22.7h10.2l13.2,22.7h22
                        v-2.9L593.3,111.1z M576.9,98.2h-14.1c0-6.1,0-13,0-19.2c4.7,0,9.5-0.1,14.1,0C588.2,79,587.6,98.2,576.9,98.2z M709.6,138.5h7V61.3
                        c-6.7,0-13.4,0-20.1,0v37.8L657.6,61h-7.2v77.3c6.7,0,13.5,0,20.4,0v-37.8L709.6,138.5z M784.3,61.4c-18,0-30.2,0-48.1,0
                        c0,25.5,0,51.2,0,77c17.8,0,31,0,49,0c0-5.8,0-11.8,0-17.5c-11.5,0-18.1,0-29.5,0c0-4.7,0-9,0-13.5h26.2c0-5.8,0-11.6,0-17.5h-26.2
                        V79c11.3,0,17,0,28.5,0C784.3,73,784.3,67.1,784.3,61.4z M813.5,79v59.4c6.5,0,13,0,19.5,0V79h18.8c0-5.9,0-11.8,0-17.6h-57.2
                        c0,5.8,0,11.6,0,17.6H813.5z M904.9,62.6l-12.9,41h-0.3l-3.4-13.1l-9.9-29.1h-19.9v2.9l27,74.8h10.9l12.3-37.1h0.3l12.3,37.1h11.3
                        l26.7-74.8v-2.9h-19.6L930,90.4l-3.4,13.1h-0.3l-13.1-41C910.1,62.6,907.9,62.6,904.9,62.6z M1045.2,100c0-53.3-79-53.3-79,0
                        C966.2,153.3,1045.2,153.3,1045.2,100z M986.4,100c0-27.7,38.6-27.7,38.6,0C1025.1,127.9,986.4,127.9,986.4,100z M1110.1,111.1
                        c19.4-13.2,14-49.6-16.3-49.7c-11.2,0-22.7,0-33.8,0c0,25.7,0,51.4,0,77c6.4,0,13.1,0,19.7,0v-22.7h10.2l13.2,22.7h22v-2.9
                        L1110.1,111.1z M1093.7,98.2h-14.1c0-6.1,0-13,0-19.2c4.7,0,9.5-0.1,14.1,0C1104.9,79,1104.4,98.2,1093.7,98.2z M1157.1,138.4V109
                        c2.5,0,5,0,7.6-0.4l16.9,29.8h21.9v-3.1l-20.9-33.9c15.3-9.7,14.8-24.3,14.8-40c-6.8,0-13.5,0-20.2,0c0,14.5,1,29.2-18.7,29.2h-1.4
                        V61.4c-6.9,0-13.4,0-20,0v77C1143.7,138.4,1150.2,138.4,1157.1,138.4z"/>
                </svg>
            </div>
        </div>
        <div class="mobile-nav-list">
            <div class="word-bg"><?= Yii::t('frontend.landing', 'menu') ?></div>
            <ul>
                <li><a href="#about" class="scroll"><?= Yii::t('frontend.landing', 'about_us') ?></a></li>
                <li><a href="#offer" class="scroll"><?= Yii::t('frontend.landing', 'proposal') ?></a></li>
                <li><a href="#for-partners" class="scroll"><?= Yii::t('frontend.landing', 'partnership') ?></a></li>
                <li><a href="#roaad-map" class="scroll"><?= Yii::t('frontend.landing', 'plans') ?></a></li>
            </ul>
        </div>
        <div id="hamburger"><span></span></div>
    </nav>
    <div class="site-content">
        <header class="hero">
            <div class="full-screen-slider">
                <div class="slider-container">
                    <div class="fs-slide-item">
                        <div class="slide-bg-img" data-slide="1" style="background-image: url('/images/1.jpg');"></div>
                        <div class="slide-wrapper">
                            <div class="slide_text">
                                <div class="fs-text-wrapper" data-slide="1">
                                    <div class="slide-text">
                                        <div class="slide_title textStagger"><span><?= Yii::t('frontend.landing', 'gold') ?></span><?= Yii::t('frontend.landing', 'title_gold') ?></div>
                                        <div class="slide-subtitle textStagger"><?= Yii::t('frontend.landing', 'gold_text_1') ?><br><?= Yii::t('frontend.landing', 'gold_text_2') ?></div>
                                        <div class="slide-btn-container">
                                            <a href="#about" class="scroll slide-btn"><?= Yii::t('frontend.landing', 'know_more') ?> <i class="mdi mdi-chevron-down"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-image-container">
                                <div class="fs-img-slides">
                                    <div class="fs-triger"></div>
                                    <div class="slide-image" data-slide="1">
                                        <div class="fs-image" style="background-image: url('/images/1.jpg');"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fs-slide-item">
                        <div class="slide-bg-img" data-slide="2" style="background-image: url('/images/2-1.jpg');"></div>
                        <div class="slide-wrapper">
                            <div class="slide_text">
                                <div class="fs-text-wrapper" data-slide="2">
                                    <div class="slide-text">
                                        <div class="slide_title textStagger"><?= Yii::t('frontend.landing', 'buy') ?></div>
                                        <div class="slide-subtitle textStagger"><?= Yii::t('frontend.landing', 'buy_text_1') ?></div>
                                        <div class="slide-btn-container">
                                            <a href="#about" class="scroll slide-btn"><?= Yii::t('frontend.landing', 'know_more') ?> <i class="mdi mdi-chevron-down"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-image-container">
                                <div class="fs-img-slides">
                                    <div class="fs-triger"></div>
                                    <div class="slide-image" data-slide="2">       
                                        <div class="fs-image" style="background-image: url('/images/2-1.jpg');"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fs-slide-item">
                        <div class="slide-bg-img" data-slide="3" style="background-image: url('/images/3-1.jpg');"></div>
                        <div class="slide-wrapper">
                            <div class="slide_text">
                                <div class="fs-text-wrapper" data-slide="3">
                                    <div class="slide-text"> 
                                        <div class="slide_title textStagger"><?= Yii::t('frontend.landing', 'boss_title_1') ?></div>
                                        <div class="slide-subtitle textStagger"><?= Yii::t('frontend.landing', 'boss_title_2') ?></div>
                                        <div class="slide-btn-container">
                                            <a href="#about" class="scroll slide-btn"><?= Yii::t('frontend.landing', 'know_more') ?> <i class="mdi mdi-chevron-down"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-image-container">
                                <div class="fs-img-slides">
                                    <div class="fs-triger"></div>
                                    <div class="slide-image" data-slide="3">
                                        <div class="fs-image" style="background-image: url('/images/3-1.jpg');"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navigation-container">
                    <div class="navigation-arrows">
                        <div class="prev-btn arrow">
                            <a href="#">
                                <!-- <svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon">
                                    <path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
                                </svg> -->
                                <i class="mdi mdi-chevron-left"></i>
                            </a>
                        </div>
                        <div class="next-btn arrow">
                            <a href="#">
                                <svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon">
                                    <path class="path circle" id="circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path>
                                </svg>
                                <i class="mdi mdi-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="navigation-dots">
                        <div class="dot" data-slide="1"><a href="#"><span>01</span></a></div>
                        <div class="dot" data-slide="2"><a href="#"><span>02</span></a></div>
                        <div class="dot" data-slide="3"><a href="#"><span>03</span></a></div>
                    </div>
                </div>
            </div>

        <!---------------- Language START ---------------->
        <?= LanguageSelector::widget([
         'layout' => 'landing_top'
        ]) ?>
        <!---------------- Language END ---------------->

            <div class="social-links">
                <ul>
                    <li><a href="#" target="blank">Facebook</a></li>
                    <li><a href="#" target="blank">Instagram</a></li>
                    <li><a href="#" target="blank">YouTube</a></li>
                </ul>
            </div>
        </header>
        <section id="about">
            <div class="word-bg">about</div>
            <div id="particles-js"></div>
            <div class="wrapper">
                <div class="half-container">
                    <div class="half-box">
                        <div class="">
                            <svg viewBox="0 0 95 110" class="car-polygon clipAnim">
                                <clipPath id="carClip" clipPathUnits="objectBoundingBox">
                                    <polygon points="0.5,0 1,0.25 1,0.75 0.5,1 0,0.75 0,0.25"/>
                                </clipPath>
                                <image clip-path="url('#carClip')" width="95" height="110" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/5-1.jpg" />
                            </svg>
                        </div>
                        <div class="poly-container">
                            <svg class="small-white-octa clipAnim" version="1.1" x="0px" y="0px">
                                <path d="M156.2,45.1v90.2l-78.1,45.1L0,135.2l0-90.2L78.1,0L156.2,45.1z"/>
                            </svg>
                            <div class="about-mask">
                                <svg viewBox="0 0 321 349.8" class="masc-container">
                                    <clipPath id="myClip">
                                        <path class="clipAnim" d="M72.2,115.9L20.6,86.1c-6.1-3.5-6.1-12.3,0-15.8L140.3,1.2c6.1-3.5,13.7,0.9,13.7,7.9v59.5
                                            c0,3.2-1.8,6.3-4.6,7.9l-68.1,39.3C78.5,117.5,75.1,117.5,72.2,115.9z"/>
                                        <path class="clipAnim" d="M65.2,222.1l-51.5,29.7C7.6,255.3,0,251,0,243.9V105.2c0-7,7.6-11.4,13.7-7.9L65.2,127
                                            c2.8,1.6,4.6,4.7,4.6,7.9v79.2C69.8,217.4,68.1,220.4,65.2,222.1z"/>
                                        <path class="clipAnim" d="M154,281.1v59.5c0,7-7.6,11.4-13.7,7.9L20,279.1c-6.1-3.5-6.1-12.3,0-15.8l51.5-29.8c2.8-1.6,6.3-1.6,9.1,0
                                            l68.8,39.7C152.3,274.8,154,277.9,154,281.1z"/>
                                        <path class="clipAnim" d="M249.7,233.4l51.5,29.7c6.1,3.5,6.1,12.3,0,15.8l-120.5,69.6c-6.1,3.5-13.7-0.9-13.7-7.9v-59.5
                                            c0-3.2,1.8-6.3,4.6-7.9l68.9-39.8C243.4,231.8,246.8,231.8,249.7,233.4z"/>
                                        <path class="clipAnim" d="M251.2,214v-79c0-3.2,1.8-6.3,4.6-7.9l51.5-29.7c6.1-3.5,13.7,0.9,13.7,7.9v138.4c0,7-7.6,11.4-13.7,7.9
                                            l-51.5-29.7C253,220.3,251.3,217.2,251.2,214z"/>
                                        <path class="clipAnim" d="M176.2,0c1.5,0,3,0.4,4.5,1.2l119.8,69.2c6.1,3.5,6.1,12.3,0,15.8L249,115.9c-1.4,0.8-3,1.2-4.6,1.2
                                            c-1.6,0-3.2-0.4-4.6-1.2l-68.2-39.4c-2.8-1.6-4.6-4.7-4.6-7.9V9.1C167,3.8,171.4,0,176.2,0L176.2,0z"/>
                                        <path class="clipAnim" d="M160.5,265.5l-78.4-45.3v-90.6l78.4-45.3l78.4,45.3v90.6L160.5,265.5z"/>
                                    </clipPath>
                                    <image clip-path="url('#myClip')" width="321" height="352" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/4-1.jpg" />
                                </svg>
                            </div>
                            <svg class="big-octa clipAnim" version="1.1" x="0px" y="0px">
                                <path d="M156.2,45.1v90.2l-78.1,45.1L0,135.2l0-90.2L78.1,0L156.2,45.1z"/>
                            </svg>
                            <svg class="big-white-octa clipAnim" version="1.1" x="0px" y="0px">
                                <path d="M156.2,45.1v90.2l-78.1,45.1L0,135.2l0-90.2L78.1,0L156.2,45.1z"/>
                            </svg>
                        </div>
                        <div>
                            <svg class="man-polygon clipAnim" viewBox="0 0 216 250">
                                <clipPath id="manClip" clipPathUnits="objectBoundingBox">
                                    <polygon points="0.5,0 1,0.25 1,0.75 0.5,1 0,0.75 0,0.25"/>
                                </clipPath>
                                <image clip-path="url('#manClip')" width="216" height="250" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/6-1.jpg" />
                            </svg>
                        </div>
                    </div>
                    <div class="half-box">
                        <div class="desc-container">
                            <div class="box-title">
                                <h2><?= Yii::t('frontend.landing', 'about_company_title') ?></h2>
                            </div>
                            <div class="box-desc">
                                <?= Yii::t('frontend.landing', 'about_company_text') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="offer">
            <div class="word-bg">offer</div>
            <div class="wrapper">
                <div class="half-container">
                    <div class="half-box">
                        <div class="left-desc-container">
                            <div class="box-title">
                                <h2><?= Yii::t('frontend.landing', 'what_we_offer') ?></h2>
                            </div>
                            <div class="box-desc">
                                <?= Yii::t('frontend.landing', 'what_we_offer_text') ?>
                            </div>
                        </div>
                    </div>
                    <div class="half-box text-center">
                        <div class="flipContainer">
                            <div class="iconContainer dollar"><i class="icon-dollar"></i></div>
                            <div class="iconContainer gold"><i class="icon-gold"></i></div>
                            <div class="iconContainer rub"><i class="icon-rub"></i></div>
                            <div class="iconContainer percent"><i class="icon-percent"></i></div>
                            <div class="iconContainer bitcoin"><i class="icon-bitcoin"></i></div>
                            <div class="iconContainer lire"><i class="icon-lire"></i></div>
                            <div class="iconContainer euro"><i class="icon-euro"></i></div>
                            <div class="iconContainer ethereum"><i class="icon-ethereum"></i></div>
                            <div class="flipInner">
                                <img src="/images/100bg.gif" class="gif-coin" alt="">
                                <!-- <div class="circleCoin">
                                    <img src="images/coinFace.png" class="coin face" alt="">
                                    <img src="images/coinBack.png" class="coin back" alt="">
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="safe">
            <div class="word-bg">safe</div>
            <div class="wrapper">
                <div class="box-title">
                    <h2> <?= Yii::t('frontend.landing', 'safe_title') ?></h2>
                </div>
                <div class="sota-container">
                    <div class="sota-bg">
                        <div class="sota-wrapper">
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                                <!-- <div class="si-bg"></div> -->
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-item"></div>
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-item">
                                <svg viewBox="0 0 271 234" class="sota-safe si-bg">
                                    <clipPath id="sotaPath" clipPathUnits="objectBoundingBox">
                                        <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                                    </clipPath>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="sota-steps-container">
                        <div class="sota-wrapper">
                            <div class="sota-safe-step">
                                <div class="sota-st-container">
                                    <div class="sota-st-number">01</div>
                                    <div class="sota-st-text"><?= Yii::t('frontend.landing', 'warranty_01') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-safe-step">
                                <div class="sota-st-container">
                                    <div class="sota-st-number">02</div>
                                    <div class="sota-st-text"><?= Yii::t('frontend.landing', 'warranty_02') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-safe-step">
                                <div class="sota-st-container">
                                    <div class="sota-st-number">03</div>
                                    <div class="sota-st-text"><?= Yii::t('frontend.landing', 'warranty_03') ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sota-steps-container">
                        <div class="sota-wrapper">
                            <div class="sota-safe-step">
                                <div class="sota-st-container">
                                    <div class="sota-st-number">04</div>
                                    <div class="sota-st-text"><?= Yii::t('frontend.landing', 'warranty_04') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="sota-wrapper">
                            <div class="sota-safe-step">
                                <div class="sota-st-container">
                                    <div class="sota-st-number">05</div>
                                    <div class="sota-st-text"><?= Yii::t('frontend.landing', 'warranty_05') ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <svg viewBox="0 0 271 234" class="sota-safe">
                <clipPath id="sotaPath1" clipPathUnits="objectBoundingBox">
                    <polygon class="st0" points="0.25,0 0.75,0 1,0.5 0.75,1 0.25,1 0,0.5"/>
                </clipPath>
            </svg>
        </section>
        <section id="action" class="action-box">
            <svg viewBox="0 0 475.9 518.6" class="action-symbol">
                <path d="M369.1,171.9l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8c-9-5.2-20.3,1.3-20.3,11.7v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4
                    C359.8,174.3,365,174.3,369.1,171.9z"/>
                <path d="M120.5,171.8l101-58.3c4.2-2.4,6.8-6.9,6.8-11.7V13.5C228.3,3.1,217-3.4,208,1.8L30.5,104.3c-9,5.2-9,18.2,0,23.4
                    l76.4,44.1C111.2,174.2,116.3,174.2,120.5,171.8z"/>
                <path d="M103.5,317.5V200c0-4.8-2.6-9.3-6.8-11.7l-76.4-44.1c-9-5.2-20.3,1.3-20.3,11.7v205.8c0,10.4,11.3,16.9,20.3,11.7
                    l76.4-44.1C100.9,326.8,103.5,322.4,103.5,317.5z"/>
                <path d="M379.2,329l76.4,44.1c9,5.2,20.3-1.3,20.3-11.7V156.1c0-10.4-11.3-16.9-20.3-11.7l-76.4,44.1c-4.2,2.4-6.8,6.9-6.8,11.7
                    v117.1C372.5,322.1,375,326.6,379.2,329z"/>
                <path d="M356.6,346.1l-102.2,59c-4.2,2.4-6.8,6.9-6.8,11.7v88.3c0,10.4,11.3,16.9,20.3,11.7l178.6-103.1c9-5.2,9-18.2,0-23.4
                    l-76.4-44.1C365.9,343.7,360.8,343.7,356.6,346.1z"/>
                <path d="M221.5,405.1l-102-58.9c-4.2-2.4-9.3-2.4-13.5,0l-76.4,44.1c-9,5.2-9,18.2,0,23.4l178.4,103c9,5.2,20.3-1.3,20.3-11.7
                        v-88.3C228.3,412,225.7,407.5,221.5,405.1z"/>
                <path d="M261.2,0c-7.1,0-13.5,5.6-13.5,13.5v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4c2.1,1.2,4.4,1.8,6.8,1.8
                    c2.3,0,4.7-0.6,6.8-1.8l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8C265.7,0.6,263.4,0,261.2,0L261.2,0z"/>
            </svg>
            <div class="paralax_wrapp">
                <div class="paralax_bg" style="background-image: url('/images/7.jpg')"></div>
            </div>
            <div class="wrapper text-center">
                <h3 class="action-title"><?= Yii::t('frontend.landing', 'begin_now') ?></h3>
                <p class="action-subtitle"><?= Yii::t('frontend.landing', 'begin_text') ?></p>
                <a href="<?=Url::to(['/user/registration/register'])?>" class="btn-gold"><?= Yii::t('frontend.landing', 'registration') ?></a>
            </div>
        </section>
        <section id="facts">
            <div class="word-bg">facts</div>
            <div class="wrapper">
                <div class="box-title vertical">
                    <h2><?= Yii::t('frontend.landing', 'facts_about_gold') ?></h2>
                </div>
                <div class="primes-container">
                    <div class="prime-box">
                        <div class="p-number facts-anim">01</div>
                        <div class="p-desc facts-anim"><?= Yii::t('frontend.landing', 'fact_01') ?></div>
                    </div>
                    <div class="prime-box">
                        <div class="p-number facts-anim">02</div>
                        <div class="p-desc facts-anim"><?= Yii::t('frontend.landing', 'fact_02') ?></div>
                    </div>
                    <div class="prime-box">
                        <div class="p-number facts-anim">03</div>
                        <div class="p-desc facts-anim"><?= Yii::t('frontend.landing', 'fact_03') ?></div>
                    </div>
                    <div class="prime-box">
                        <div class="p-number facts-anim">04</div>
                        <div class="p-desc facts-anim"><?= Yii::t('frontend.landing', 'fact_04') ?></div>
                    </div>
                    <div class="prime-box">
                        <div class="p-number facts-anim">05</div>
                        <div class="p-desc facts-anim"><?= Yii::t('frontend.landing', 'fact_05') ?></div>
                    </div>
                </div>
                <div class="primes-img">
                    <div class="img-par">
                        <img src="/images/gold.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section id="for-partners">
            <div class="word-bg">For <br><span>Partners</span></div>
            <div class="wrapper">
                <div class="half-container">
                    <div class="half-box">
                        <svg viewBox="0 0 425 492" class="forPartnersSota fp-anim">
                            <clipPath id="partnersPath" clipPathUnits="objectBoundingBox">
                                <polygon points="0.5,0 1,0.25 1,0.75 0.5,1 0,0.75 0,0.25"> 
                            </clipPath>
                        </svg>
                        <div class="fp-sota-wrap">
                            <svg viewBox="0 0 263 303.7" class="fp-s1 fp-anim">
                                <defs>
                                </defs>
                                <path d="M131.5,303.7L0,227.8V75.9L131.5,0L263,75.9v151.8L131.5,303.7z"/>
                            </svg>
                            <svg viewBox="0 0 263 303.7" class="fp-s2 fp-anim">
                                <defs>
                                </defs>
                                <path d="M131.5,303.7L0,227.8V75.9L131.5,0L263,75.9v151.8L131.5,303.7z"/>
                            </svg>
                            <svg viewBox="0 0 475.9 518.6" class="fp-logo-icon fp-anim">
                                <defs>
                                    <style>
                                        .white{fill: #fff;}
                                    </style>
                                </defs>
                                <path class="white" d="M369.1,171.9l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8c-9-5.2-20.3,1.3-20.3,11.7v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4
                                    C359.8,174.3,365,174.3,369.1,171.9z"/>
                                <path class="white" d="M120.5,171.8l101-58.3c4.2-2.4,6.8-6.9,6.8-11.7V13.5C228.3,3.1,217-3.4,208,1.8L30.5,104.3c-9,5.2-9,18.2,0,23.4
                                    l76.4,44.1C111.2,174.2,116.3,174.2,120.5,171.8z"/>
                                <path class="white" d="M103.5,317.5V200c0-4.8-2.6-9.3-6.8-11.7l-76.4-44.1c-9-5.2-20.3,1.3-20.3,11.7v205.8c0,10.4,11.3,16.9,20.3,11.7
                                    l76.4-44.1C100.9,326.8,103.5,322.4,103.5,317.5z"/>
                                <path class="white" d="M379.2,329l76.4,44.1c9,5.2,20.3-1.3,20.3-11.7V156.1c0-10.4-11.3-16.9-20.3-11.7l-76.4,44.1c-4.2,2.4-6.8,6.9-6.8,11.7
                                    v117.1C372.5,322.1,375,326.6,379.2,329z"/>
                                <path class="white" d="M356.6,346.1l-102.2,59c-4.2,2.4-6.8,6.9-6.8,11.7v88.3c0,10.4,11.3,16.9,20.3,11.7l178.6-103.1c9-5.2,9-18.2,0-23.4
                                    l-76.4-44.1C365.9,343.7,360.8,343.7,356.6,346.1z"/>
                                <path class="white" d="M221.5,405.1l-102-58.9c-4.2-2.4-9.3-2.4-13.5,0l-76.4,44.1c-9,5.2-9,18.2,0,23.4l178.4,103c9,5.2,20.3-1.3,20.3-11.7
                                        v-88.3C228.3,412,225.7,407.5,221.5,405.1z"/>
                                <path class="white" d="M261.2,0c-7.1,0-13.5,5.6-13.5,13.5v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4c2.1,1.2,4.4,1.8,6.8,1.8
                                    c2.3,0,4.7-0.6,6.8-1.8l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8C265.7,0.6,263.4,0,261.2,0L261.2,0z"/>
                            </svg>
                            <div class="fp-sota-box fp-anim" style="background-image: url('images/9.jpg');"></div>
                            <div class="fb-sota-bottom fp-anim" style="background-image: url('images/10.jpg');"></div>
                            <svg viewBox="0 0 263 303.7" class="fp-s3 fp-anim">
                                <defs>
                                </defs>
                                <path d="M131.5,303.7L0,227.8V75.9L131.5,0L263,75.9v151.8L131.5,303.7z"/>
                            </svg>
                            <svg viewBox="0 0 263 303.7" class="fp-s4 fp-anim">
                                <defs>
                                </defs>
                                <path d="M131.5,303.7L0,227.8V75.9L131.5,0L263,75.9v151.8L131.5,303.7z"/>
                            </svg>
                            <svg viewBox="0 0 263 303.7" class="fp-s5 fp-anim">
                                <defs>
                                </defs>
                                <path d="M131.5,303.7L0,227.8V75.9L131.5,0L263,75.9v151.8L131.5,303.7z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="half-box">
                        <div class="box-title">
                            <h2><?= Yii::t('frontend.landing', 'partners_title') ?></h2>
                        </div>
                        <p class="subtitle"><?= Yii::t('frontend.landing', 'partners_subtitle') ?></p>
                        <ul class="fp-primes">
                            <li>
                                <div class="fp-icon"><i class="icon-referal"></i></div>
                                <div class="fp-desc"><?= Yii::t('frontend.landing', 'icon_refferal') ?></div>
                            </li>
                            <li>
                                <div class="fp-icon"><i class="icon-marketing"></i></div>
                                <div class="fp-desc"><?= Yii::t('frontend.landing', 'icon_marketing') ?></div>
                            </li>
                            <li>
                                <div class="fp-icon"><i class="icon-team"></i></div>
                                <div class="fp-desc"><?= Yii::t('frontend.landing', 'icon_team') ?></div>
                            </li>
                            <li>
                                <div class="fp-icon"><i class="icon-diamond"></i></div>
                                <div class="fp-desc"><?= Yii::t('frontend.landing', 'icon_diamond') ?></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section id="roaad-map">
            <div class="word-bg">Road Map</div>
            <div class="wrapper text-center">
                <div class="box-title"><h2><?= Yii::t('frontend.landing', 'roadmap_title') ?></h2></div>
                <div class="road-steps">
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_1') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_1') ?></p>
                        <div class="before gold-color"></div>
                    </div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_2') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_2') ?></p>
                        <div class="before gold-color"></div>
                    </div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_3') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_3') ?></p>
                        <div class="before gold-color"></div>
                    </div>
                    <div class="r-step"></div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_4') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_4') ?></p>
                        <div class="before"></div>
                    </div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_5') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_5') ?></p>
                        <div class="before gold-color"></div>
                        <div class="after"></div>
                    </div>
                    <div class="r-step"></div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_6') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_6') ?></p>
                        <div class="before"></div>
                    </div>
                    <div class="r-step">
                        <p class="r-desc"><?= Yii::t('frontend.landing', 'r_desc_7') ?></p>
                        <p class="r-date"><?= Yii::t('frontend.landing', 'r_date_7') ?></p>
                        <div class="before"></div>
                    </div>
                </div>
            </div>
            <div class="road-container">
                <svg class="road-line" viewBox="0 0 1440 1024">
                    <defs>
                        <filter filterUnits="userSpaceOnUse" id="Filter_0" x="-2px" y="7px">
                            <feOffset in="SourceAlpha" dx="0" dy="60" />
                            <feGaussianBlur result="blurOut" stdDeviation="4.472" />
                            <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="1"/></feComponentTransfer>
                            <feMerge>
                            <feMergeNode/>
                            <feMergeNode in="SourceGraphic"/>
                          </feMerge>
                        </filter>
                        <filter filterUnits="userSpaceOnUse" id="Filter_1" x="8px" y="-2px">
                            <feOffset in="SourceAlpha" dx="0" dy="0" />
                            <feGaussianBlur result="blurOut" stdDeviation="3.162" />
                            <feFlood flood-color="rgb(213, 167, 67)" result="floodOut" />
                            <feComposite operator="atop" in="floodOut" in2="blurOut" />
                            <feComponentTransfer><feFuncA type="linear" slope="0.7"/></feComponentTransfer>
                            <feMerge>
                            <feMergeNode/>
                            <feMergeNode in="SourceGraphic"/>
                          </feMerge>
                        </filter>
                    </defs>
                    <g filter="url(#Filter_0)">
                        <!-- <path  fill-rule="evenodd"  stroke="rgb(255, 255, 255)" stroke-width="0px" stroke-linecap="round" stroke-linejoin="miter" fill="#fff" d="M1444,929c-1.1,0-2-0.9-2-2c0-192-209.1-208.6-251-210c-0.3,0-0.7,0-1-0.1v0H540c-70,0-127-57-127-127
                        s57-127,127-127c34.5,0,172.5-0.1,306-0.3c133.5-0.1,271.5-0.3,306-0.3c67.8,0,123-55.2,123-123s-55.2-123-123-123
                        c-68.3,0-889.7-0.5-898-0.5l-2.1,0l0-0.1c-22.3-0.8-77.8-5.4-131.9-29.9c-35.1-15.9-63-37.4-82.9-63.8C12.5,89.5,0,49.1,0,2
                        c0-1.1,0.9-2,2-2s2,0.9,2,2c0,192,207.4,208.6,249,210c0.4,0,0.7,0,1,0c8.8,0,829.8,0.5,898,0.5c70,0,127,57,127,127
                        s-57,127-127,127c-34.5,0-172.5,0.1-306,0.3c-133.5,0.1-271.5,0.3-306,0.3c-67.8,0-123,55.2-123,123s55.2,123,123,123h652.1l0,0.1
                        c22.5,0.8,78.4,5.4,133,29.9c35.4,15.9,63.5,37.4,83.5,63.8c24.8,32.7,37.4,73.1,37.4,120.2C1446,928.1,1445.1,929,1444,929z"/> -->
                        <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="round" stroke-linejoin="miter" fill="none"
                         d="M1465.000,938.000 C1465.000,726.236 1210.987,726.334 1211.000,726.000 C1211.000,726.000 630.037,726.001 561.001,726.001 C491.965,726.001 436.000,670.036 436.000,601.000 C436.000,531.964 491.965,475.999 561.001,475.999 C630.037,475.999 1103.963,475.501 1172.999,475.501 C1242.035,475.501 1298.000,419.536 1298.000,350.500 C1298.000,281.464 1242.035,225.499 1172.999,225.499 C1103.963,225.499 275.000,225.000 275.000,225.000 C275.013,224.667 23.000,224.765 23.000,13.000 "/>
                    </g>
                    <g filter="url(#Filter_1)">
                        <path class="road-gold" fill-rule="evenodd"  stroke="rgb(213, 167, 67)" stroke-width="4px" stroke-linecap="round" stroke-linejoin="miter" fill="none"
                         d="M23.000,13.000 C23.000,224.765 275.013,224.667 275.000,225.000 C275.000,225.000 1103.963,225.499 1172.999,225.499 C1242.035,225.499 1298.000,281.464 1298.000,350.500 C1298.000,419.536 1242.035,475.501 1172.999,475.501 C1103.963,475.501 1121.036,476.000 1052.000,476.000 "/>
                    </g>
                </svg>
            </div>
        </section>
        <section id="action2" class="action-box">
            <svg viewBox="0 0 475.9 518.6" class="action-symbol">
                <path d="M369.1,171.9l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8c-9-5.2-20.3,1.3-20.3,11.7v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4
                    C359.8,174.3,365,174.3,369.1,171.9z"/>
                <path d="M120.5,171.8l101-58.3c4.2-2.4,6.8-6.9,6.8-11.7V13.5C228.3,3.1,217-3.4,208,1.8L30.5,104.3c-9,5.2-9,18.2,0,23.4
                    l76.4,44.1C111.2,174.2,116.3,174.2,120.5,171.8z"/>
                <path d="M103.5,317.5V200c0-4.8-2.6-9.3-6.8-11.7l-76.4-44.1c-9-5.2-20.3,1.3-20.3,11.7v205.8c0,10.4,11.3,16.9,20.3,11.7
                    l76.4-44.1C100.9,326.8,103.5,322.4,103.5,317.5z"/>
                <path d="M379.2,329l76.4,44.1c9,5.2,20.3-1.3,20.3-11.7V156.1c0-10.4-11.3-16.9-20.3-11.7l-76.4,44.1c-4.2,2.4-6.8,6.9-6.8,11.7
                    v117.1C372.5,322.1,375,326.6,379.2,329z"/>
                <path d="M356.6,346.1l-102.2,59c-4.2,2.4-6.8,6.9-6.8,11.7v88.3c0,10.4,11.3,16.9,20.3,11.7l178.6-103.1c9-5.2,9-18.2,0-23.4
                    l-76.4-44.1C365.9,343.7,360.8,343.7,356.6,346.1z"/>
                <path d="M221.5,405.1l-102-58.9c-4.2-2.4-9.3-2.4-13.5,0l-76.4,44.1c-9,5.2-9,18.2,0,23.4l178.4,103c9,5.2,20.3-1.3,20.3-11.7
                        v-88.3C228.3,412,225.7,407.5,221.5,405.1z"/>
                <path d="M261.2,0c-7.1,0-13.5,5.6-13.5,13.5v88.3c0,4.8,2.6,9.3,6.8,11.7l101.2,58.4c2.1,1.2,4.4,1.8,6.8,1.8
                    c2.3,0,4.7-0.6,6.8-1.8l76.4-44.1c9-5.2,9-18.2,0-23.4L267.9,1.8C265.7,0.6,263.4,0,261.2,0L261.2,0z"/>
            </svg>
            <div class="paralax_wrapp">
                <div class="paralax_bg" style="background-image: url('/images/11.jpg')"></div>
            </div>
            <div class="wrapper text-center">
                <h3 class="action-title"><?= Yii::t('frontend.landing', 'invest_in_gold') ?></h3>
                <?= Html::a(Yii::t('frontend.landing', 'registration'), Url::to(['/user/registration/register']), ['class' => 'btn-gold']) ?>

            </div>
        </section>