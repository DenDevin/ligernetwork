<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use themes\ligertheme\frontend\assets\LigerThemeAsset;
use common\widgets\Alert;

LigerThemeAsset::register($this);

?>



<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <style> #my-menu:not( .mm-menu ) { display: none; } </style>
    <body>
    <?php $this->beginBody() ?>
    <div id="app"><!-- BEGIN: VUE_APP -->
    <div class="preloader Fixed">
        <div class="preloader__logo">
            <?= Html::img('/images/logo.png')?>
            <div class="preloader__loader">
                <span></span>
            </div>
        </div>
    </div>

        <?= $this->render(
            'header/header-login.php'
        ) ?>

        <?=$content?>

    <?= $this->render(
        'footer/footer.php'
    ) ?>

    </div> <!-- END: VUE_APP -->
    <?php $this->endBody() ?>

    </body>
    </html>
<?php $this->endPage() ?>