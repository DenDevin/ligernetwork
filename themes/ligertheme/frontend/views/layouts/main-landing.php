<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use themes\ligertheme\frontend\assets\LandingAsset;
use themes\ligertheme\frontend\assets\VueAsset;
use common\widgets\Alert;

LandingAsset::register($this);
\onmotion\vue\VueAsset::register($this);


?>



<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
    <title><?= Yii::t('frontend.landing', 'liger_title') ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="keywords" content="Liger Network" />
    <meta name="description" content="<?= Yii::t('frontend.landing', 'liger_descr') ?>" />

    <meta property="og:title" content="Liger Network – инвестиции в золото"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Европейская, международная площадка для крупных и начинающих инвесторов. Инвестируйте в золото - обеспечьте будущее в достатке себе и вашим близким."/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content="Liger Network"/>

    <meta name="author" content="Liger Network" />
    <meta name="robots" content="follow, index" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

   
<body>
     <?php $this->beginBody() ?>
     <div id="app">
       <div class="preloader">
         <div class="loader">
            <svg viewBox="0 0 324 352.7"  class="loader-octa">
                <path class="orange" class="white" d="M83.1,117.5L151,78.3c2.8-1.6,4.6-4.6,4.6-7.9V11.1c0-7-7.6-11.4-13.6-7.9L22.6,72.1c-6.1,3.5-6.1,12.2,0,15.7
                    l51.4,29.6C76.8,119.1,80.3,119.1,83.1,117.5z"/>
                <path class="orange" d="M71.6,215.5v-79c0-3.2-1.7-6.3-4.6-7.9L15.6,99C9.6,95.5,2,99.8,2,106.8v138.4c0,7,7.6,11.4,13.6,7.9L67,223.4
                    C69.9,221.7,71.6,218.8,71.6,215.5z"/>
                <path class="orange" d="M151,274.4l-68.6-39.6c-2.8-1.6-6.3-1.6-9.1,0L22,264.5c-6.1,3.5-6.1,12.2,0,15.7l119.9,69.2
                    c6.1,3.5,13.6-0.9,13.6-7.9v-59.4C155.5,279,153.8,276,151,274.4z"/>
                <path class="orange" d="M241.8,234.7l-68.7,39.7c-2.8,1.6-4.6,4.6-4.6,7.9v59.4c0,7,7.6,11.4,13.6,7.9l120.1-69.4
                    c6.1-3.5,6.1-12.2,0-15.7l-51.4-29.6C248.1,233.1,244.6,233.1,241.8,234.7z"/>
                <path class="orange" d="M257,223.2l51.4,29.6c6.1,3.5,13.6-0.9,13.6-7.9V107c0-7-7.6-11.4-13.6-7.9L257,128.7c-2.8,1.6-4.6,4.6-4.6,7.9
                    v78.7C252.5,218.6,254.2,221.6,257,223.2z"/>
                <path class="white" d="M177.7,2c-4.8,0-9.1,3.8-9.1,9.1v59.4c0,3.2,1.7,6.3,4.6,7.9l68,39.3c1.4,0.8,3,1.2,4.6,1.2
                    c1.5,0,3.2-0.4,4.6-1.2l51.4-29.6c6.1-3.5,6.1-12.2,0-15.7l-119.5-69C180.7,2.4,179.1,2,177.7,2L177.7,2z"/>
            </svg>
        </div>
    </div>

           <?= $content ?>

            <footer>
                <div class="wrapper">
                    <div class="copy">Liger Network © 2019</div>
                    <div class="f-social">
                        <a href="#">Facebook</a>
                        <a href="#">Instagram</a>
                        <a href="#">YouTube</a>
                    </div>
                    <div class="policy">
                        <a href="#"><?= Yii::t('frontend.landing', 'liger_rules') ?></a>
                        <a href="#"><?= Yii::t('frontend.landing', 'liger_policy') ?></a>
                    </div>
                </div>
            </footer>
    </div>
  </div>
     </div>
</body>
 <?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>