<?


use yii\helpers\Html;
use frontend\widgets\LanguageSelector\LanguageSelector;
use yii\helpers\Url;
use common\widgets\Alert;

?>



<div id="my-page">
    <div id="my-header">
        <div class="header-top">
            <div class="row aic header-top__row">
                <div class="col-md-1 col-1">
                    <a href="#my-menu" class="hamburger hamburger--collapse" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
                    </a>
                </div>

                <div class="col-md-2 col-sm-6 col-12 header-top__gray-column">
                    <div class="gray-partner">
                        <span class="mdi mdi-star"></span>
                        <p><span class="accent-color"><?= Yii::t('frontend.office', Yii::$app->useridentity->getUserPartnerStatus()) ?></span></p>

                    </div>
                </div>


                <div class="col-md-4 col-10 header-top__search-column">
                    <? if (Yii::$app->useridentity->isActivePartner()) : ?>
                    <div class="header-top__search">
                        <button class="clipboard-btn" data-clipboard-target="#copy-link">
                            <span class="mdi mdi-content-copy"></span>
                        </button>
                        <input id="copy-link" type="text" value="<?= Yii::$app->urlManager->createAbsoluteUrl(['id/'.Yii::$app->user->identity->user_token]); ?>">
                    </div>
                    <? endif; ?>
                </div>


                <div class="col-md-5 col-sm-6 col-12 header-top__profile-column d-flex">
        <!---------------- Language START ---------------->
        <?= LanguageSelector::widget([
         'layout' => 'header_top'
        ]) ?>
        <!---------------- Language END ---------------->


                
                    <div class="header-top__profile">
                        <div class="header-top__notification">
                            <a href="<?= Url::to(['/users/profile']) ?>"><span class="mdi mdi-bell"></span></a>
                            <? if (Yii::$app->notification->notifyCount() > 0) : ?>
                            <span class="length animated fadeIn infinite">
                                <?=Yii::$app->notification->notifyCount() ?>
                            </span>
                            <? endif; ?>
                        </div>

                        <div class="dropdown">
                            <a href="#" class="header-top__profile-item" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="avatar">
                                    <img src="/images/avatar.png" alt="">
                                </div>
                                <p><?=Yii::$app->user->identity->username?></p>
                                <div class="triangle">
                                    <span class="mdi mdi-menu-down"></span>
                                </div>
                            </a>
                            <div class="dropdown-menu profile-dropdown" aria-labelledby="dropdownMenuLink">

                                <?= Html::a(Yii::t('frontend.office', 'my_account'),
                                            Url::to(['/users/profile']),
                                    ['class' => 'dropdown-item']) ?>

                                <? if(Yii::$app->user->can('superadmin')) : ?>
                                <?= Html::a(Yii::t('frontend.office', 'adminpanel'),
                                            Url::to(['/admin']),
                                            ['class' => 'dropdown-item']) ?>
                                <? endif; ?>

                                <?= Html::a(Yii::t('frontend.office', 'logout'),
                                            Url::to(['/user/security/logout']),
                                    [
                                      'class' => 'dropdown-item',
                                      'data' => ['method' => 'post'],
                                    ]);

                                ?>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <nav id="my-menu">

                <ul>
                    <li><a href="<?= Url::to(['/users/office'])?>"><span class="mdi mdi-pulse"></span><?= Yii::t('frontend.office', 'main') ?></a></li>
                    <li><a href="<?= Url::to(['/users/wallet'])?>"><span class="mdi mdi-wallet"></span><?= Yii::t('frontend.office', 'wallet') ?></a></li>
                    <li><a href="<?= Url::to(['/users/market'])?>"><span class="mdi mdi-shopping"></span><?= Yii::t('frontend.office', 'marketplace') ?></a></li>
                    <? if ($purchases) : ?><li><a href="#"><span class="mdi mdi-webpack"></span><?= Yii::t('frontend.office', 'my_purchases') ?></a></li> <? endif; ?>
                    <li><a href="<?= Url::to(['/users/team'])?>"><span class="mdi mdi-share-variant"></span><?= Yii::t('frontend.office', 'my_team') ?></a></li>
                    <? if ($news) : ?><li><a href="#"><span class="mdi mdi-newspaper"></span><?= Yii::t('frontend.office', 'news') ?></a></li> <? endif; ?>
                    <li><a href="<?= Url::to(['/users/profile'])?>"><span class="mdi mdi-settings"></span><?= Yii::t('frontend.office', 'profile') ?></a></li>
                    <? if ($help) : ?> <li><a href="#"><span class="mdi mdi-help"></span><?= Yii::t('frontend.office', 'faq') ?></a></li> <? endif; ?>
                </ul>




        </nav>
    </div>

<?= Alert::widget() ?>