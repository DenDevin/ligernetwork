<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;







if (Yii::$app->controller->action->id === 'login'    ||
    Yii::$app->controller->action->id === 'register' ||
    Yii::$app->controller->id === 'error'            ||
    Yii::$app->controller->action->id === 'request'  ||
    Yii::$app->controller->action->id === 'resend'   ||
    Yii::$app->controller->action->id === 'reset'    ||
    Yii::$app->controller->action->id === 'confirm'



)
{
	
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
}

elseif (Yii::$app->controller->id === 'landing')
{
   echo $this->render(
        'main-landing',
        ['content' => $content]
    );

}

else
{
	
    echo $this->render(
        'main-user',
        ['content' => $content]
    );





}

?>











