function preLoader(){
	var loader = new TimelineMax();
		loader.fromTo('.loader-octa path', 2, {drawSVG:'0%', ease: Power2.easeOut},{drawSVG:'100%', ease: Power2.easeOut})
			  .to('.loader-octa .orange', .5, {fill:'#d5a744', ease: Power2.easeOut}, 'col')
			  .to('.loader-octa .white', .5, {fill:'#ffffff', ease: Power2.easeOut}, 'col+=0')
			  .to('.loader-octa', 2, {scale:'60', ease: Linear.easeNone}, 'asd')
			  .to('.preloader', 1, {autoAlpha:0, ease: Power2.easeOut}, 'asd+=0.3');
		TweenMax.fromTo(loader, 2, {timeScale:0}, {timeScale:1})
		// TweenMax.lagSmoothing(500, 33)
}
preLoader();
$(document).ready(function(){


$('.btn-gold').click(function(e){
	e.preventDefault();
})

	var slider = $('.slider-container'),
		sliderNavContainer = $('.navigation-arrows'),
		sliderDotsContainer = $('.navigation-dots'),
		prevBtnText = '<span href="#"><svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon"><path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path></svg><i class="mdi mdi-chevron-left"></i></span>',
		nextBtnText = '<span href="#"><svg viewBox="0 0 40 40" class="js-hover-it js-hit-icon"><path class="path circle" d="M20,2A18,18,0,1,1,2,20,18,18,0,0,1,20,2"></path></svg><i class="mdi mdi-chevron-right"></i></span>',
		circle = $('.next-btn .circle'),
		sliderDuration = 1000,
		interval = 14000,
		circleInterval = 14;
	 //    function progressTiming(){
		// 	$('#circle').attr('class','circle active');
	 //    }
		// $('#circle').attr('class','circle active');
		TweenMax.from('#circle', circleInterval, {drawSVG: 0});
	slider.owlCarousel({
		items:1,
		mouseDrag:false,
		touchDrag:false,
		navContainer: sliderNavContainer,
		dotsContainer: sliderDotsContainer,
		animateIn: 'fadeIn',
		// smartSpeed: 1000,
		nav:false,
		dots:true,
		loop:true,
		navText: [prevBtnText,nextBtnText],
		autoplay:false,
		onChange: callback
	})
	function autoplaySlider(){
		$('.next-btn').click()
	};
	var setTimer = setInterval(autoplaySlider, interval);
	function callback(event) {
		var slideText = $('.slide-text'),
			slideImage = $('.fs-img-slides');
	    function visibleContent(){
			slideImage.addClass('visible');
			slideText.removeClass('visible-out')
			function slideIn(){
				slideText.addClass('visible-in')
			}
	    	setTimeout(slideIn, 100)
	    }
	    setTimeout(visibleContent, 500)
	}
	$('.next-btn').click(function() {
	    function nextSlide(){
	    	slider.trigger('next.owl.carousel')
	    }
	    setTimeout(nextSlide, sliderDuration)
		var slideText = $('.slide-text'),
			slideImage = $('.fs-img-slides');
			slideText.addClass('visible-out').removeClass('visible-in');
			slideImage.removeClass('visible');
	    clearInterval(setTimer);
	    setTimer = setInterval(autoplaySlider, interval);
		// $('#circle').attr('class','circle');
		TweenMax.fromTo('#circle', circleInterval, {drawSVG: 0},{drawSVG: '100%'});
		// setTimeout(progressTiming,100)
	})
	$('.prev-btn').click(function() {
	    function prevSlide(){
		    slider.trigger('prev.owl.carousel');
	    }
	    setTimeout(prevSlide, sliderDuration)
		var slideText = $('.slide-text'),
			slideImage = $('.fs-img-slides');
		slideText.addClass('visible-out').removeClass('visible-in');
		slideImage.removeClass('visible');
	    clearInterval(setTimer);
	    setTimer = setInterval(autoplaySlider, interval);
		TweenMax.fromTo('#circle', circleInterval, {drawSVG: 0},{drawSVG: '100%'});
		// $('#circle').attr('class','circle');
		// setTimeout(progressTiming,100)
	})
	$('.navigation-dots .dot').click(function () {
		var _this = $(this) 
	    function nextSlide(){
	  		slider.trigger('to.owl.carousel', [_this.index(), 300]);
	    }
	    setTimeout(nextSlide, sliderDuration)
		var slideText = $('.slide-text'),
			slideImage = $('.fs-img-slides');
			slideText.addClass('visible-out').removeClass('visible-in');
			slideImage.removeClass('visible');
	    clearInterval(setTimer);
	    setTimer = setInterval(autoplaySlider, interval);
		TweenMax.fromTo('#circle', circleInterval, {drawSVG: 0},{drawSVG: '100%'});
		// $('#circle').attr('class','circle');
		// setTimeout(progressTiming,100)
	});

	$(".scroll").click(function(event){
        event.preventDefault();
        var full_url = this.href;
        var parts = full_url.split("#");
        var trgt = parts[1];
        var target_offset = $("#"+trgt).offset();
        var target_top = target_offset.top - 0;
        $('html, body').animate({scrollTop:target_top}, 1000);
 	});
	$("#offer .half-container").mousemove(function(e1) {
	  	parallaxIt2(e1, ".dollar", -10);
	  	parallaxIt2(e1, ".gold", -30);
	  	parallaxIt2(e1, ".rub", -15);
	  	parallaxIt2(e1, ".percent", 8);
	  	parallaxIt2(e1, ".bitcoin", -15);
	  	parallaxIt2(e1, ".lire", -5);
	  	parallaxIt2(e1, ".euro", 10);
	  	parallaxIt2(e1, ".ethereum", 10);
	});
	function parallaxIt2(e1, target1, movement1) {
		var $this = $("#offer .half-container");
		var relX = e1.pageX - $this.offset().left;
		var relY = e1.pageY - $this.offset().top;
		TweenMax.to(target1, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement1,
			y: (relY - $this.height() / 2) / $this.height() * movement1
		});
	}
	$("#for-partners .half-container").mousemove(function(e2) {
	  	parallaxIt3(e2, ".fp-sota-box", -15);
	});
	function parallaxIt3(e2, target2, movement2) {
		var $this = $("#for-partners .half-container");
		var relX = e2.pageX - $this.offset().left;
		var relY = e2.pageY - $this.offset().top;
		TweenMax.to(target2, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement2 - 31,
			y: (relY - $this.height() / 2) / $this.height() * movement2 -77
		});
	}
	$("#for-partners .half-container").mousemove(function(e2) {
	  	parallaxIt4(e2, ".fp-s1", 10);
	  	parallaxIt4(e2, ".fp-s2", -10);
	  	parallaxIt4(e2, ".fp-logo-icon", -30);
	  	parallaxIt4(e2, ".fb-sota-bottom", 8);
	  	parallaxIt4(e2, ".fp-s3", 25);
	  	parallaxIt4(e2, ".fp-s4", -5);
	  	parallaxIt4(e2, ".fp-s5", 10);
	});
	function parallaxIt4(e2, target2, movement2) {
		var $this = $("#for-partners .half-container");
		var relX = e2.pageX - $this.offset().left;
		var relY = e2.pageY - $this.offset().top;
		TweenMax.to(target2, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement2,
			y: (relY - $this.height() / 2) / $this.height() * movement2
		});
	}
})
$(window).load(function(){
	var wScrTop =  $(window).scrollTop();
	var winHeight = $(window).height();
	var sedebarAnim = new TimelineMax(),
		sideNav = $('#fixed-navigation'),
		sideNavItems = $('#fixed-navigation li'),
		logo = $('#fixed-navigation .logo'),
		logoText = $('#fixed-navigation .logo-text'),
		btnDown = $('#fixed-navigation .btn-down'),
		btnUp = $('#fixed-navigation .btn-upp');
	var controller = new ScrollMagic.Controller();
    var screenHeight = $(window).height();
    $('.paralax_wrapp').each(function(){
        var parallaxBg = $(this).find('.paralax_bg')
        var prallax = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 1,
            duration: screenHeight*3,
        })
        .setTween(TweenMax.to(parallaxBg, 1, {y:screenHeight,ease: Power0.easeNone}))
        .addTo(controller)
    });
	function addItem() {
	    var _item = document.createElement('div');
	    $(_item).addClass('nav-bg');
	    $('#fixed-navigation').append(_item);
	}
	var sidebarAnim = new TimelineMax({onComplite:addItem()});
		TweenMax.set('.nav-bg', {y:'+100%',display:'block',position:'absolute',top:'0',left:'0',width: '100%',height: '100%',background:'var(--dark)',zIndex:'-1'})
		sidebarAnim
					.to(logo, .1, {x:'-100%', opacity: 0, ease: Power2.easeOut})
					.to(logoText, .1, {opacity:0, ease: Power2.easeOut},0)
					.staggerTo(sideNavItems,.3,{x:'-=100%', ease: Power2.easeOut}, 0.05)
					.to(btnDown, .3, {y:'100%', ease: Power2.easeOut})
					.to('.nav-bg', .3, {y:'+0%', ease: Power2.easeOut})
					.to(logo, .1, {x:'+0%', opacity: 1, ease: Power2.easeOut})
					.staggerFromTo(sideNavItems,.3,{x:'+=0%', ease: Power2.easeOut},{x:'+=100%', ease: Power2.easeOut}, 0.05)
					.to(btnUp, .3, {bottom: '0%',opacity:1, ease: Power2.easeOut})
	var sidebarScene = new ScrollMagic.Scene({
	    triggerElement: '#about',
	    triggerHook: .8,
	})
	.setTween(sidebarAnim)
	.addTo(controller)
	var aboutWordBg1 = $('#about .word-bg')
	var aboutWordBg2 = $('#offer .word-bg')
	var aboutWordBg3 = $('#safe .word-bg')
	var aboutWordBg4 = $('#facts .word-bg')
	var aboutWordBg5 = $('#for-partners .word-bg')
	var aboutWordBg6 = $('#roaad-map .word-bg')
	var aboutWordBg7 = $('#mobile-navigation .word-bg')
	var split1 = new SplitText(aboutWordBg1, {type:'chars, words'});
	var split2 = new SplitText(aboutWordBg2, {type:'chars, words'});
	var split3 = new SplitText(aboutWordBg3, {type:'chars, words'});
	var split4 = new SplitText(aboutWordBg4, {type:'chars, words'});
	var split5 = new SplitText(aboutWordBg5, {type:'chars, words', wordsClass:"word++"});
	var split6 = new SplitText(aboutWordBg6, {type:'chars, words'});
	var split7 = new SplitText(aboutWordBg7, {type:'chars, words'});
	var aboutAnim = new TimelineMax();
		aboutAnim
				.from('.about-mask', .4, {opacity: 0, ease: Power2.easeOut})
				.staggerFrom('.clipAnim', .4, {scale:'0', opacity: 0, ease: Power2.easeOut}, .1)
				.staggerFrom(split1.chars, .2, {x:'100%', opacity: 0}, 0.1, 0.5)
				.from('#about .box-title', .5, {y:'+100', opacity: 0, ease: Power2.easeOut}, 0.8)
				.staggerFrom('#about .box-desc p', .5, {y:'+100', opacity: 0, ease: Power2.easeOut},0.2, 1)
	new ScrollMagic.Scene({
	    triggerElement: '#about',
	    triggerHook: .8,
	    offset: '100',
		// reverse:false
	})
	.setTween(aboutAnim)
	.addTo(controller)

	$("#about").mousemove(function(e1) {
	  	parallaxIt5(e1, ".car-polygon", 10);
	  	parallaxIt5(e1, ".small-white-octa", 5);
	  	parallaxIt5(e1, ".big-octa", -10);
	  	parallaxIt5(e1, ".big-white-octa", 8);
	  	parallaxIt5(e1, ".man-polygon", 15);
	});
	function parallaxIt5(e1, target1, movement1) {
		var $this = $("#about");
		var relX = e1.pageX - $this.offset().left;
		var relY = e1.pageY - $this.offset().top;
		TweenMax.to(target1, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement1,
			y: (relY - $this.height() / 2) / $this.height() * movement1
		});
	}

	if ($(window).width() > 1023) {
		var offerAnim = (new TimelineMax)
					.add(TweenMax.from('#offer .flipInner', .4, {scale:'0', opacity: 0, ease: Power2.easeOut}))
					.add(TweenMax.staggerFrom('#offer .iconContainer', .4, {top:'0px', left:'0px', scale:'0', opacity: 0, ease: Power2.easeOut}, 0.1))
					.add(TweenMax.staggerFrom(split2.chars, .2, {x:'100px', opacity: 0}, 0.1), 0.5)
					.add(TweenMax.from('#offer .box-title', .5, {y:'+100', opacity: 0, ease: Power2.easeOut}), 0.8)
					.add(TweenMax.staggerFrom('#offer .box-desc p', .5, {y:'+100', opacity: 0, ease: Power2.easeOut},0.2), 1)
		new ScrollMagic.Scene({
		    triggerElement: '#offer',
		    triggerHook: .6,
		    offset: '100',
		})
		.setTween(offerAnim)
		.addTo(controller);
	}else{
		var offerAnim = (new TimelineMax)
					.add(TweenMax.from('#offer .flipInner', .4, {scale:'0', opacity: 0, ease: Power2.easeOut}))
					.add(TweenMax.staggerFrom(split2.chars, .2, {x:'100px', opacity: 0}, 0.1), 0.5)
					.add(TweenMax.from('#offer .box-title', .5, {y:'+100', opacity: 0, ease: Power2.easeOut}), 0.8)
					.add(TweenMax.staggerFrom('#offer .box-desc p', .5, {y:'+100', opacity: 0, ease: Power2.easeOut},0.2), 1)
		new ScrollMagic.Scene({
		    triggerElement: '#offer',
		    triggerHook: .6,
		    offset: '100',
		})
		.setTween(offerAnim)
		.addTo(controller);

		var offerAnimcoins = (new TimelineMax)
					.add(TweenMax.staggerFrom('#offer .iconContainer', .4, {top:'0px', left:'0px', scale:'0', opacity: 0, ease: Power2.easeOut}, 0.1))
		new ScrollMagic.Scene({
		    triggerElement: '#offer .half-box.text-center',
		    triggerHook: .6,
		    offset: '100',
		})
		.setTween(offerAnimcoins)
		.addTo(controller)
	}




	var sotaBg = $('.si-bg'),
		sotaContainer = $('.sota-container'),
		sotaBgLangth = sotaBg.langth,
		offsetContainer = sotaContainer.offset().top,
		containerWidth = sotaContainer.innerWidth() + 100,
		containerHeight = sotaContainer.innerHeight() + 300;
	TweenMax.set(sotaBg,{
		backgroundSize:''+containerWidth+'px '+containerHeight+'px'
	})
	sotaBg.each(function(){
		var posX = Math.abs($(this).offset().left + 90),
			posY = Math.abs($(this).offset().top),
			offsetTopBg = Math.abs(posY - offsetContainer + 90);

		// console.log(posX, posY, offsetTopBg)
		TweenMax.set($(this),{backgroundPosition:'-'+posX+'px -'+offsetTopBg+'px', 
		})
	})
	var safeAnim = new TimelineMax();
		safeAnim
				.staggerFrom('.sota-wrapper', .4, {scale: '0',x:'-=100', y: 100, autoAlfa: 0, ease: Power2.easeOut},0.1)
				.from('#safe .box-title', .4, {y:100, opacity: 0},0)
				.staggerFrom('.sota-wrapper .sota-safe-step', .4, {scale:'0', autoAlfa: 0, ease: Power2.easeOut}, .2, 0)
	new ScrollMagic.Scene({
	    triggerElement: '#safe',
	    triggerHook: .5,
	    offset: '0',
		// reverse:false
	})
	.setTween(safeAnim)
	.addTo(controller)
	$("#safe").mousemove(function(e6) {
	  	parallaxIt6(e6, ".sota-bg", 20);
	});
	function parallaxIt6(e6, target6, movement6) {
		var $this = $("#safe");
		var relX = e6.pageX - $this.offset().left;
		var relY = e6.pageY - $this.offset().top;
		TweenMax.to(target6, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement6,
			y: (relY - $this.height() / 2) / $this.height() * movement6
		});
	}
	var sotaStepAnim = new TimelineMax();
		sotaStepAnim
				.staggerFrom(split3.chars, .2, {x:'100px', opacity: 0, ease: Power2.easeOut}, 0.1, 0.5)
	new ScrollMagic.Scene({
	    triggerElement: '#safe',
	    triggerHook: .8,
	    offset: '0',
		// reverse:false
	})
	.setTween(sotaStepAnim)
	.addTo(controller)
	$('.action-box').each(function(){
		var actionAnim = new TimelineMax(),
			actionTitle = $('.action-title', this)
			actionSubtitle = $('.action-subtitle', this)
			actionBtn = $('.btn-gold', this);
			actionAnim
					  .from(actionTitle, 1, {y:100, opacity:0 ,  ease: Power2.easeOut})
					  .from(actionSubtitle, 1, {y:100, opacity:0 ,  ease: Power2.easeOut})
					  .from(actionBtn, 1, {y:100, opacity:0 ,  ease: Power2.easeOut})
        new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.8,
            duration: '400px',
			// reverse:false
        })
        .setTween(actionAnim)
        .addTo(controller)
    });
	var factsAnim = new TimelineMax();
		factsAnim
				 .staggerFrom('#facts .prime-box', .3, {y:'100', opacity:'0', ease: Power2.easeOut}, 0.2)
				 .staggerFrom(split4.chars, .2, {x:'100px', opacity: 0}, 0.1, 0.5)
				 .from('#facts .box-title', .3, {x:'-100', opacity:'0', ease: Power2.easeOut}, 0.2)
	new ScrollMagic.Scene({
	    triggerElement: '#facts',
	    triggerHook: .5,
	    offset: '0',
		// reverse:false
	})
	.setTween(factsAnim)
	.addTo(controller)

	$("#facts").mousemove(function(e1) {
	  	parallaxIt7(e1, ".img-par", -35);
	});
	function parallaxIt7(e1, target1, movement1) {
		var $this = $("#facts");
		var relX = e1.pageX - $this.offset().left;
		var relY = e1.pageY - $this.offset().top;
		TweenMax.to(target1, 2, {
			x: (relX - $this.width() / 2) / $this.width() * movement1,
			y: (relY - $this.height() / 2) / $this.height() * movement1
		});
	}
	var forPartneersAnim = new TimelineMax();
		forPartneersAnim
						.staggerFrom('#for-partners .fp-anim', .3, {scale:'0', x:100, opacity:'0', ease: Power2.easeOut}, 0.2)
						.staggerFrom(split5.chars, .2, {x:'100px', opacity: 0}, 0.1, 0.5)
						.from('#for-partners .box-title', .4, {y:100, opacity: 0, ease: Power2.easeOut}, .5)
						.from('#for-partners .subtitle', .4, {y:100, opacity: 0, ease: Power2.easeOut}, .7)
						.staggerFrom('#for-partners .fp-primes li', .4, {y:100, opacity: 0, ease: Power2.easeOut}, .2, .9)
	new ScrollMagic.Scene({
	    triggerElement: '#for-partners',
	    triggerHook: .7,
	    offset: '0',
	})
	.setTween(forPartneersAnim)
	.addTo(controller)
	if ($(window).width() > 1023) {
		var roadAnim = (new TimelineMax)
		.add([TweenMax.from('#roaad-map .box-title', .4, {y: 100, opacity: '0', ease: Power2.easeOut}),
			TweenMax.fromTo('.road-gold', 2.7, {drawSVG: '0%'},{drawSVG: '100%', ease: Power2.easeOut}),
			TweenMax.staggerFrom('.r-step p', .3, {x:'150', opacity: 0, ease: Power2.easeOut}, .1),
			TweenMax.staggerFrom('.before.gold-color', .5, {scale:'2', opacity: 0, ease: Power2.easeOut}, .4)])
		.add(TweenMax.staggerFrom(split6.chars, .2, {x:'100px', opacity: 0, ease: Power2.easeOut}, 0.1), 0.5)
		.add(TweenMax.from('.r-step .after', .3, {scale:'0', opacity: 0, ease: Power2.easeOut}), 1.7);
		new ScrollMagic.Scene({
		    triggerElement: '#roaad-map',
		    triggerHook: .8,
		    offset: '350',
		})
		.setTween(roadAnim)
		.addTo(controller)
	}
	if($(window).width() < 1024){
		$('#hamburger').on('click', function(){
			var mobNavAnim = (new TimelineMax),
				mobList = $('.mobile-nav-list'),
				mobListItem = $('.mobile-nav-list li');
			if($(this).hasClass('active')){
				TweenMax.set($(this), {className: '-=active'})
				mobNavAnim.to(mobList, .5, {autoAlpha:0, ease: Power2.easeOut})
			}else{
				TweenMax.set($(this), {className: '+=active'})
				mobNavAnim.add(TweenMax.to(mobList, .3, {autoAlpha:1, ease: Power2.easeOut}))
				.add(TweenMax.staggerFrom(split7.chars, .2, {x:'100%', autoAlpha: 0, ease: Power2.easeOut}, 0.1), 0.3)
				.add(TweenMax.staggerFrom(mobListItem, .2, {y:'100%', autoAlpha: 0, ease: Power2.easeOut}, 0.1), 0.3)
			}
		})
		$('.mobile-nav-list li a').on('click', function(){
			var mobList = $('.mobile-nav-list');
			TweenMax.set('#hamburger', {className: '-=active'})
			TweenMax.to(mobList, .5, {autoAlpha:0, ease: Power2.easeOut})
		})
	}
});