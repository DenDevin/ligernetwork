<?php

namespace themes\ligertheme\frontend\widgets\GoJs;

use Yii;
use yii\base\Widget;
use themes\ligertheme\frontend\widgets\GoJs\models\GoJsModel;
use common\models\User\UserTree;
use common\models\Package\UserPackageStat;
use common\models\User;
use yii\helpers\Json;

class GoJs extends Widget

{
    public $gojs;
    public $user;
    public $usertree;
    public $template = 'users_team';
    public $nodeDataArray;

    public function __consrtuct(GoJsModel $gojs,
                                User $user)
    {
        $this->gojs = $gojs;
        $this->user = $user;

    }

    public function run()
    {
        $user = Yii::$app->user->identity;
        $user_node = UserTree::findOne(['name' => $user->user_token]);
        $childs = $user_node->getDescendants(8, true)->all();
        $nodes = '[';
        foreach($childs as $child)
        {
           if($child->depth == 0)
           {
               $usr_data = User::findOne(['user_token' => $child->name]);
               $nodes .= "{key:".$child->id.", name:\"".$usr_data->username."\"},";
           }

            elseif($child->depth > 0)
            {
                $usr_data = User::findOne(['user_token' => $child->name]);
                $nod = UserTree::findOne(['name' => $child->name]);
                $parent = $nod->parent;
                $nodes .= "{key:".$child->id.", name:\"".$usr_data->username."\", parent:\"".$parent->id."\"},";
            /*


                if(
                UserPackageStat::find()->
                where(['user_id' => $usr_data->id])->
                andWhere(['package_id' => UserPackageStat::PACKAGE_BUSINESS])->
                andWhere(['>', 'pay_count', 1])->
                one()
                )
                {
                    $nod = UserTree::findOne(['name' => $child->name]);
                    $parent = $nod->parent;
                    $nodes .= "{key:".$child->id.", name:\"".$usr_data->username."\", parent:\"".$parent->id."\"},";
                }

                elseif(Yii::$app->useridentity->findActivePartner($child->name))
                {
                    $nod = UserTree::findOne(['name' => $child->name]);
                    $parent = $nod->parent;
                    $nodes .= "{key:".$child->id.", name:\"".$usr_data->username."\", parent:\"".$parent->id."\"},";

                }
*/



            }

        }

        $nodes .= ']';

//echo '<pre>';
//print_r($nodes);
//echo '</pre>';
//die;

        $nodeDataArray = $nodes;

        return $this->render($this->template, compact('gojs', 'user', 'nodeDataArray'));
    }





}