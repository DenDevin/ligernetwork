<?php

namespace themes\ligertheme\frontend\widgets\UserTree;

use Yii;
use yii\base\Widget;
use common\models\User\UserTree as ModelTree;

class UserTree extends Widget

{

    public function init()
    {

    }

    public function run()
    {
        $user = Yii::$app->user->identity;
        $model = new ModelTree();
      //  $childs = ModelTree::getTree($user);
        return $this->render('index', compact('model', 'user'));
    }





}