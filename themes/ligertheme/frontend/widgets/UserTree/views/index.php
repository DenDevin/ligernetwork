<div class="team-tree-tab__content" data-tab-content="team-tree">
    <div class="team-tree__zoom" id="team-tree__zoom">
        <!-- 1 START -->
        <ul class="tree__wrap">
            <li>
                <a href="#" class="tree__item tree__item-active straight-line">
                    <div class="client">
                        <p class="name">Julie Cortez</p>
                    </div>
                    <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                </a>

                <!-- Sub START -->
                <ul class="tree__item-sub has-line-3">
                    <!-- List START -->
                    <li>
                        <a href="#" class="tree__item tree__item-active accent-first straight-line">
                            <div class="first-triangle">
                                <img src="/images/first-triangle.png" alt="">
                            </div>
                            <div class="client">
                                <p class="name">Billie Gardner</p>
                                <p class="cat"><?=Yii::t('frontend.team', 'bronze_partner')?></p>
                            </div>
                            <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                        </a>

                        <ul class="tree__item-sub has-line-4">
                            <li>
                                <a href="#" class="tree__item border-accent">
                                    <div class="first-triangle">
                                        <img src="/images/first-triangle-gold.png" alt="">
                                    </div>
                                    <div class="client">
                                        <p class="name">Frederick Allison</p>
                                        <p class="cat"><?=Yii::t('frontend.team', 'bronze_partner')?></p>
                                    </div>
                                    <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tree__item border-accent">
                                    <div class="client">
                                        <p class="name">Greg Sharp</p>
                                        <p class="cat"><?=Yii::t('frontend.team', 'platinum_partner')?></p>
                                    </div>
                                    <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tree__item border-gray">
                                    <div class="client">
                                        <p class="name">Jake Schneider</p>
                                        <p class="cat"><?=Yii::t('frontend.team', 'client')?></p>
                                    </div>
                                    <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tree__item tree__item-inactive border-gray">
                                    <div class="client">
                                        <p class="name">Janet Hicks</p>
                                        <p class="cat"><?=Yii::t('frontend.team', 'waiting_activate')?></p>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tree__item tree__item-inactive border-gray">
                                    <div class="client">
                                        <p class="name">Janet Hicks</p>
                                        <p class="cat"><?=Yii::t('frontend.team', 'waiting_activate')?></p>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <!-- List END -->

                    <!-- List START -->
                    <li>
                        <a href="#" class="tree__item border-accent">
                            <div class="first-triangle">
                                <img src="/images/first-triangle-gold.png" alt="">
                            </div>
                            <div class="client">
                                <p class="name">Cindy West</p>
                                <p class="cat"><?=Yii::t('frontend.team', 'platinum_partner')?></p>
                            </div>
                            <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                        </a>
                    </li>
                    <!-- List END -->

                    <!-- List START -->
                    <li>
                        <a href="#" class="tree__item border-gray">
                            <div class="first-triangle">
                                <img src="/images/first-triangle-gray.png" alt="">
                            </div>
                            <div class="client">
                                <p class="name">Erma Alexander</p>
                                <p class="cat"><?=Yii::t('frontend.team', 'client')?></p>
                            </div>
                            <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="tree__item border-accent">
                            <div class="first-triangle">
                                <img src="/images/first-triangle-gold.png" alt="">
                            </div>
                            <div class="client">
                                <p class="name">Cindy West</p>
                                <p class="cat"><?=Yii::t('frontend.team', 'platinum_partner')?></p>
                            </div>
                            <button data-mfp-src="#partners-popup" class="mdi mdi-plus-circle"></button>
                        </a>
                    </li>


                    <!-- List END -->
                </ul>
                <!-- Sub END -->
            </li>
        </ul>
        <!-- 1 END -->

        <div class="zoom__wrap">
            <button class="mdi mdi-plus zoom__item"></button>
            <button class="mdi mdi-minus zoom__item"></button>
        </div>

    </div>
</div>