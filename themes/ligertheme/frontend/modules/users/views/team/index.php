<?php

use themes\ligertheme\frontend\widgets\UserTree\UserTree;
use themes\ligertheme\frontend\widgets\GoJs\GoJs;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;


?>


<div id="my-content" class="p-purchases my-content">

    <div class="container-fluid">
        <!----------------- Shop START ----------------->
        <section class="my-team-structure" id="my-team-structure"> <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="my-team-left">
                        <div class="accent-title">
                            <h2><?=Yii::t('frontend.team', 'total_profit_title')?></h2>
                            <span class="line"></span>
                        </div>

                        <div class="price-big">
                            350 <span class="currency">€</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12">
                    <div class="your-structure">
                        <div class="accent-title">
                            <h2><?=Yii::t('frontend.team', 'your_structure_title')?></h2>
                            <span class="line"></span>
                        </div>

                        <div class="structure-card__wrap">
                            <div class="structure-card__item border-left-orange">
                                <p class="color-gray"><?=Yii::t('frontend.team', 'entire_period')?></p>
                                <div class="bottom">
                                    <div class="left">
                                        <span class="number">
                                            <?= Yii::$app->useridentity->getAllPartnersCount()?>
                                        </span>
                                        <span class="text"><?=Yii::t('frontend.team', 'partners')?></span>
                                    </div>
                                    <div class="right">
                                        <span class="number">
                                          <?= Yii::$app->useridentity->getAllClientsCount()?>
                                        </span>
                                        <span class="text"><?=Yii::t('frontend.team', 'clients')?></span>
                                    </div>
                                </div>
                            </div>
<!--
                            <div class="structure-card__item border-left-orange">
                                <p class="color-gray"><?=Yii::t('frontend.team', 'invitations')?></p>
                                <div class="bottom">
                                    <div class="left">
                                        <span class="number">0</span>
                                        <span class="text"><?=Yii::t('frontend.team', 'partners')?></span>
                                    </div>
                                    <div class="right">
                                        <span class="number">0</span>
                                        <span class="text"><?=Yii::t('frontend.team', 'clients')?></span>
                                    </div>
                                </div>
                            </div>
                            -->


                        </div>
                    </div>
                </div>








                <div class="col-md-12 team-tree__column">
                    <!-- New parner popup START -->
                    <div class="new-partner-popup__wrap mfp-hide" id="partners-popup">
                        <div class="new-partner-popup">
                            <form>
                                <h3><?=Yii::t('frontend.team', 'add_new_partner')?></h3>

                                <div class="partner-input login-input">
                                    <p><?=Yii::t('frontend.team', 'partner_login')?></p>
                                    <input type="text">
                                </div>
                                <div class="partner-input password-input">
                                    <p><?=Yii::t('frontend.team', 'partner_password')?></p>
                                    <input type="text">
                                </div>
                                <button><?=Yii::t('frontend.team', 'add_partner')?></button>
                                <a href="#" class="cancel"><?=Yii::t('frontend.team', 'cancel')?></a>
                            </form>
                        </div>
                    </div>

                    <div class="team-tree__wrap">
                        <ul class="custom__tab">
                            <li class="active" data-tab="team-tree"><a href="#"><?=Yii::t('frontend.team', 'structure_tree')?></a></li>
                            <li data-tab="team-table"><a href="#"><?=Yii::t('frontend.team', 'first_line')?></a></li>
                        </ul>

                        <?= GoJs::widget([]) ?>

                        <div class="team-tree-tab__table" data-tab-content="team-table">
                            <?php Pjax::begin(); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => false,
                                'layout' => '{items}',
                                'tableOptions' => [
                                    'class' => 'table table-custom'
                                ],
                                'headerRowOptions' => [
                                    'class' => 'title table-custom__title'
                                ],
                                'afterRow' => function()
                                {
                                    return '<tr class="gutter"></tr>';
                                },
                                'rowOptions' => ['class' => 'table__line table-custom__line'],
                                'columns' => [

                                     [
                                             'attribute' =>  'name',
                                             'value' => function($searchModel)
                                             {
                                                 return $searchModel->getUserName($searchModel->name);
                                             }

                                     ],

                                    [
                                        'attribute' => 'country',
                                        'value' => function($searchModel)
                                        {
                                            return $searchModel->getUserCountry($searchModel->name);
                                        }

                                    ],

                                    [
                                        'attribute' => 'status',
                                        'value' => function($searchModel)
                                        {
                                            return $searchModel->getUserStatus($searchModel->name);
                                        }

                                    ],

                                    [
                                        'attribute' => 'telegram',
                                        'format' => 'html',
                                        'value' => function($searchModel)

                                        {
                                            if(!empty($searchModel->getUserTelegram($searchModel->name)))
                                            {
                                                return Html::tag('span', $searchModel->getUserTelegram($searchModel->name), ['class' => 'accent-color']);
                                            }
                                            else
                                                {
                                                  return "";
                                                }

                                        }

                                    ],

                                    [
                                        'attribute' => 'structure',
                                        'value' => function($searchModel)
                                        {
                                            return $searchModel->getUserStructure($searchModel->name);
                                        }

                                    ]
                                ],
                            ]); ?>

                            <div class="pagination">
                                <?= LinkPager::widget([
                                    'pagination' => $dataProvider->pagination,
                                    'linkContainerOptions' => [
                                        'class' => ''
                                    ],
                                    'prevPageLabel' => false,
                                    'nextPageLabel' => false,

                                ]);?>
                            </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>