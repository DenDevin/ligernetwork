<?
  use yii\helpers\Url;
?>



<div id="my-content" class="p-dashboard my-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-sm-12 dashboard__left">
                <div class="dashboard-content">
                    <!---------- All information START ---------->
                    <section class="d-all-information" id="d-all-information">
                        <div class="accent-title">
                            <h2><?= Yii::t('frontend.office', 'general_information', ['qwert' => 12345]) ?></h2>
                            <span class="line"></span>
                        </div>

                        <div class="card__wrap">


                            <div class="card__item">
                                <p class="card__title"><?= Yii::t('frontend.office', 'your_income') ?></p>
                                <p class="card__number accent-color">
                                    <?=Yii::$app->formatter->asCurrency($balance_data['income']);?> <span class="usd">€</span>
                                </p>

                            </div>

                            <div class="card__item">
                                <p class="card__title"><?= Yii::t('frontend.office', 'wallet_balance') ?></p>
                                <p class="card__number accent-color">
                                    <?=Yii::$app->formatter->asCurrency($balance_data['euro']);?> <span class="usd">€</span>
                                </p>
                            </div>

                            <div class="card__item">
                                <p class="card__title"><?= Yii::t('frontend.office', 'lp_balance') ?></p>
                                <p class="card__number accent-color">
                                    <?= $balance->getUserBonuses() ?> <span class="usd">DM</span>
                                </p>
                            </div>

                            <div class="card__item">
                                <p class="card__title"><?= Yii::t('frontend.office', 'withdrawn_funds') ?></p>
                                <p class="card__number accent-color"><?= $balance->getWithdrawnFunds() ?><span class="usd">€</span></p>
                            </div>
                        </div>
                    </section>
                    <!---------- All information END ---------->

                    <!---------- Status START ---------->
                    <section class="d-status" id="d-status">
                        <div class="accent-title">
                            <h2><?= Yii::t('frontend.office', 'your_status') ?></h2>
                            <span class="line"></span>
                        </div>
                        <? if(!$verify->isVerified($user->id)) : ?>
                        <div class="verification">
                            <div class="left">
                                <p><?= Yii::t('frontend.office', 'not_verified') ?></p>
                            </div>

                            <div class="right">
                                <a href="<?= Url::to(['/users/profile', '#' => 'verification__wrap']) ?>"><?= Yii::t('frontend.office', 'pass_verification') ?></a>
                            </div>
                        </div>
                        <? endif; ?>




                        <div class="row card__wrap">

                                <?  $i = 0; ?>
                            <?php foreach ($marketing as $key => $status) : $i++  ?>


                            <div class="card__item
                              <?  if(Yii::$app->useridentity->getUserPartnerStatus() == $status->title) : ?>
                                card__active
                               <?  endif; ?>">
                                    <div class="image">
                                        <img src="/images/verification_<?=$i?>.png" alt="">
                                    </div>
                                    <div class="center">
                                        <div class="text__wrap">
                                            <p><?= Yii::t('frontend.office', 'bonus') ?></p>
                                            <p class="green-text"><?=$status->leader_bonus?> €</p>
                                        </div>

                                        <div class="text__wrap">
                                            <p><?= Yii::t('frontend.office', 'trade_turnover') ?></p>
                                            <p><?=$status->product_return?> €</p>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <span class="green-text"><?= Yii::t('frontend.office', Yii::t('frontend.office', $status->title)) ?></span>
                                    </div>
                                </div>

                            <? endforeach; ?>

                        </div>
                    </section>
                    <!---------- Status END ---------->

                    <!---------- Status range START ---------->
                    <?  if(Yii::$app->useridentity->getUserPartnerStatus() != 'client') : ?>
                    <section class="d-range">
                        <h2 class="min-title"><?= Yii::t('frontend.office', 'for_status_you_lack') ?>
                            <?= $statuses['next']['product_return'] - $statuses['product_return'] ?> €
                            <?= Yii::t('frontend.office', 'till_status') ?>
                            <?= Yii::t('frontend.office', $statuses['next']['title']) ?>
                        </h2>
                        <div class="range__wrap">
                            <div class="range__left">
                                <p><?= Yii::t('frontend.office', $statuses['current']) ?></p>
                                <p><?= $statuses['prev_product_return'] ?> €</p>
                            </div>

                            <div class="range__center">
                                <p><span class="green-text"><?= $statuses['product_return'] ?> €</span></p>
                                <p><?= Yii::t('frontend.office', 'current_product_return') ?></p>
                            </div>

                            <div class="range__right">
                                <p><?= Yii::t('frontend.office', $statuses['next']['title']) ?></p>
                                <p><span class="green-text"><?= $statuses['next']['product_return'] ?> €</span></p>
                            </div>

                            <div class="line">
                                <div class="line-left">
                                    <div class="circle-left"></div>
                                    <div class="circle-center"></div>
                                </div>

                                <div class="line-right">
                                    <div class="circle-right"></div>
                                </div>
                            </div>

                        </div>
                    </section>
                    <?  endif; ?>
                    <!---------- Status range END ---------->

                    <!---------- Old purchases START ---------->
                    <section class="d-old-purchases">
                        <div class="accent-title">
                            <h2><?= Yii::t('frontend.office', 'recent_purchases') ?></h2>
                            <span class="line"></span>
                        </div>

                        <table class="table">
                            <thead>
                            <tr class="title">
                                <td><?= Yii::t('frontend.office', 'date') ?></td>
                                <td><?= Yii::t('frontend.office', 'description') ?></td>
                                <td><?= Yii::t('frontend.office', 'price') ?></td>
                                <td><?= Yii::t('frontend.office', 'status_action') ?></td>
                            </tr>
                            </thead>

                            <tbody>
                            <!-- 1 -->
                            <tr class="table__line">
                                <td class="date"><?= Yii::t('frontend.office', 'date_number') ?></td>
                                <td class="desc"><?= Yii::t('frontend.office', '12_ticket') ?></td>
                                <td class="price">€ 75,00</td>
                                <td class="status">
                                    <a @click="viewTicket" href="#"><?= Yii::t('frontend.office', 'view_ticket') ?></a>
                                </td>
                                <td class="arrow"><span class="mdi mdi-chevron-right"></span></td>
                            </tr>
                            <!-- Разделитель -->
                            <tr class="gutter"></tr>

                            </tbody>
                        </table>
                    </section>
                    <!---------- Old purchases END ---------->

                    <!---------- More button START ---------->
                    <a href="#" class="more-button">
                        <span class="mdi mdi-dots-horizontal"></span>
                    </a>
                    <!---------- More button END ---------->
                </div>
            </div>

            <div class="col-lg-4 col-md-12 dashboard__right">
                <!-- Diagram 1 START -->
                <div class="dashboard-diagram">
                    <div class="accent-title">
                        <h2><?= Yii::t('frontend.office', 'gold_rate') ?></h2>
                        <span class="line"></span>
                    </div>
                    <canvas id="myChart" width="400" height="250"></canvas>
                </div>
                <!-- Diagram 1 END -->

                <!-- Diagram 2 START -->
                <div class="dashboard-diagram">
                    <div class="accent-title">
                        <h2><?= Yii::t('frontend.office', 'liger_rate') ?></h2>
                        <span class="line"></span>
                    </div>
                    <div id="chartdiv"></div>

                </div>
                <!-- Diagram 2 END -->

                <!-- Diagram 3 START -->
                <div class="dashboard-diagram">
                    <div class="accent-title">
                        <h2><?= Yii::t('frontend.office', 'user_dynamics') ?></h2>
                        <span class="line"></span>
                    </div>
                    <canvas id="myChart3" width="400" height="250"></canvas>
                </div>
                <!-- Diagram 3 END -->
            </div>

        </div>
    </div>














<?

$this->registerJsFile(Yii::getAlias('@web/js/vue/user.office.js'), [
    'depends' => [
        \onmotion\vue\VueAsset::className(),
        \onmotion\vue\AxiosAsset::className(),
        \onmotion\vue\VueResourceAsset::className(),
        \themes\ligertheme\frontend\assets\LigerThemeAsset::className(),
        \themes\ligertheme\frontend\assets\MapAsset::className()
    ]
]);

?>