<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


?>


<div id="my-content" class="p-shop my-content">
    <div class="container-fluid">
        <!-- 2 START -->
        <div class="wallet-popup__wrap wallet-popup__withdraw custom-popup mfp-hide" id="wallet-withdraw-income">
            <form>
                <div class="wallet-popup__item">
                    <h3><?= Yii::t('frontend.market', 'replenish_money') ?></h3>
                    <div v-if="paymentRecieved" class="timer my-3"><?= Yii::t('frontend.wallet', 'time_along') ?> {{ time }}</div>
                    <div v-if="paymentExceeded" class="timer my-3 text-danger">{{ paymentExceededText }}</div>
                    <div class="available">
                        <h4><?= Yii::t('frontend.market', 'avaliable_balance') ?></h4>
                        <div class="amount">
                            <?=Yii::$app->formatter->asCurrency($balance_data['euro'])?> EUR
                        </div>
                    </div>

                    <div class="wallet-popup__inner">
                        <h4><?= Yii::t('frontend.market', 'amount_replenish') ?></h4>
                        <div class="input">
                            <div class="left">
                                <input v-model.number="amount_replenish" placeholder="300" type="text">
                            </div>
                            <div class="right">
                                <p>EUR</p>
                            </div>
                        </div>
                    </div>
                        <div v-if="loading" class="row d-flex justify-content-center my-3">
                            <div class="spinner-border text-light" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>

                    <div v-if="paymentRecieved" class="wallet-popup__inner">
                        <h4><?= Yii::t('frontend.market', 'payment_summ') ?></h4>
                        <div class="copy-link radio-first__copy">
                            <span class="mdi mdi-content-copy clipboard-btn__wallet" data-clipboard-target="#copy-link-wallet"></span>
                            <input :value="coin_amount" type="text" id="copy-link-wallet">
                        </div>

                    </div>

                    <div v-if="paymentRecieved" class="wallet-popup__inner">
                        <h4><?= Yii::t('frontend.market', 'payment_address') ?></h4>
                        <div class="copy-link radio-first__copy">
                            <span class="mdi mdi-content-copy clipboard-btn__wallet" data-clipboard-target="#copy-link-wallet1"></span>
                            <input :value="address" type="text" id="copy-link-wallet1">
                        </div>
                    </div>

                    <button v-if="!paymentRecieved" @click.prevent="submitPayCoins" class="accent-button"><?= Yii::t('frontend.market', 'payment_pay') ?></button>
                    <div class="text-center">
                        <div class="cancel-custom"><?= Yii::t('frontend.market', 'cancel') ?></div>
                    </div>
                </div>
            </form>
        </div>
        <!-- 2 END -->









        <!----------------- Shop START ----------------->
        <section class="shop-top" id="shop=top">
            <div class="accent-title">
                <h2><?= Yii::t('frontend.market', 'packages') ?></h2>
                <span class="line"></span>
            </div>

            <div class="row mt-25">

                <? foreach ($packages as $package) : ?>
                <!-- Left -->
                <div class="col-lg-6 col-md-12 shop-top-card__column">
                    <div class="shop-top-card__item
                     <?php if($package->id == 1) : ?>
                     border-left-accent
                     <?php else : ?>
                     border-left-gray
                     <?php endif; ?>">
                        <div class="left">
                            <div class="title">
                                <p class="text-gray"><?= Yii::t('frontend.market', $package->type) ?> <span class="mdi mdi-chevron-right"></span></p>
                                <h3><?=$package->title?></h3>
                            </div>

                            <div class="bonus">
                                <? if ($package->id == $package::PACKAGE_CLIENT) : ?>
                                    <a href="#">+<?=$package->countBonusForClient()?> DM Bonus</a>
                                <? endif; ?>
                            </div>

                        </div>

                        <div class="center">
                            <div class="price">
                                <? if ($package->id == $package::PACKAGE_BUSINESS) : ?>
                                    <h3><?=$package->getDirectCost() ?> <span class="currency">€</span></h3>
                                <? endif; ?>
                                <? if ($package->id == $package::PACKAGE_CLIENT) : ?>
                                    <p class="text-gray"><?= Yii::t('frontend.market', $package->pay_term) ?></p>
                                    <h3><?=$package->cost?> <span class="currency">€</span></h3>
                                <? endif; ?>
                            </div>
                        </div>

                          <?php if ($package->isActiveforUser($package->id)) : ?>
                            <div class="right bg-dark-light">
                            <p class="text-gray"><?=Yii::t('frontend.market', 'paid')?> <?=Yii::$app->formatter->asDate($package->termForUser($package->id))?></p>
                           </div>
                          <? else : ?>

                              <?php if ($balance->euro >= $balance->convertEuroToCent($package->cost)) : ?>
                                  <div class="right" :class="{' bg-dark-light': disablePay<?=$package->id?>}">
                                      <p v-if="disablePay<?=$package->id?>" class="text-gray"><?=Yii::t('frontend.market', 'paid')?> {{ avaliable_till }}</p>
                                      <a v-if="!disablePay<?=$package->id?>" @click.prevent="submitPayBalance(<?=$package->id?>,<?=$package->cost?>, <?=$balance->euro?>)" href="#wallet-replenish" class="accent-button"><?= Yii::t('frontend.market', 'pay') ?></a>
                                  </div>
                              <?php else: ?>



                                  <div class="right">
                                      <a href="#wallet-withdraw-income" class="accent-button custom-magnific-popup"><?= Yii::t('frontend.market', 'pay') ?></a>
                                  </div>
                              <?php endif; ?>

                          <? endif; ?>




                    </div>
                </div>
                <? endforeach; ?>
            </div>

        </section>

        <section class="shop-events" id="shop-events">
            <div class="accent-title">
                <h2><?= Yii::t('frontend.market', 'events') ?></h2>
                <span class="line"></span>
            </div>

            <div class="event-ticket__wrap">
                <div class="row event-ticket-slider">

                    <? foreach ($events as $event) : ?>


                    <!-- 1 START -->
                    <div class="col-sm-6">
                        <div class="event-ticket__item">
                            <div class="photo">
                                <?= Html::img('@web/uploads/image/'.$event->type, ['alt' => $event->title, 'style' => 'height: 300px; width: 200px;']) ?>
                            </div>
                            <div class="content">
                                <h3 class="title"><?=$event->title?></h3>
                                <p class="date"><?=$event->date?></p>
                                <p class="text">
                                    <?=$event->text?>
                                </p>
                                <div class="bottom">
                                    <h3 class="price">
                                        <?=$event->cost?> <span class="currency">€</span>
                                    </h3>

                                    <div class="buy">
                                        <div class="increment increment-js">
                                            <a href="#" class="minus">-</a>
                                            <input class="value" min="0" type="number" value="1">
                                            <a href="#" class="plus">+</a>
                                        </div>
                                        <a href="#" class="accent-button"><?= Yii::t('frontend.market', 'buy') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 1 END -->

                    <? endforeach; ?>

                </div>
            </div>
        </section>
        <!----------------- Shop END ----------------->


        <!----------------- Shop START ----------------->
        <section class="shop-gold-bars">
            <div class="accent-title">
                <h2><?= Yii::t('frontend.market', 'gold_bars') ?></h2>
                <span class="line"></span>
            </div>

            <div class="card__wrap">
                <div class="row">

                    <!-- 1 START -->

                    <? foreach ($bars as $bar) : ?>

                    <div class="col-xl-2 col-md-3 col-sm-4">
                        <div class="card__item">
                            <div class="photo">
                                <img src="/images/gold-cart.png" alt="Gold bars">
                            </div>
                            <h3 class="title" title="<?=$bar->weight?><?= Yii::t('frontend.market', 'g') ?>">
                                <?=$bar->weight?><?= Yii::t('frontend.market', 'g') ?>
                            </h3>
                            <p class="price"><?=$bar->cost?> <span class="currency-min">DM</span></p>
                            <?= Html::a(Yii::t('frontend.market', 'buy_package'), Url::to(['user/']), ['class' => 'buy-button']) ?>
                            <?= Html::a(Yii::t('frontend.market', 'more'), Url::to(['user/']), ['class' => 'more']) ?>
                        </div>
                    </div>

                    <? endforeach; ?>

                    <!-- 1 END -->

                </div>
            </div>
        </section>
        <!----------------- Shop END ----------------->

    </div>
</div>



<?

$this->registerJsFile(Yii::getAlias('@web/themes/ligertheme/js/vue/users/market.js'), [
    'depends' => [
        \onmotion\vue\VueAsset::className(),
        \onmotion\vue\AxiosAsset::className(),
        \onmotion\vue\VueResourceAsset::className(),
        \onmotion\vue\VueRouterAsset::className(),
        \themes\ligertheme\frontend\assets\LigerThemeAsset::className(),
        \themes\ligertheme\frontend\assets\MapAsset::className(),


    ]
]);

?>