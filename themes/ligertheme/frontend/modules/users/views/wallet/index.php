<?

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

?>



<div id="my-content" class="p-wallet my-content">

    <div class="container-fluid">
        <!----------------- Wallet price START ----------------->
        <section class="wallet-price">

            <!-- Wallet POPUP START -->

            <!-- 1 START -->
            <div class="wallet-popup__wrap wallet-popup__withdraw custom-popup mfp-hide" id="wallet-replenish">
                    <div class="wallet-popup__item">
                        <h3><?= Yii::t('frontend.wallet', 'replenish_money') ?></h3>
                        <div v-if="paymentRecieved" class="timer my-3"><?= Yii::t('frontend.wallet', 'time_along') ?> {{ time }}</div>
                        <div v-if="paymentExceeded" class="timer my-3 text-danger">{{ paymentExceededText }}</div>
                        <div class="available">
                            <h4><?= Yii::t('frontend.wallet', 'avaliable_balance') ?></h4>
                            <div class="amount">
                                <?=Yii::$app->formatter->asCurrency($balance_data['euro'])?> EUR
                            </div>

                        </div>

                        <div class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'amount_replenish') ?></h4>
                            <div class="input">
                                <div class="left">
                                    <input v-model.number="amount_replenish" placeholder="300" type="text">
                                </div>
                                <div class="right">
                                    <p>EUR</p>
                                </div>
                            </div>
                        </div>
                        <div v-if="loading" class="row d-flex justify-content-center my-3">
                            <div class="spinner-border text-light" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>

                        <div v-if="paymentRecieved" class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'payment_summ') ?></h4>
                            <div class="copy-link radio-first__copy">
                                <span class="mdi mdi-content-copy clipboard-btn__wallet" data-clipboard-target="#copy-link-wallet"></span>
                                <input :value="coin_amount" type="text" id="copy-link-wallet">
                            </div>

                        </div>

                        <div v-if="paymentRecieved" class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'payment_address') ?></h4>
                            <div class="copy-link radio-first__copy">
                                <span class="mdi mdi-content-copy clipboard-btn__wallet" data-clipboard-target="#copy-link-wallet1"></span>
                                <input :value="address" type="text" id="copy-link-wallet1">
                            </div>
                        </div>

                        <button v-if="!paymentRecieved" @click.prevent="submitPayCoins" class="accent-button"><?= Yii::t('frontend.wallet', 'payment_pay') ?></button>
                        <div class="text-center">
                            <div class="cancel-custom"><?= Yii::t('frontend.wallet', 'cancel') ?></div>
                        </div>
                    </div>
            </div>
            <!-- 1 END -->

            <!-- 2 START -->
            <div class="wallet-popup__wrap wallet-popup__withdraw custom-popup mfp-hide" id="wallet-withdraw-income">
                    <div class="wallet-popup__item">
                        <h3><?= Yii::t('frontend.wallet', 'withdraw_money') ?></h3>

                        <div class="available">
                            <h4><?= Yii::t('frontend.wallet', 'available_withdrawal') ?></h4>
                            <div class="amount">

                                    <?=Yii::$app->formatter->asCurrency($balance_data['income'])?> EUR
                            </div>
                        </div>

                        <div class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'amount_withdrawal') ?></h4>
                            <div class="input">
                                <div class="left">
                                    <input v-model.number="amount_withdraw" placeholder="300" type="text">
                                </div>
                                <div class="right">
                                    <p>EUR</p>
                                </div>
                            </div>
                        </div>

                        <div class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'indicate_withdrawal') ?></h4>
                            <div class="withdraw-radio__wrap">
                                <div class="withdraw-radio__item active">
                                    <input type="radio" name="withdraw-money" id="to-bitcoin" data-wallet-tab-btn="bitcoin" checked>
                                    <label for="to-bitcoin"><?= Yii::t('frontend.wallet', 'on_bitcoin') ?></label>
                                </div>
                            </div>
                        </div>


                        <div class="warning">
                            <p><?= Yii::t('frontend.wallet', 'warning') ?></p>
                        </div>

                        <div class="wallet-tab__wrap">
                            <!---------------- Bitcoin START ---------------->
                            <div class="input__wrap tab-active" data-wallet-tab="bitcoin">
                                <div class="input__item">
                                    <label for="wallet-bitcoin"><?= Yii::t('frontend.wallet', 'bitcoin_wallet_number') ?></label>
                                    <input v-model="wallet_withdraw" type="text" id="wallet-bitcoin">
                                </div>
                            </div>
                            <!---------------- Bitcoin END ---------------->
                        </div>

                        <button v-if="!paymentRecieved" @click.prevent="withdrawMoney(<?=$balance_data['income']?>)" class="accent-button"><?= Yii::t('frontend.wallet', 'withdraw_money') ?></button>
                        <div class="text-center">
                            <div class="cancel-custom"><?= Yii::t('frontend.wallet', 'cancel') ?></div>
                        </div>
                    </div>
            </div>
            <!-- 2 END -->

            <!-- 3 START -->
            <div class="wallet-popup__wrap wallet-popup__withdraw custom-popup mfp-hide" id="wallet-withdraw-liger">
                <form>
                    <div class="wallet-popup__item">
                        <h3><?= Yii::t('frontend.wallet', 'withdraw_liger') ?></h3>

                        <div class="available">
                            <h4><?= Yii::t('frontend.wallet', 'available_withdrawal') ?></h4>
                            <div class="amount">2 150 EUR</div>
                        </div>

                        <div class="wallet-popup__inner">
                            <h4><?= Yii::t('frontend.wallet', 'amount_withdrawal') ?></h4>
                            <div class="input">
                                <div class="left">
                                    <input placeholder="300" type="text">
                                </div>
                                <div class="right">
                                    <p>EUR</p>
                                </div>
                            </div>
                        </div>

                        <div class="your-get">
                            <h4><?= Yii::t('frontend.wallet', 'your_get') ?></h4>
                            <div class="accent-color">295 EUR</div>
                        </div>

                        <div class="input__wrap">
                            <div class="input__item">
                                <label for="liger-number"><?= Yii::t('frontend.wallet', 'liger_number') ?></label>
                                <input type="text" id="liger-number">
                            </div>
                        </div>

                        <button class="accent-button"><?= Yii::t('frontend.wallet', 'withdraw_money') ?></button>
                        <div class="text-center">
                            <div class="cancel-custom"><?= Yii::t('frontend.wallet', 'cancel') ?></div>
                        </div>

                        <div class="text">

                            <p><?= Yii::t('frontend.wallet', 'text_1') ?></p>
                            <p><?= Yii::t('frontend.wallet', 'text_2') ?></p>
                            <p><?= Yii::t('frontend.wallet', 'text_3') ?></p>
                            <p><?= Yii::t('frontend.wallet', 'text_4') ?></p>
                            <p><?= Yii::t('frontend.wallet', 'text_5') ?></p>


                        </div>
                    </div>
                </form>
            </div>
            <!-- 3 END -->

            <!-- Wallet POPUP END -->

            <div class="row">
                <!-- 1 START -->
                <div class="col-xl-3 col-sm-4 col-12 wallet-top__column">
                    <div class="wallet-top__item">
                        <div class="wallet-top__title">
                            <p><?= Yii::t('frontend.wallet', 'wallet_balance') ?></p>
                            <p class="number"><?= Yii::$app->formatter->asCurrency($balance_data['euro']) ?> <span class="currency">€</span></p>
                        </div>

                        <div class="wallet-top__btn">
                            <a class="accent-button custom-magnific-popup" href="#wallet-replenish" data-effect="mfp-zoom-in"><?= Yii::t('frontend.wallet', 'replenish') ?></a>
                        </div>
                    </div>
                </div>
                <!-- 1 END -->

                <!-- 2 START -->
                <div class="col-xl-3 col-sm-4 col-12 wallet-top__column">
                    <div class="wallet-top__item">
                        <div class="wallet-top__title">
                            <p><?= Yii::t('frontend.wallet', 'your_income') ?></p>
                                <p class="number"><?=Yii::$app->formatter->asCurrency($balance_data['income'])?> <span class="currency">€</span></p>
                        </div>

                        <div class="wallet-top__btn">
                            <a class="accent-button custom-magnific-popup" href="#wallet-withdraw-income"><?= Yii::t('frontend.wallet', 'withdraw') ?></a>
                            <a class="how-button" href="#"><?= Yii::t('frontend.wallet', 'increase_income') ?></a>
                        </div>
                    </div>
                </div>
                <!-- 2 END -->

                <!-- 3 START -->
                <div class="col-xl-3 col-sm-4 col-12 wallet-top__column">
                    <div class="popup-custom mfp-hide" id="wallet-balance-popup">
                        <p>
                            <?= Yii::t('frontend.wallet', 'wallet_popup_info_1') ?>
                        </p>

                        <p>
                            <?= Yii::t('frontend.wallet', 'wallet_popup_info_2') ?>
                        </p>

                        <p>
                            <?= Yii::t('frontend.wallet', 'wallet_popup_info_3') ?>
                        </p>

                        <p>
                            <?= Yii::t('frontend.wallet', 'wallet_popup_info_4') ?>
                        </p>

                        <p>
                            <?= Yii::t('frontend.wallet', 'wallet_popup_info_5') ?>
                        </p>
                    </div>
                    <div class="wallet-top__item">
                        <div class="wallet-top__title">
                            <p><?= Yii::t('frontend.wallet', 'lp_balance') ?> <a href="#wallet-balance-popup" class="custom-magnific-popup"><span class="mdi mdi-information"></span></a></p>
                            <p class="number"><?= $balance->getUserBonuses() ?> <span class="currency">DM</span></p>
                        </div>
<!--
                        <div class="wallet-top__btn">
                            <a class="accent-button custom-magnific-popup" href="#wallet-withdraw-liger"><?= Yii::t('frontend.wallet', 'withdraw') ?></a>
                        </div>
                        -->
                    </div>
                </div>
                <!-- 3 END -->
            </div>
        </section>
        <!----------------- Wallet price END ----------------->

        <!----------------- Wallet transactions START ----------------->
        <section class="wallet-transactions" id="wallet-transactions">
            <div class="accent-title">
                <h2><?= Yii::t('frontend.wallet', 'transactions') ?></h2>
                <span class="line"></span>
            </div>

<div class="transactions-table">
    <?= GridView::widget([
        'layout' => '{items}',
        'dataProvider' => $dataProvider,

        'headerRowOptions' => [
            'class' => 'title table-custom__title'
        ],
        'tableOptions' => [
            'class' => 'table table table-custom'
        ],
        'afterRow' => function()
        {
          return '<tr class="gutter"></tr>';
        },
        'rowOptions' => ['class' => 'table__line table-custom__line'],
        'columns' => [
            [
                     'label' => Yii::t('frontend.wallet', 'transactions_hash'),
                     'attribute' =>  'transaction_id',
            ],
            [
                'attribute' =>   'payment_type',
                'label' => Yii::t('frontend.wallet', 'payment_type'),
                'contentOptions' => [
                        'class' => 'table-custom__column name'
                ],
                'value' => function($model)
                {
                    if($model->payment_type == 'replenishment')
                    {
                        return Yii::t('frontend.wallet', 'replenish_type');
                    }

                    else
                    {
                        return Yii::t('frontend.wallet', 'withdraw_type');
                    }
                }

            ],

            [
                    'label' => Yii::t('frontend.wallet', 'date'),
                    'attribute' =>   'created_at',
                    'contentOptions' => [
                        'class' => 'table-custom__column date'
                    ],
                    'value' => function($model)
                    {
                        return Yii::$app->formatter->asDatetime($model->created_at);
                    }
            ],

            [
                'label' => Yii::t('frontend.wallet', 'amount'),
                'attribute' =>   'payment_summ_eur',
                'contentOptions' => [
                    'class' => 'table-custom__column price'
                ],
                'value' => function($model)
                {
                    return '€ '.$model->convertToEuro($model->payment_summ_eur);
                }
            ],
            [
                'label' => Yii::t('frontend.wallet', 'status'),
                'attribute' =>    'payment_status',
                'contentOptions' => [
                    'class' => 'table-custom__column ',

                ],
                'value' => function($model)
                {
                    if ($model->payment_status == 0)
                    {
                        return Yii::t('frontend.wallet', 'waiting');
                    }

                    if ($model->payment_status == 1)
                    {
                        return Yii::t('frontend.wallet', 'confirmed');
                    }

                    if ($model->payment_status == 2)
                    {
                        return Yii::t('frontend.wallet', 'expired');
                    }
                },


            ],




        ],
    ]) ?>


</div>

            <div class="pagination">
                <?= LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'linkContainerOptions' => [
                        'class' => ''
                    ],
                    'prevPageLabel' => false,
                    'nextPageLabel' => false,

                ]);?>
            </div>
        </section>
        <!----------------- Wallet transactions END ----------------->
        <section class="wallet-course-liger">
            <!-- Diagram Wallet START -->
            <div class="dashboard-diagram">
                <div class="top">
                    <div class="accent-title">
                        <h2><?= Yii::t('frontend.wallet', 'liger_course') ?></h2>
                        <span class="line"></span>
                    </div>

                    <div class="mount-tab">
                        <a href="#"><?= Yii::t('frontend.wallet', 'liger_week') ?></a>
                        <a class="active" href="#"><?= Yii::t('frontend.wallet', 'liger_month') ?></a>
                        <a href="#"><?= Yii::t('frontend.wallet', 'liger_year') ?></a>
                    </div>
                </div>

                <canvas id="myChartWallet" width="100" height="25"></canvas>
            </div>
            <!-- Diagram Wallet END -->
        </section>

    </div>
</div>


<?

$this->registerJsFile(Yii::getAlias('@web/themes/ligertheme/js/vue/users/wallet.js'), [
    'depends' => [
        \onmotion\vue\VueAsset::className(),
        \onmotion\vue\AxiosAsset::className(),
        \onmotion\vue\VueResourceAsset::className(),
        \onmotion\vue\VueRouterAsset::className(),
        \themes\ligertheme\frontend\assets\LigerThemeAsset::className(),
        \themes\ligertheme\frontend\assets\MapAsset::className(),


    ]
]);

?>