<?php

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\widgets\LanguageSelector\LanguageSelector;
use common\widgets\Alert;
use kartik\select2\Select2;

?>


<div id="my-content" class="p-register my-content">

    <div class="container-fluid">

        <!---------------- Language START ---------------->
      
        <!---------------- Language END ---------------->
        <?= LanguageSelector::widget([
            'layout' => 'login_register'
        ]) ?>
        <!----------------- Login START ----------------->

        <section class="register">

            <div class="liger-logo">
                <?= Html::img('/images/logo.svg') ?>
            </div>

             <?php $form = ActiveForm::begin([
                    'id' => 'registration-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => false,
                ]); ?>

            <?= $form->field($model, 'refferal_id')->hiddenInput()->label(false); ?>

                <div class="accent-title">
                    <h2><?= Yii::t('frontend.users', 'registration') ?></h2>
                </div>

                <div class="custom-popup">
                    <div class="input__wrap">
                        <div class="input__item">

                            <label for="login-partner"><?= Yii::t('frontend.users', 'partner_login') ?></label>

                              <?= $form->field($model, 'user_token', [
                               'inputOptions' => [
                               'id' => 'login-partner',
                                   'disabled' => !empty($model->user_token) ? true : false,
                                   'readonly' => !empty($model->user_token) ? true : false,
                               ]
                               ])->label(false) ?>

                        </div>

                        <div class="input__item">
                            <label for="your-login"><?= Yii::t('frontend.users', 'your_login') ?></label>
                           <?= $form->field($model, 'username', [
                            'inputOptions' => [
                             'id' => 'your-login'
                            ]
                            ])->label(false) ?>
                        </div>

                        <div class="input__item">
                            <label for="email"><?= Yii::t('frontend.users', 'email') ?></label>
                            <?= $form->field($model, 'email', [
                            'inputOptions' => [
                             'id' => 'email'
                            ]
                            ])->label(false) ?>
                           
                        </div>

                       

                <?php if ($module->enableGeneratingPassword == false): ?>
                    <div class="input__item">
                            <label for="register-password"><?= Yii::t('frontend.users', 'password') ?></label>
                            <?= $form->field($model, 'password', [
                            'inputOptions' => [
                             'id' => 'register-password'
                            ]
                            ])->passwordInput()->label(false) ?>

                    </div>
                <?php endif ?>


                           
                      

                        <div class="input__item">
                            <label for="register-password"><?= Yii::t('frontend.users', 'choose_country') ?></label>
                            <?=
                            $form->field($model, 'country')->
                            dropDownList(ArrayHelper::map($model->getCountries(), 'code', 'title'),
                                [
                                    'id' => 'register-password',
                                    'class' => ''
                                ]
                            )->label(false);

                            ?>


                        </div>
                    </div>

                    <?= Html::submitButton(Yii::t('frontend.users', 'register_go'), ['class' => 'accent-button']) ?>

                    <div class="text-center">
                        <?= Html::a(
                            Yii::t('frontend.users', 'button_login'),
                        ['/user/security/login'],
                        [
                            'class' => 'forgot-password',
                            'tabindex' => '5'
                        ]

                            ) ?>
                    </div>
                </div>
             <?php ActiveForm::end(); ?>
        </section>
        <!----------------- Background effect START ----------------->
        <div id="background-effect" class="background-effect"></div>
        <!----------------- Background effect END ----------------->
        <!----------------- Login END ----------------->
    </div>
</div>

