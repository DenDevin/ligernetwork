<?php

use yii\bootstrap\Alert;

?>





<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;



?>


<div id="my-content" class="p-register my-content">

    <div class="container-fluid">
        <section class="register">
            <div class="custom-popup">
            <div class="liger-logo">
                <?= Html::img('/images/logo.svg') ?>
            </div>
                <? if (isset($title)):  ?>
                <div class="form-group">
                    <div class="jumbotron">
                        <p class="lead"><?=$title?></p>
                    </div>
                </div>
                <? endif; ?>


                        <div class="row">
                            <div class="col-xs-12 mt-4 d-flex">
                                <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                                    <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>

                                        <div class="alert alert-<?=$type?> alert-dismissible fade show" role="alert">
                                            <?= $message; ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        </div>


                </div>
            </div>
        </section>
        <!----------------- Background effect START ----------------->
        <div id="background-effect" class="background-effect"></div>
        <!----------------- Background effect END ----------------->
        <!----------------- Login END ----------------->
    </div>
</div>













