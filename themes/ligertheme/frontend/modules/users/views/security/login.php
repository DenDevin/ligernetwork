<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\widgets\LanguageSelector\LanguageSelector;


?>


<div id="my-content" class="p-login my-content pt-5">
    <div class="container-fluid">

        <!---------------- Language START ---------------->
        <?= LanguageSelector::widget([
         'layout' => 'login_register'
        ]) ?>
        <!---------------- Language END ---------------->

        <!----------------- Login START ----------------->
        <section class="s-login">
            <div class="liger-logo">
                <?= Html::img('/images/logo.svg') ?>
            </div>
           
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
            ]) ?>

                <div class="accent-title">
                    <h2><?= Yii::t('frontend.users', 'entrance') ?></h2>
                </div>

                <div class="custom-popup">
                    <div class="input__item">
                        <label for="login-name"><?= Yii::t('frontend.users', 'nickname') ?></label>
                        <?php if ($module->debug): ?>
                            <?= $form->field($model, 'login', [
                                'inputOptions' => [
                                    'autofocus' => 'autofocus',
                                    'id' => 'login-name',
                                    'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                            ?>

                        <?php else: ?>

                            <?= $form->field($model, 'login',
                                ['inputOptions' => [
                                        'autofocus' => 'autofocus',
                                        'id' => 'login-name',
                                        'tabindex' => '1']]
                            )->label(false);
                            ?>

                        <?php endif ?>

                    </div>

                    <div class="input__item">
                        <label for="login-password"><?= Yii::t('frontend.users', 'password') ?></label>
                        <?php if ($module->debug): ?>
                            <div class="alert alert-warning">
                                <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                            </div>
                        <?php else: ?>
                            <?= $form->field(
                                $model,
                                'password',
                                ['inputOptions' => [
                                        'id' => 'login-password',
                                        'tabindex' => '2']])
                                ->passwordInput()->label(false)
                            ?>
                        <?php endif ?>

                    </div>

                    <?= Html::submitButton(
                        Yii::t('frontend.users', 'log_in'),
                        ['class' => 'accent-button', 'tabindex' => '4']
                    ) ?>

                    <? if ($module->enablePasswordRecovery) : ?>
                    <div class="text-center">
                        <?= Html::a(
                        Yii::t('frontend.users', 'forgot_password'),
                        ['/user/recovery/request'],
                        [
                             'class' => 'forgot-password',
                             'tabindex' => '5'
                        ]
                        ) ?>
                    </div>
                    <? endif; ?>
                    <? if ($module->enableRegistration): ?>
                    <div class="text-center">
                        <?= Html::a(
                            Yii::t('frontend.users', 'register'),
                        ['/user/registration/register'],
                        [
                            'class' => 'forgot-password',
                            'tabindex' => '5'
                        ]

                            ) ?>
                    </div>
                    <? endif; ?>

                    <?php if ($module->enableConfirmation): ?>
                    <div class="text-center">
                            <?= Html::a(
                                Yii::t('frontend.users', 'resend'),
                                ['/user/registration/resend'],
                                [
                                    'class' => 'forgot-password',
                                    'tabindex' => '5'
                                ]



                            ) ?>
                    </div>
                    <?php endif ?>
                </div>
            <?php ActiveForm::end(); ?>
        </section>

        <!----------------- Background effect START ----------------->
        <div id="background-effect" class="background-effect"></div>
        <!----------------- Background effect END ----------------->
        <!----------------- Login END ----------------->

    </div>
</div>
