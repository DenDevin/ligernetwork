<?php

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\widgets\LanguageSelector\LanguageSelector;

?>


<div id="my-content" class="p-register my-content">

    <div class="container-fluid">

        <!---------------- Language START ---------------->
      
        <!---------------- Language END ---------------->
        <?= LanguageSelector::widget([
            'layout' => 'login_register'
        ]) ?>
        <!----------------- Login START ----------------->
        <section class="register">
            <div class="liger-logo">
                <?= Html::img('/images/logo.svg') ?>
            </div>

              <?php $form = ActiveForm::begin([
                    'id' => 'password-recovery-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>

                <div class="accent-title">
                    <h2><?= Yii::t('frontend.users', 'recovery_password') ?></h2>
                </div>

                <div class="custom-popup">
                    <div class="input__wrap">
                        <div class="input__item">
                            <label for="email"><?= Yii::t('frontend.users', 'email') ?></label>
                            <?= $form->field($model, 'email', [
                            'inputOptions' => [
                             'id' => 'email'
                            ]
                            ])->label(false) ?>
                           
                        </div>
                    </div>

                    <?= Html::submitButton(Yii::t('frontend.users', 'continue'), ['class' => 'accent-button']) ?>

                </div>
             <?php ActiveForm::end(); ?>
        </section>
        <!----------------- Background effect START ----------------->
        <div id="background-effect" class="background-effect"></div>
        <!----------------- Background effect END ----------------->
        <!----------------- Login END ----------------->
    </div>
</div>
