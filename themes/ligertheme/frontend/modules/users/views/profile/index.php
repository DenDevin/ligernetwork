<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use themes\ligertheme\frontend\widgets\FileUpload\FileUpload;




?>

<div id="my-content" class="p-profile my-content">

    <div class="container-fluid">
        <!----------------- News inner START ----------------->
        <section class="profile">
            <div class="profile__wrap">
                <ul class="profile__tab custom__tab">
                    <li id="profile-tab__personal-information" class="active"><a href="#personal-information"><?= Yii::t('frontend.profile', 'personal_information')?></a></li>
                    <? if(!$verify->isVerified($user->id)) : ?>
                    <li id="profile-tab__personal-verification"><a href="#verification__wrap"><?= Yii::t('frontend.profile', 'verification')?></a></li>
                    <? endif; ?>
                 <!--<li id="profile-tab__personal-two-autentification"><a href="#authentication__wrap"><?= Yii::t('frontend.profile', 'two_authentication')?></a></li>-->
                    <li id="profile-tab__personal-change-password"><a href="#change-password__wrap"><?= Yii::t('frontend.profile', 'change_password')?></a></li>
                </ul>

                <!-- Tab 1 Content START -->
                <div class="personal-information personal-tab__content" id="personal-information">
                    <div class="row">





                        <div class="col-lg-8">

                            <?php $form = ActiveForm::begin([
                                'id' => 'profile-form',

                            ]); ?>


                            <div class="top">
                                <div class="info__wrap">
                                    <div class="accent-title maw-n">
                                        <h2><?= Yii::t('frontend.profile', 'profile')?></h2>
                                        <span class="line"></span>
                                    </div>

                                    <div class="info">
                                        <div class="avatar">
                                            <img src="/images/avatar-profile.png" alt="Julie Cortez">
                                        </div>
                                        <div class="text">
                                            <div class="login text__item">
                                                <p class="login min-title"><?= Yii::t('frontend.profile', 'your_login')?></p>
                                                <p class="accent-color"><?= Yii::$app->user->identity->username ?></p>
                                            </div>

                                            <div class="id text__item">
                                                <p class="login min-title"><?= Yii::t('frontend.profile', 'account_id')?></p>
                                                <p class="accent-color"><?= Yii::$app->user->identity->user_token ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="referral-link__wrap">
                                    <div class="accent-title maw-n">
                                        <h2><?= Yii::t('frontend.profile', 'your_referral_link')?></h2>
                                        <span class="line"></span>
                                    </div>
                                    <? if (Yii::$app->useridentity->isActivePartner()) : ?>
                                    <div class="referal-custom">
                                        <button data-clipboard-target="#referal-link" class="mdi mdi-content-copy clipboard-btn"></button>
                                        <input id="referal-link" type="text" value="<?= Yii::$app->urlManager->createAbsoluteUrl(['id/'.Yii::$app->user->identity->user_token]); ?>">
                                    </div>
                                    <? else : ?>
                                        <div class="referal-custom">
                                            <?= Yii::t('frontend.profile', 'avaliable_after_business_buy')?>
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>


                                <div class="custom-form">
                                    <div class="form__left">
                                        <div class="tagged">
                                            <label for="personal-name"><?= Yii::t('frontend.profile', 'name')?></label>
                                           <input id="personal-name" type="text" value="<?=$model->name?>" readonly>
                                        </div>

                                        <div class="tagged">
                                            <label for="personal-email"><?= Yii::t('frontend.profile', 'email')?></label>
                                            <input id="personal-email" type="text" value="<?=$model->public_email?>" readonly>
                                        </div>

                                        <div class="tagged">
                                            <label for="personal-telegram"><?= Yii::t('frontend.profile', 'telegram')?></label>
                                            <?= $form->field($model, 'telegram')->textInput(['id' => 'personal-telegram', 'class' => ''])->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="form__right">
                                        <div class="tagged">
                                            <label for="personal-lastname"><?= Yii::t('frontend.profile', 'surname')?></label>
                                            <input id="personal-lastname" type="text" value="<?=$model->surname?>" readonly>
                                        </div>

                                        <div class="tagged">
                                            <label for="personal-phone"><?= Yii::t('frontend.profile', 'phone')?></label>
                                            <input id="personal-phone" type="text" value="<?=$model->phone?>" placeholder="+78 123 456 78 90" readonly>
                                        </div>

                                        <div class="tagged">
                                            <label for="personal-date"><?= Yii::t('frontend.profile', 'date_of_birth')?></label>
                                            <input id="personal-date" type="text" value="<?=$model->date_birth?>" placeholder="01/01/2001" readonly>
                                        </div>
                                    </div>
                                </div>
                                <?= Html::submitButton(Yii::t('frontend.profile', 'save'), ['class' => 'accent-button']) ?>

                            <?php ActiveForm::end(); ?>
                        </div>

                        <div class="col-lg-4">

                            <div class="personal-notification">
                                <div class="accent-title maw-n">
                                    <h2><?= Yii::t('frontend.profile', 'notifications')?></h2>
                                    <span class="line"></span>
                                </div>
                                <div class="notification__wrap">
                                    <? if(count($notifications) > 0) : ?>
                                        <? foreach($notifications as $notification) : ?>
                                    <a href="#" class="notification__item border-left-accent">
                                        <h3 class="title text-<?= $notification->type ?>"><?= $notification->title ?></h3>
                                        <p class="color-white"><?= $notification->text ?></p>
                                        <div class="date"></div>
                                    </a>
                                        <? endforeach; ?>
                                    <? else: ?>

                                            <p class="color-white"><?= Yii::t('frontend.message', 'no_notifications') ?></p>

                                        </a>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Tab 1 Content END -->


                <? if(!$verify->isVerified($user->id)) : ?>

                <!-- Tab 2 Verification START -->
                <div class="verification__wrap personal-tab__content hidden" id="verification__wrap">
                    <div class="accent-title">
                        <h2><?= Yii::t('frontend.profile', 'identity_verification')?></h2>
                        <span class="line"></span>
                    </div>

                    <div class="dark-block">
                        <p class="color-red"><?= Yii::t('frontend.profile', 'not_verified')?></p>
                    </div>


                        <div class="custom-form">
                            <?php $formVerify = ActiveForm::begin([
                                'options' => [
                                    'enctype' => 'multipart/form-data'],
                                    'id' => 'verify-form',

                            ]); ?>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="verification__item">
                                        <div class="info-popup hidden" id="verification-name-popup">
                                            <p><?= Yii::t('frontend.profile', 'verify_step_1')?></p>
                                            <span class="mdi mdi-close"></span>
                                        </div>

                                        <h3 class="title h3">01. <?= Yii::t('frontend.profile', 'identity_verification')?> <a href="#verification-name-popup" class="mdi mdi-information verification-information"></a></h3>
                                        <div class="tagged">
                                            <label for="verification-name"><?= Yii::t('frontend.profile', 'name')?></label>
                                            <?= $formVerify->field($verify, 'name')->textInput(['id' => 'verification-name', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-lastname"><?= Yii::t('frontend.profile', 'surname')?></label>
                                            <?= $formVerify->field($verify, 'surname')->textInput(['id' => 'verification-lastname', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-patronymic"><?= Yii::t('frontend.profile', 'middle_name')?></label>
                                            <?= $formVerify->field($verify, 'second_name')->textInput(['id' => 'verification-patronymic', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-date"><?= Yii::t('frontend.profile', 'date_of_birth')?></label>
                                            <?= $formVerify->field($verify, 'date_birth')->textInput(['id' => 'verification-date', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-document"><?= Yii::t('frontend.profile', 'series_number_document')?></label>
                                            <?= $formVerify->field($verify, 'doc_serial_num')->textInput(['id' => 'verification-document', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-country-document"><?= Yii::t('frontend.profile', 'country_of_issue')?></label>
                                            <?= $formVerify->field($verify, 'doc_country')->textInput(['id' => 'verification-country-document', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-term"><?= Yii::t('frontend.profile', 'valid_until')?></label>
                                            <?= $formVerify->field($verify, 'doc_valid_to')->textInput(['id' => 'verification-term', 'class' => ''])->label(false) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="verification__item">
                                        <div class="info-popup hidden" id="verification-address-popup">
                                            <p><?= Yii::t('frontend.profile', 'verify_step_2')?></p>
                                            <span class="mdi mdi-close"></span>
                                        </div>

                                        <h3 class="title h3">02. <?= Yii::t('frontend.profile', 'adress_verification')?> <a href="#verification-address-popup" class="mdi mdi-information verification-information"></a></h3>

                                        <div class="tagged tagged-not-before">
                                            <?= $formVerify->field($verify, 'address')->textArea(['id' => 'verification-address', 'class' => '', 'placeholder' => Yii::t('frontend.profile', 'address')])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-city"><?= Yii::t('frontend.profile', 'city')?></label>
                                            <?= $formVerify->field($verify, 'city')->textInput(['id' => 'verification-city', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-email-index"><?= Yii::t('frontend.profile', 'postcode')?></label>
                                            <?= $formVerify->field($verify, 'postcode')->textInput(['id' => 'verification-email-index', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-city-document"><?= Yii::t('frontend.profile', 'country_of_issue')?></label>
                                            <?= $formVerify->field($verify, 'country_issue')->textInput(['id' => 'verification-city-document', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged">
                                            <label for="verification-date-issue"><?= Yii::t('frontend.profile', 'date_of_issue')?></label>
                                            <?= $formVerify->field($verify, 'date_issue')->textInput(['id' => 'verification-date-issue', 'class' => ''])->label(false) ?>
                                        </div>

                                        <div class="tagged input-accept">
                                            <?= $formVerify->field($verify, 'accept')->label(false)->checkbox(['enableLabel' => false, 'id' => 'verification-accept', 'class' => '', 'checked' => true], false)?>
                                            <label for="verification-accept"><?= Yii::t('frontend.profile', 'i_accept')?></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="verification__upload">
                                        <div class="verification__item">
                                            <div class="info-popup hidden" id="passport-photo-popup">
                                                <p><?= Yii::t('frontend.profile', 'verify_step_3')?></p>
                                                <span class="mdi mdi-close"></span>
                                            </div>
                                            <h3 class="title h3">03. <?= Yii::t('frontend.profile', 'passport_color')?> <a href="#passport-photo-popup" class="mdi mdi-information verification-information"></a></h3>
                                        </div>

                                        <div class="tagged tagged-upload tagged-not-before">
                                            <label for="passport-photo">
                                                <h4><?= Yii::t('frontend.profile', 'png_jpg')?></h4>
                                                <p class="color-gray"><?= Yii::t('frontend.profile', 'max_size')?></p>
                                                <div class="mdi mdi-cloud-download"></div>
                                            </label>

                                            <?= $formVerify->field($verify, 'pass_scan')->fileInput(['id' => 'passport-photo', 'class' => ''])->label(false) ?>

                                        </div>
                                    </div>

                                    <div class="verification__upload">
                                        <div class="verification__item">
                                            <div class="info-popup hidden" id="color-scan-popup">
                                                <p><?= Yii::t('frontend.profile', 'verify_step_4')?></p>
                                                <span class="mdi mdi-close"></span>
                                            </div>
                                            <h3 class="title h3">04. <?= Yii::t('frontend.profile', 'your_photo_passport')?> <a href="#color-scan-popup" class="mdi mdi-information verification-information"></a></h3>
                                        </div>

                                        <div class="tagged tagged-upload tagged-not-before">
                                            <label for="color-scan">
                                                <h4><?= Yii::t('frontend.profile', 'png_jpg')?></h4>
                                                <p class="color-gray"><?= Yii::t('frontend.profile', 'max_size')?></p>
                                                <div class="mdi mdi-cloud-download"></div>
                                            </label>
                                            <?= $formVerify->field($verify, 'pass_photo')->fileInput(['id' => 'color-scan', 'class' => ''])->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="verification__upload">
                                        <div class="verification__item">
                                            <div class="info-popup hidden" id="receipt-passport-popup">
                                                <p><?= Yii::t('frontend.profile', 'verify_step_5')?></p>
                                                <span class="mdi mdi-close"></span>
                                            </div>

                                            <h3 class="title h3">05. <?= Yii::t('frontend.profile', 'receipt_address')?> <a href="#receipt-passport-popup" class="mdi mdi-information verification-information"></a></h3>
                                        </div>

                                        <div class="tagged tagged-upload tagged-not-before">
                                            <label for="receipt-passport">
                                                <h4><?= Yii::t('frontend.profile', 'png_jpg')?></h4>
                                                <p class="color-gray"><?= Yii::t('frontend.profile', 'max_size')?></p>
                                                <div class="mdi mdi-cloud-download"></div>
                                            </label>
                                            <?= $formVerify->field($verify, 'address_invoice')->fileInput(['id' => 'receipt-passport', 'class' => ''])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?= Html::submitButton(Yii::t('frontend.profile', 'send_verification'), ['class' => 'accent-button']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>

                </div>

               <? endif; ?>


                <!-- Tab 2 Verification END -->

                <!-- Tab 3 Autentification START -->
                <!--
                <div class="authentication__wrap personal-tab__content hidden" id="authentication__wrap">
                    <form>
                        <div class="row">
                            <div class="col-lg-4 authentication__column">
                                <div class="authentication__item list__item">
                                    <div class="content">
                                        <h2 class="title"><span class="underline">01.</span> <?= Yii::t('frontend.profile', 'install_app')?></h2>
                                        <ul>
                                            <li><?= Yii::t('frontend.profile', 'google_for_ios')?></li>
                                            <li><?= Yii::t('frontend.profile', 'google_for_android')?></li>
                                            <li><?= Yii::t('frontend.profile', 'yandex_for_ios')?></li>
                                            <li><?= Yii::t('frontend.profile', 'yandex_for_android')?></li>
                                            <li><?= Yii::t('frontend.profile', 'authenticator_for_windows')?></li>
                                            <li><?= Yii::t('frontend.profile', 'authy_all')?></li>
                                            <li><?= Yii::t('frontend.profile', 'authenticator_for_chrome')?></li>
                                            <li><?= Yii::t('frontend.profile', 'freeotp_all')?></li>
                                        </ul>
                                    </div>
                                    <div class="border"></div>
                                </div>
                            </div>

                            <div class="col-lg-4 authentication__column">
                                <div class="authentication__item qr__item">
                                    <div class="content">
                                        <h2 class="title"><span class="underline">02.</span> <?= Yii::t('frontend.profile', 'scan_qr')?></h2>
                                        <div class="image">
                                            <img src="/images/qr-code.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="border"></div>
                                </div>
                            </div>

                            <div class="col-lg-4 authentication__column">
                                <div class="authentication__item code__item">
                                    <div class="content">
                                        <h2 class="title"><span class="underline">03.</span> <?= Yii::t('frontend.profile', 'enter_code')?></h2>
                                        <div class="code">
                                            <p><?= Yii::t('frontend.profile', 'code')?></p>
                                        </div>
                                        <div class="activate-code">
                                            <input class="input-first mask-number" type="text"  placeholder="000" max_n=3 tabindex="1">
                                            <input class="input-second mask-number" type="text" placeholder="000" max_n=3 tabindex="2">
                                            <div class="bottom-line">
                                                <span data-number-first="1"></span>
                                                <span data-number-first="2"></span>
                                                <span data-number-first="3"></span>

                                                <span data-number-second="1"></span>
                                                <span data-number-second="2"></span>
                                                <span data-number-second="3"></span>
                                            </div>
                                        </div>
                                        <button class="accent-button"><?= Yii::t('frontend.profile', 'activate_2fa')?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
-->
                <!-- Tab 3 Autentification END -->
                <div class="change-password__wrap personal-tab__content hidden" id="change-password__wrap">

                    <?php $form = ActiveForm::begin([
                        'id' => 'profile-form-pass',

                    ]); ?>

                        <div class="accent-title">
                            <h2><?= Yii::t('frontend.profile', 'change_pass')?></h2>
                            <span class="line"></span>
                        </div>
                        <div class="custom-form">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tagged">
                                        <label for="old-password"><?= Yii::t('frontend.profile', 'old_password')?></label>
                                        <?= $form->field($model, 'old_pass')->passwordInput(['id' => 'old-password', 'class' => ''])->label(false) ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="tagged">
                                        <label for="new-password"><?= Yii::t('frontend.profile', 'new_password')?></label>
                                        <?= $form->field($model, 'new_pass')->passwordInput(['id' => 'new-password', 'class' => ''])->label(false) ?>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="tagged">
                                        <label for="repeat-password"><?= Yii::t('frontend.profile', 'repeat_password')?></label>
                                        <?= $form->field($model, 'repeat_pass')->passwordInput(['id' => 'repeat-password', 'class' => ''])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <?= Html::submitButton(Yii::t('frontend.profile', 'save'), ['class' => 'accent-button']) ?>
                        </div>


                    <?php ActiveForm::end(); ?>





                </div>
                <!-- Tab 4 Change password START -->

                <!-- Tab 4 Change password END -->
            </div>

        </section>
        <!----------------- News inner END ----------------->
    </div>