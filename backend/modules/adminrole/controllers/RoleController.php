<?php

namespace backend\modules\adminrole\controllers;

use dektrium\rbac\controllers\RoleController as BaseRoleController;
use yii\rbac\Role;
use yii\web\NotFoundHttpException;
use yii\rbac\Item;



class RoleController extends BaseRoleController
{

    protected $modelClass = 'backend\modules\adminrole\models\Role';

    protected $type = Item::TYPE_ROLE;


    protected function getItem($name)
    {
        $role = \Yii::$app->authManager->getRole($name);

        if ($role instanceof Role) {
            return $role;
        }

        throw new NotFoundHttpException;
    }

}
