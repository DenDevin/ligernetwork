<?php

namespace backend\modules\marketing\models;

use Yii;

/**
 * This is the model class for table "user_marketing_team".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $title
 * @property int|null $level
 * @property int|null $bonus
 * @property int|null $percent
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserMarketingTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_marketing_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'bonus', 'percent', 'created_at', 'updated_at'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
            [['active'], 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'title' => 'Алиас',
            'level' => 'Уровень',
            'bonus' => 'Бонус',
            'percent' => 'Процент',
            'active' => 'Активность',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
