<?php

namespace backend\modules\marketing\models;

use Yii;
use backend\modules\marketing\models\query\UserBonusesQuery;

/**
 * This is the model class for table "user_bonuses".
 *
 * @property int $id
 * @property int|null $bonus_id
 * @property int|null $min
 * @property int|null $max
 * @property int|null $amount
 * @property string|null $data
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserBonuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_bonuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bonus_id', 'min', 'max', 'amount'], 'integer'],
            [['data'], 'string'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_id' => 'Бонус ID',
            'min' => 'Мин. кол-во оплат',
            'max' => 'Макс. кол-во оплат',
            'amount' => 'Размер бонуса',
            'data' => 'Данные',
            'active' => 'Активность',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UserBonusesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserBonusesQuery(get_called_class());
    }
}
