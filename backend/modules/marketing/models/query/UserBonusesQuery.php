<?php

namespace backend\modules\marketing\models\query;

/**
 * This is the ActiveQuery class for [[UserBonuses]].
 *
 * @see UserBonuses
 */
class UserBonusesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserBonuses[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserBonuses|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
