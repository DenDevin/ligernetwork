<?php

namespace backend\modules\marketing\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\marketing\models\UserMarketing]].
 *
 * @see \backend\modules\marketing\models\UserMarketing
 */
class UserMarketingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \backend\modules\marketing\models\UserMarketing[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \backend\modules\marketing\models\UserMarketing|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
