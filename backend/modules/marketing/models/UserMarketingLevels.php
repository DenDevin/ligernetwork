<?php

namespace backend\modules\marketing\models;

use Yii;

/**
 * This is the model class for table "user_marketing_levels".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $level_id
 * @property int|null $bonus
 * @property int|null $percent
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserMarketingLevels extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_marketing_levels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level_id', 'bonus', 'percent', 'active', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'level_id' => 'Уровень',
            'bonus' => 'Бонус',
            'percent' => 'Процент',
            'active' => 'Активность',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
}
