<?php

namespace backend\modules\marketing\models;

use Yii;

/**
 * This is the model class for table "user_partner_bonuses".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $partners_count
 * @property int|null $level
 * @property int|null $amount
 * @property string|null $data
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserPartnerBonuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_partner_bonuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['partners_count', 'level', 'amount', 'active', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'partners_count' => 'Количество партнеров',
            'level' => 'Уровень',
            'amount' => 'Сумма бонуса',
            'data' => 'Данные',
            'active' => 'Активность',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
