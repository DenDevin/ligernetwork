<?php

namespace backend\modules\marketing\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\marketing\models\UserPartnerBonuses;

/**
 * UserPartnerBonusesSearch represents the model behind the search form of `backend\modules\marketing\models\UserPartnerBonuses`.
 */
class UserPartnerBonusesSearch extends UserPartnerBonuses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'partners_count', 'level', 'amount', 'active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'data'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPartnerBonuses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partners_count' => $this->partners_count,
            'level' => $this->level,
            'amount' => $this->amount,
            'active' => $this->active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
