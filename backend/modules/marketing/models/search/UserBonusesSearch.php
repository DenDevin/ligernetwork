<?php

namespace backend\modules\marketing\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\marketing\models\UserBonuses;

/**
 * UserBonusesSearch represents the model behind the search form of `backend\modules\billing\models\UserBonuses`.
 */
class UserBonusesSearch extends UserBonuses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bonus_id', 'min', 'max', 'amount', 'active', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserBonuses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_id' => $this->bonus_id,
            'min' => $this->min,
            'max' => $this->max,
            'amount' => $this->amount,
            'active' => $this->active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
