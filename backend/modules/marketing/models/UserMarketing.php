<?php

namespace backend\modules\marketing\models;

use Yii;
use common\models\Payment\UserBalance;

/**
 * This is the model class for table "user_marketing".
 *
 * @property int $id
 * @property string|null $title
 * @property int $product_return
 * @property int $income
 * @property int $leader_bonus
 * @property int $month_income
 * @property string|null $data
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserMarketing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_marketing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active'], 'boolean'],
            [['product_return', 'income', 'leader_bonus', 'month_income'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'product_return' => 'Товарооборот (Евро)',
            'income' => 'Доход (Евро)',
            'leader_bonus' => 'Лидерский Бонус (Евро)',
            'month_income' => 'Месячный Доход (Евро)',
            'data' => 'Данные',
            'active' => 'Активность',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \backend\modules\marketing\models\query\UserMarketingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\marketing\models\query\UserMarketingQuery(get_called_class());
    }

    public static function getMarketingStatuses()
    {
       return self::find()->asArray()->all();
    }


    public static function getUserStatusesData()
    {
        $statuses = [];
        $balance = UserBalance::getUserBalanceByToken(Yii::$app->user->identity->user_token);
        $statuses['prev'] = Yii::$app->useridentity->getUserPreviousStatus();
        $statuses['next'] = Yii::$app->useridentity->getUserNextStatus();
        $statuses['current'] = Yii::$app->useridentity->getUserPartnerStatus();
        $statuses['product_return'] = self::convertToEuro($balance->product_return);
        $statuses['prev_product_return'] = Yii::$app->useridentity->getPrevProductReturn($statuses['current']);

        return $statuses;
    }



    public static function convertToCent($euro)
    {
        return $euro * 100;
    }


    public static function convertToEuro($cent)
    {
        return $cent / 100;
    }


}
