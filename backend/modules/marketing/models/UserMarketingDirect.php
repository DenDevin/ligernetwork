<?php

namespace backend\modules\marketing\models;

use Yii;

/**
 * This is the model class for table "user_marketing_direct".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $title
 * @property int|null $first_pay
 * @property int|null $level
 * @property int|null $first_pay_percent
 * @property int|null $next_pay
 * @property int|null $next_pay_percent
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class UserMarketingDirect extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_marketing_direct';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_pay', 'level', 'first_pay_percent', 'next_pay', 'next_pay_percent', 'active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'title' => 'Алиас',
            'first_pay' => 'Первый платеж',
            'level' => 'Уровень',
            'first_pay_percent' => 'Процент за первый платеж',
            'next_pay' => 'Каждый следующий платеж',
            'next_pay_percent' => 'Процент за каждый следующий платеж',
            'active' => 'Активный',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
