<?php

namespace backend\modules\marketing;


use Yii;
use yii\filters\AccessControl;

class Module extends \yii\base\Module
{

    public $layout = '@themes/ligertheme/backend/views/layouts/main';
    public $controllerNamespace = 'backend\modules\marketing\controllers';

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superadmin']
                    ]
                ]
            ]

        ];
    }




    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
