<?php

namespace backend\modules\adminuser;

use Yii;
use yii\filters\AccessControl;

class Module extends \yii\base\Module
{

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superadmin']
                    ]
                ]
            ]

        ];
    }


    public $layout = '@themes/ligertheme/backend/views/layouts/main';
    public $controllerNamespace = 'backend\modules\adminuser\controllers';


    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
