<?php

namespace backend\modules\adminuser\controllers;

use yii\web\Controller;
use dektrium\rbac\controllers\AssignmentController as BaseAssignmentController;
use dektrium\rbac\models\Assignment;
use Yii;


class AssignmentController extends BaseAssignmentController
{
    public function actionAssign($id)
    {
        $model = Yii::createObject([
            'class'   => Assignment::className(),
            'user_id' => $id,
        ]);

        if ($model->load(\Yii::$app->request->post()) && $model->updateAssignments()) {
        }

        return \dektrium\rbac\widgets\Assignments::widget([
            'model' => $model,
        ]);
        /*$model = Yii::createObject([
            'class'   => Assignment::className(),
            'user_id' => $id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->updateAssignments()) {

        }

        return $this->render('assign', [
            'model' => $model,
        ]);*/
    }
}
