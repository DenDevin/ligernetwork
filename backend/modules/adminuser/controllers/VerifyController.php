<?php

namespace backend\modules\adminuser\controllers;

use common\components\Notify\models\Notification;
use Yii;
use common\models\Verify;
use common\models\Profile;
use common\models\search\VerifySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VerifyController implements the CRUD actions for Verify model.
 */
class VerifyController extends Controller
{





    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Verify models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VerifySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionNotice()
    {
        $notification = new Notification();
        if ($notification->load(Yii::$app->request->post()) && $notification->validate())
        {
            $notification->sendClarifyMessage($notification);
        }

        return $this->redirect(['index']);

    }



    public function actionView($id)
    {

        $notification = new Notification();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'notification' => $notification,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {

        $verify = $this->findModel($id);
        $verify->deleteImages($verify);

        $verify->delete();


        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Verify::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionConfirm($id)
    {
        $model = Verify::findOne(['user_id' => $id]);
        if($model->verifyUser($id, $model))
        {
            return $this->redirect('index');
        }

    }

}
