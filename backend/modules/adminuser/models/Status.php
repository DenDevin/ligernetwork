<?php

namespace backend\modules\adminuser\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string|null $code
 * @property string|null $title
 * @property int|null $turnover
 * @property int|null $bonus
 * @property int|null $cost
 * @property int|null $term
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turnover', 'bonus', 'cost', 'term'], 'integer'],
            [['code', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'title' => 'Название',
            'turnover' => 'Товарооборот',
            'bonus' => 'Бонус',
            'cost' => 'Стоимость',
            'term' => 'Срок предоставления',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \backend\modules\adminuser\models\query\StatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\adminuser\models\query\StatusQuery(get_called_class());
    }
}
