<?php

namespace backend\modules\adminuser\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\adminuser\models\Status]].
 *
 * @see \backend\modules\adminuser\models\Status
 */
class StatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \backend\modules\adminuser\models\Status[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \backend\modules\adminuser\models\Status|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
