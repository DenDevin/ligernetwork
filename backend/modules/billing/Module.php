<?php

namespace backend\modules\billing;

/**
 * billing module definition class
 */
class Module extends \yii\base\Module
{

    public $layout = '@themes/ligertheme/backend/views/layouts/main';
    public $controllerNamespace = 'backend\modules\billing\controllers';


    public function init()
    {
        parent::init();


    }
}
