<?php

namespace backend\modules\billing\models;

use Yii;

/**
 * This is the model class for table "payment_system".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $application_title
 * @property string|null $pass
 * @property string|null $secret_key
 * @property string|null $currency_type
 * @property string|null $data
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 */
class PaymentSystem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_system';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'string'],
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['name', 'application_title', 'pass', 'secret_key', 'currency_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'application_title' => 'Application Title',
            'pass' => 'Pass',
            'secret_key' => 'Secret Key',
            'currency_type' => 'Currency Type',
            'data' => 'Data',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \backend\modules\billing\models\query\PaymentSystemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\billing\models\query\PaymentSystemQuery(get_called_class());
    }
}
