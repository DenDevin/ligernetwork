<?php

namespace backend\modules\billing\models;

use Yii;
use backend\modules\billing\models\query\UserTransactionQuery;
use common\models\User;
use common\models\Payment\UserTransaction as BaseUserTransaction;

/**
 * This is the model class for table "user_transaction".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $payment_type
 * @property int|null $transaction_id
 * @property string|null $payment_title
 * @property string|null $invoice_link
 * @property float|null $payment_summ_btc
 * @property int|null $payment_summ_eur
 * @property int|null $payment_system
 * @property int|null $payment_status
 * @property string|null $payment_bank
 * @property string|null $routing
 * @property string|null $account_number
 * @property string|null $account_type
 * @property string|null $sender_number
 * @property string|null $reciever_number
 * @property int|null $sender_id
 * @property int|null $reciever_id
 * @property string|null $benefitiary_name
 * @property int $created_at
 * @property int $updated_at
 */
class UserTransaction extends BaseUserTransaction
{

    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_EXPIRED = 2;


    public static function tableName()
    {
        return 'user_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'transaction_id', 'payment_summ_eur', 'payment_system', 'payment_status', 'sender_id', 'reciever_id', 'created_at', 'updated_at'], 'integer'],
            [['payment_summ_btc'], 'number'],
            [['created_at', 'updated_at'], 'required'],
            [['payment_type', 'payment_title', 'invoice_link', 'payment_bank', 'routing', 'account_number', 'account_type', 'sender_number', 'reciever_number', 'benefitiary_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'payment_type' => 'Вид платежа',
            'transaction_id' => 'ID Транзакции',
            'payment_title' => 'Название платежа',
            'invoice_link' => 'Ссылка на инвойс',
            'payment_summ_btc' => 'Сумма в BTC',
            'payment_summ_eur' => 'Сумма в EUR',
            'payment_system' => 'Платежная система',
            'payment_status' => 'Статус платежа',
            'payment_bank' => 'Банк',
            'routing' => 'Routing',
            'account_number' => 'Account Number',
            'account_type' => 'Account Type',
            'sender_number' => 'Sender Number',
            'reciever_number' => 'Reciever Number',
            'sender_id' => 'Sender ID',
            'reciever_id' => 'Reciever ID',
            'benefitiary_name' => 'Benefitiary Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }




    /**
     * {@inheritdoc}
     * @return UserTransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserTransactionQuery(get_called_class());
    }

    public function getUserById($id)
    {
        $user = User::findOne($id);
        return $user->username;
    }
}
